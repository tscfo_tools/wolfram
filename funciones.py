
"""
Created on Mon Jul 12 11:10:22 2021

@author: romina

Mejoras:
    - Random Forest para prediccion de campos a asignar automáticamente
    - Asignación de borrar y borrar? más rápida
    - Exchange rate automático
    - link de drive ya funciona
    - color blanco de mayor solucionado
"""

import pandas as pd
import datetime as dt
from difflib import SequenceMatcher
from os.path import exists,basename,dirname,abspath
from xlwings import App,Book
import win32com.client as win32
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.ensemble import RandomForestClassifier
from forex_python.converter import CurrencyRates
from getpass import getuser
from os import remove
from shutil import move as move_file
import psutil
from time import sleep
from win32com.client import constants
import numpy as np

from Wolfram.InputPL_CF.ML_assignment import ML_assignment
from Wolfram.InputPL_CF.update_forecast import update_forecast

# import warnings
# warnings.filterwarnings("ignore")
from warnings import simplefilter
simplefilter(action="ignore", category=pd.errors.PerformanceWarning)


def dataframe_difference(df1, df2, lista=None, which=None):
    '''
    Parameters
    ----------
    df1 : dataframe
    df2 : dataframe
    which : TYPE, optional
        The default is None.

    Returns
    -------
    diff_df : dataframe.

    '''
    """Find rows which are different between two DataFrames."""
    comparison_df = df1.merge(
        df2,
        indicator=True,
        how='right',
        on=lista,
        suffixes=('_DROP', ''),
    )
    if which is None:
        diff_df = comparison_df[comparison_df['_merge'] != 'both']
    else:
        diff_df = comparison_df[comparison_df['_merge'] == which]
        #diff_df.to_csv('data/diff.csv')
    return diff_df


def cambiar_mes(fecha):
    try:
        fecha=dt.datetime.strptime(fecha,'%d-%m-%Y').strftime("%b-%y")
    except:
        
            fecha=dt.datetime.strptime(fecha,'%d/%m/%Y').strftime("%b-%y")
        
    return fecha
        
def cambiar_fecha1(columna_fecha):
    try:
        columna_fecha=pd.to_datetime(columna_fecha,format='%d-%m-%Y')
        columna_fecha=columna_fecha.replace("-","/")
    except:
        try:
            columna_fecha=pd.to_datetime(columna_fecha,format='%d/%m/%Y')
        except:
            try:
                columna_fecha=pd.to_datetime(columna_fecha,format='%d-%m-%y')
            except:
                columna_fecha=pd.to_datetime(columna_fecha,format='%d/%m/%y')
    return columna_fecha   

def cambiar_fecha2(fecha):
    
    try:
        fecha=fecha.strftime('%d-%m-%Y')
        fecha=fecha.replace("-","/")
    except:
            fecha=fecha.strftime('%d/%m/%Y')
    return fecha
        
def cambiar_tipos(x,y):
    try:
        y=type(x)(y)
    except:
        y=y   
    return y    

    

def similar(a, b):
    '''

    Parameters
    ----------
    a : str.
    b : str.

    Returns
    -------
    TYPE
        boolean.

    '''
    return SequenceMatcher(None, a, b).ratio()

def z_st(n,df):
    media=df.mean()
    sigma=df.std(ddof=0)
    z=(n-media)/sigma
    return z
    
def insertar_filas(mayor_nombre, mayor_hoja, inp_nombre, inp_hoja, move, libro_nuevo, month, year, 
                   asignacion_aut = None, company = None, predictor = "Words similarity (%)", tmp_path = 'C:/tmp/', both_moves = False):
    """

    Parameters
    ----------
    mayor : string. nombre del documento
    mayor_hoja :string. nombre de la hoja en excel
    inp : string.nombre del documento del Reporting
    inp_hoja : string
    move : string. Values= CF or PL
    asignacion_aut : list

    Returns
    -------
    excel.

    """
    
    #Crear libro nuevo en escritorio y cambiar los exchange rates a 1 en Aux, tambien quitar filtros y mostras filas escondidas de hoja Input
    
    save_to_path= tmp_path
    nombre_nuevo_inp=basename(libro_nuevo)
    reporting_escritorio=save_to_path + nombre_nuevo_inp
    exists1=check_if_file_exists(reporting_escritorio)
    if exists1==False:
        app=App(visible=False)
        wb=Book()
        wb.save(reporting_escritorio)
        wb.close()
        app.kill()
        for proc in psutil.process_iter():
            if proc.name() == "EXCEL.EXE":
                proc.kill()
    
    
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False 
    excel.DisplayAlerts = False
    try:
        if both_moves == True and move =="CF":
            wb = excel.Workbooks.Open(reporting_escritorio, False, None)
        else:
            wb = excel.Workbooks.Open(inp_nombre, False, None)
            
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {libro_nuevo}')

    try:
        ws=wb.Sheets(inp_hoja)
    except:
        sleep(4.0)
        ws=wb.Sheets(inp_hoja)
    
    try:
        ws.ShowAllData()
    except:
        pass
    
    try:
        ws2=wb.Sheets("Aux")
        ws2.Range("exrate").Value=1
    except:
        pass
    
    ws.Rows.EntireRow.Hidden=False
    if ws.FilterMode: ws.ShowAllData()
    wb.SaveAs(reporting_escritorio.replace("/","\\"))
    wb.Close(True)
    
    
    #Copiar mayor a escritorio
    nombre_nuevo_mayor=basename(mayor_nombre)
    mayor_escritorio=save_to_path + nombre_nuevo_mayor
    try:
        wb = excel.Workbooks.Open(mayor_nombre,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {libro_nuevo}')
    ws=wb.Sheets(mayor_hoja)
    col_fecha=ws.Rows("1:1").Find("Fecha").Column
    ws.Columns(colnum_string(col_fecha)+":"+colnum_string(col_fecha)).TextToColumns(Destination=ws.Range(colnum_string(col_fecha)+"1"),DataType=constants.xlDelimited,
                                        TextQualifier=constants.xlDoubleQuote,ConsecutiveDelimiter=False,Tab=False,
                                        Semicolon=False, Comma=False, Space=False, Other=False, FieldInfo=(1,4),TrailingMinusNumbers=True)
    
    last_row=ws.Range("A" + str(ws.Rows.Count)).End(3).Row
    ws.Range("1:" + str(last_row)).ClearFormats()
    wb.SaveAs(mayor_escritorio.replace("/","\\"))
    wb.Close(True)
    
    excel.Visible = False 
    excel.DisplayAlerts = False
    excel.Application.Quit() 
    for proc in psutil.process_iter():
        if proc.name() == "EXCEL.EXE":
            proc.kill()
    
    
    #abrir documentos de escritorio con pandas
    mayor=pd.read_excel(mayor_escritorio,sheet_name=mayor_hoja)
    
    if move=="PL":
        mayor=mayor.drop(mayor[mayor.MovePL!=1].index)
    elif move=="CF":
        mayor=mayor.drop(mayor[mayor.MoveCF!=1].index)
        
    inp=pd.read_excel(reporting_escritorio, sheet_name=inp_hoja)
    inp = inp.loc[:, ~inp.columns.str.contains('^Unnamed')]
    ultima_fila_inp=inp[inp["Concepto"]=="END"].index[0]+2
    
    inp_old=inp.copy()
    inp_old["Borrar/Nuevo"]=""
    if company!=None:
        inp=inp.drop(inp[inp.Company!=company].index)
        mayor["Company"]=company
    
    inp=inp.drop(inp[inp["Concepto"]=="END"].index)
    inp.drop('END',axis='columns', inplace=True)
    
    #Convertir Net, Haber y Debe a numeros
    mayor["Net"]=pd.to_numeric(mayor["Net"],errors='coerce')
    mayor["Haber"]=pd.to_numeric(mayor["Haber"],errors='coerce')
    mayor["Debe"]=pd.to_numeric(mayor["Debe"],errors='coerce')
    inp["Net"]=pd.to_numeric(inp["Net"],errors='coerce')
    inp["Haber"]=pd.to_numeric(inp["Haber"],errors='coerce')
    inp["Debe"]=pd.to_numeric(inp["Debe"],errors='coerce')
    
    #Eliminar filas con NaN en la fecha
    inp["Fecha"]=inp["Fecha"].fillna(-99999)
    mayor["Fecha"]=mayor["Fecha"].fillna(-99999)
    inp.drop(inp[inp["Fecha"]==-99999].index,inplace=True)
    mayor.drop(mayor[mayor["Fecha"]==-99999].index,inplace=True)
    
    if str(mayor.Fecha.dtype)=="float64" or str(mayor.Fecha.dtype)=="int64":
        mayor.Fecha=mayor["Fecha"].apply(lambda x: dt.datetime(1900,1,1)+dt.timedelta(days=x-2))
    else:
        mayor["Fecha"]=mayor["Fecha"].apply(lambda x: cambiar_fecha1(x))
    
    #Modificar formatos de meses y fechas
    
    inp["Fecha"]=inp["Fecha"].apply(lambda x: cambiar_fecha1(x))
    
    mayor["Fecha"]=mayor["Fecha"].apply(lambda x: cambiar_fecha2(x))
    inp["Fecha"]=inp["Fecha"].apply(lambda x: cambiar_fecha2(x))
    
    mayor["Month"]=mayor["Fecha"].apply(lambda x: cambiar_mes(x))
    inp["Month"]=inp["Fecha"].apply(lambda x: cambiar_mes(x))
    
    mayor["Haber"]=mayor["Haber"].fillna(0)
    mayor["Debe"]=mayor["Debe"].fillna(0)
    
    inp["Haber"]=inp["Haber"].fillna(0)
    inp["Debe"]=inp["Debe"].fillna(0)
    if move=="CF":
        mayor["Net"]=mayor["Debe"]-mayor["Haber"]
        inp["Net"]=inp["Debe"]-inp["Haber"]
    elif move=="PL":
        mayor["Net"]=mayor["Haber"]-mayor["Debe"]
        inp["Net"]=inp["Haber"]-inp["Debe"]
        
    mayor["Net"]=mayor["Net"].fillna(0)
    inp["Net"]=inp["Net"].fillna(0)
    

    
    #Comparar nombres de columnas
    col_iguales=list(set(inp.columns).intersection(set(mayor.columns)))
    

    
    for titulo in col_iguales:
        if titulo!="Net" or titulo!="Haber" or titulo!="Debe" or titulo!="Saldo":
            try:
                mayor[titulo]=mayor[titulo].apply(lambda x: cambiar_tipos(inp[titulo].iloc[0],x))
            except:
                pass
            try:
                inp[titulo]=inp[titulo].apply(lambda x: cambiar_tipos(mayor[titulo].iloc[0],x))
            except:
                pass
            try:
                mayor[titulo]=mayor[titulo].astype(inp[titulo].dtype)
            except:
                pass 

    
    inp["Borrar/Nuevo"]=""
    inp["indexes"]=list(inp.axes[0])
    mayor_comparar=mayor[col_iguales]
    inp_comparar=inp[col_iguales+["indexes"]]
    

    titulos_a_comparar=["Nº Asiento","Net","Month","Cuenta"]
    if not "Nº Asiento" in inp_comparar.columns:
        titulos_a_comparar=["Concepto","Net","Month","Cuenta"]
    if not "Cuenta" in inp_comparar.columns:
        titulos_a_comparar=["Concepto","Net","Month","Nombre cuenta"]
    
    no_iguales_inp=dataframe_difference(mayor_comparar,inp_comparar,lista=titulos_a_comparar)
    no_iguales_mayor=dataframe_difference(inp_comparar,mayor_comparar,lista=titulos_a_comparar)
    
    
    
    #Borrar columnas con DROP y columna _merge
    no_iguales_inp=no_iguales_inp.drop(['_merge'],axis=1)
    no_iguales_mayor=no_iguales_mayor.drop(['_merge'],axis=1)
    cols = [c for c in no_iguales_inp.columns if '_DROP' in c]
    no_iguales_inp=no_iguales_inp.drop(cols,axis=1)
    no_iguales_mayor=no_iguales_mayor.drop(cols,axis=1)
        
    filas_repetidas=inp_comparar[inp_comparar[titulos_a_comparar].duplicated(keep=False)]
    
    #Desde aquí empezaremos a editar el excel con la hoja input en el mismo excel
    #Primero se abre el excel con xlwings para añadir las filas sin afectar las fórmulas
    filas_nuevas=len(no_iguales_mayor)
    
    deter_coeff="None"
    
    if len(filas_repetidas)!=0:
        app = App(visible=False)
        wb_inp=Book(reporting_escritorio)
        ws_inp=wb_inp.sheets[inp_hoja]
        
        lista_col=list(inp.columns)
        inp_old["Borrar/Nuevo"].iloc[list(filas_repetidas["indexes"])]="Fila repetida"
        pos_col=lista_col.index("Borrar/Nuevo")+2
        rango=chr(64+pos_col)+"1"
        try:
            ws_inp.range(rango).options(index=False).value=inp_old["Borrar/Nuevo"]
        except:
            pass
    
    if len(no_iguales_inp)!=0:
        if len(filas_repetidas)==0:
            app = App(visible=False)
            wb_inp=Book(reporting_escritorio)
            ws_inp=wb_inp.sheets[inp_hoja]
        
        
        meses=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
        no_iguales_inp_neg=inp_comparar.copy()
        no_iguales_inp_neg.Net=-no_iguales_inp_neg.Net
        no_iguales_inp_neg.Month=no_iguales_inp_neg["Month"].apply(lambda x: x.replace(x[0:3],meses[meses.index(x[0:3])-1]) if x[0:3]!="Jan" else x)
        borrar_=no_iguales_inp.merge(no_iguales_inp_neg,indicator=True,how="inner",on=titulos_a_comparar,suffixes=("DROP_",""))
        borrar_=borrar_.append(no_iguales_inp_neg.merge(no_iguales_inp,indicator=True,how="inner",on=titulos_a_comparar,suffixes=("DROP_","")))
        borrar=no_iguales_inp[~no_iguales_inp["indexes"].isin(list(borrar_.indexes))]
        lista_borrar_=list(borrar_["indexes"])
        inp_old["Borrar/Nuevo"].iloc[list(borrar["indexes"])]="Borrar"
        inp_old["Borrar/Nuevo"].iloc[lista_borrar_]="Borrar?"
        lista_col=list(inp.columns)
        pos_col=lista_col.index("Borrar/Nuevo")+2
        rango=chr(64+pos_col)+"1"
        try:
            ws_inp.range(rango).options(index=False).value=inp_old["Borrar/Nuevo"]
        except:
            pass
        
    
    
    
    
    #Añadir filas en blanco antes del END
    if filas_nuevas!=0:
        #Predecir columnas de asignacion automatica del mayor
        if len(no_iguales_inp)==0 and len(filas_repetidas)==0:
            app = App(visible=False)
            wb_inp=Book(reporting_escritorio)
            ws_inp=wb_inp.sheets[inp_hoja]
        
        if predictor=="Machine learning":
            no_iguales_mayor,deter_coeff=RF_predictor(inp,col_iguales,asignacion_aut,no_iguales_mayor)
            no_iguales_mayor["Borrar/Nuevo"]="Nuevo"
        elif predictor == "ML (new preprocessing)":
  
            ## NA accounts initial numbers
            NA_accounts_ini = ["63", "64", "66", "67", "68", "69", "76", "77", "79"] if (company == "ESP") else []
            # Possible labels for ML
            labels_to_use = ["Concepto", "Nombre cuenta"]

            ## Apply ML assignment
            inp, no_iguales_mayor, deter_coeff = ML_assignment(inp, no_iguales_mayor, "Cuenta", "indexes", labels_to_use, asignacion_aut, NA_accounts_ini)
            no_iguales_mayor["Borrar/Nuevo"]="Nuevo"

        elif predictor=="Words similarity (%)":
            no_iguales_mayor=similares(no_iguales_mayor,inp,asignacion_aut)
        
        no_iguales_mayor.replace(to_replace="nan",value="",inplace=True)
        
        if "Fecha" in no_iguales_mayor.columns:
            no_iguales_mayor.Fecha=cambiar_fecha1(no_iguales_mayor.Fecha)
            no_iguales_mayor.Fecha=no_iguales_mayor["Fecha"].apply(lambda x: (x-dt.datetime(1900,1,1)).days+2)

        
        rango=str(ultima_fila_inp) +":" + str(ultima_fila_inp+filas_nuevas)
        ws_inp.range(rango).insert()
    
        #Copiar formato y valores nuevos
        
        lista_col=list(inp.columns)
        col_iguales=list(set(inp.columns).intersection(set(no_iguales_mayor.columns)))
                
        for titulo in col_iguales:
            if titulo=="indexes":
                pass
            else:
                pos_col=lista_col.index(titulo)+1
                if titulo=="Borrar/Nuevo": #  or ("Acc - " in titulo and predictor == "ML (new preprocessing)") or ("Diff - " in titulo and predictor == "ML (new preprocessing)"):
                    pos_col=pos_col+1
                rango_antes=colnum_string(pos_col) + str(ultima_fila_inp-1)
                rango=colnum_string(pos_col)+str(ultima_fila_inp)
                ws_inp.range(rango).options(index=False).value=no_iguales_mayor[titulo]
                rango=colnum_string(pos_col)+str(ultima_fila_inp)+":"+colnum_string(pos_col)+str(ultima_fila_inp+len(no_iguales_mayor)-1)
                
                if "Acc - " in titulo and predictor == "ML (new preprocessing)":
                    ws_inp.range(colnum_string(pos_col) + "1:" + colnum_string(pos_col) + str(ultima_fila_inp-1)).options(index=False).value = ""
                    ws_inp.range(colnum_string(pos_col) + str(1)).options(index=False).value = titulo
                elif "Diff - " in titulo and predictor == "ML (new preprocessing)":
                    ws_inp.range(colnum_string(pos_col) + "1:" + colnum_string(pos_col) + str(ultima_fila_inp-1)).options(index=False).value = ""
                    ws_inp.range(colnum_string(pos_col) + str(1)).options(index=False).value = titulo
                else:
                    ws_inp.range(rango_antes).copy()
                    ws_inp.range(rango).paste("formats")
  
                
        #Copiar fórmulas        
        for titulo in lista_col:
            if titulo not in asignacion_aut:
                pos_col=lista_col.index(titulo)+1
                rango_antes=colnum_string(pos_col) + str(ultima_fila_inp-1)
                if ws_inp.range(rango_antes).formula!="":
                    if ws_inp.range(rango_antes).formula[0]=="=" and titulo!="Fecha":
                        rango=colnum_string(pos_col)+str(ultima_fila_inp)+":"+colnum_string(pos_col)+str(ultima_fila_inp+len(no_iguales_mayor))
                        ws_inp.range(rango_antes).copy()
                        ws_inp.range(rango).paste("formulas")
                        

    
    
    
    if filas_nuevas==0 and len(no_iguales_inp)==0 and len(filas_repetidas)==0:
            app = App(visible=False)
            wb_inp=Book(reporting_escritorio)
            ws_inp=wb_inp.sheets[inp_hoja]
    
    lista_col=list(inp.columns)
    pos_col=lista_col.index("Borrar/Nuevo")+2
    rango=colnum_string(pos_col)+"1"
    ws_inp.range(rango).options(index=False).value="Borrar/Nuevo"
    
    if filas_nuevas!=0:
        ws_inp.range(str(ultima_fila_inp)+":"+str(ultima_fila_inp)).api.Delete()
    
    try:
        ws2=wb_inp.sheets["Aux"]
        monedas=ws2.range("currencies").value
        cambios=[]
        c=CurrencyRates('http://www.ecb.int/stats/eurofxref/eurofxref-hist.zip')
        for moneda in monedas:
            cambios.append([c.convert(moneda,"EUR",1)])
        ws2.range("exrate").options(index=False).value=cambios
    except:
        pass
    
    wb_inp.save()
    wb_inp.close()
     
    app.kill()

    # Update forecast
    if move == "PL": input_op = ["InputPL"]
    if move == "CF": input_op = ["InputCF"]
    update_forecast(reporting_escritorio, month, year, input_op)

    
    #Mover reporting de escritorio a ruta de libro nuevo
    if not(both_moves == True and move =="PL"):
        #Eliminar mayor de escritorio
        if reporting_escritorio!=libro_nuevo.replace("\\","/") and reporting_escritorio.lower()!=libro_nuevo.lower():
            existe=check_if_file_exists(libro_nuevo)
            if existe==True:
                remove(libro_nuevo)
            carpeta=dirname(abspath(libro_nuevo))
            move_file(reporting_escritorio,carpeta)
        if mayor_escritorio!=mayor_nombre.replace("\\","/") and mayor_escritorio.lower()!=mayor_nombre.lower():
            remove(mayor_escritorio)

    for proc in psutil.process_iter():
        if proc.name() == "EXCEL.EXE":
            proc.kill()
        
    return deter_coeff


def insertar_filas_enero(mayor_nombre,mayor_hoja,inp_nombre,inp_hoja,move,libro_nuevo,
                          asignacion_aut=None,company=None,predictor="Words similarity (%)"):
    a=0

        
def check_if_file_exists(path_with_file_name):
    """
    

    Parameters
    ----------
    path_with_file_name :string
    Returns
    -------
    exists1 : BOOLEAN.

    """
    exists1=exists(path_with_file_name)
    return exists1

def col_exists(mayor_nombre,mayor_hoja,libro_nuevo,inp_hoja):
    mayor=pd.read_excel(mayor_nombre,sheet_name=mayor_hoja)
    inp=pd.read_excel(libro_nuevo, sheet_name=inp_hoja)

    if "Concepto" in inp.columns and "Net" in inp.columns and "Month" in inp.columns and "Concepto" in mayor.columns and "Net" in mayor.columns and "Month" in mayor.columns:
        hay_col=True
    else:
        hay_col=False
    
    return hay_col

def lista_asignacion(input_nombre,input_hoja):
    inp=pd.read_excel(input_nombre, sheet_name=input_hoja)
    lista_asignacion_aut=list(inp.columns)
    return lista_asignacion_aut




def hoja_mayor(mayor_nombre):
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False
    excel.DisplayAlerts = False
    try:
        wb = excel.Workbooks.Open(mayor_nombre,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {mayor_nombre}')
    sleep(0.5)
    lista_hojas=[sheet.Name for sheet in wb.Sheets]
    wb.Close(True)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit() 
    
    return lista_hojas
        
    

def cambiar_mes_pivot(fecha):
    try:
        mes=int(dt.datetime.strptime(fecha,'%d-%m-%Y').strftime('%m'))+1
        if mes>12:
            mes=1
        anno=int(dt.datetime.strptime(fecha,'%d-%m-%Y').strftime('%Y'))
        dia=dt.date(anno, mes, 1) - dt.timedelta(days=1)
        dia=int(dia.strftime("%d"))
        mes=mes-1
        if mes<1:
             mes=12
        
    except:
         mes=int(dt.datetime.strptime(fecha,'%d/%m/%Y').strftime('%m'))+1
         if mes>12:
            mes=1
         anno=int(dt.datetime.strptime(fecha,'%d/%m/%Y').strftime('%Y'))
         dia=dt.date(anno, mes, 1) - dt.timedelta(days=1)
         dia=int(dia.strftime("%d"))
         mes=mes-1
         if mes<1:
             mes=12
    fecha=dt.date(anno,mes,dia).strftime('%d/%m/%Y')
    return fecha


def is_binary(series, allow_na=False):
    if allow_na:
        series.dropna(inplace=True)
    return sorted(series.unique()) == [0, 1]


def RF_predictor(df,columnas_para_prediccion,targets,df_mayor):
    """
    

    Parameters
    ----------
    df : dataframe de inputpl o inputcf limpio (pandas)
    columnas_para_prediccion : list - columnas a ser usadas para predicción de algoritmo
    targets : list - columnas a asignar
    df_mayor : dataframe del mayor limpio (pandas)
    Returns
    -------
    df_mayor : dataframe pandas del mayor +  columnas asignadas
    deter_coeff : determination coefficient float

    """
  
    
    # 0) Limpieza de datos
    df_mayor2=df_mayor[columnas_para_prediccion]
    df=df[columnas_para_prediccion+targets]
    df[targets]=df[targets].fillna("NA")
    
    if "Nº Asiento" in columnas_para_prediccion:
        df["Nº Asiento"]=df["Nº Asiento"].fillna(0)
        df["Nº Asiento"]=df["Nº Asiento"].apply(lambda x: 1 if x!=0 else 0)
        df_mayor2["Nº Asiento"]=df_mayor2["Nº Asiento"].fillna(0)
        df_mayor2["Nº Asiento"]=df_mayor2["Nº Asiento"].apply(lambda x: 1 if x!=0 else 0)
        
    if "Fecha" in columnas_para_prediccion:
        df.drop("Fecha",axis="columns",inplace=True)
        df_mayor2.drop("Fecha",axis="columns",inplace=True)
        columnas_para_prediccion.remove("Fecha")
        
    if "Saldo" in columnas_para_prediccion and ("Cuenta" in columnas_para_prediccion or "Nombre cuenta" in columnas_para_prediccion):
        df.drop("Saldo",axis="columns",inplace=True)
        df_mayor2.drop("Saldo",axis="columns",inplace=True)
        columnas_para_prediccion.remove("Saldo")
    
    if "Cuenta" in columnas_para_prediccion and "Nombre cuenta" in columnas_para_prediccion:
        df.drop("Nombre cuenta",axis="columns",inplace=True)
        df_mayor2.drop("Nombre cuenta",axis="columns",inplace=True)
        columnas_para_prediccion.remove("Nombre cuenta")
        
    if "Net" in columnas_para_prediccion and "Debe" in columnas_para_prediccion:
        df.drop("Debe",axis="columns",inplace=True)
        df_mayor2.drop("Debe",axis="columns",inplace=True)
        columnas_para_prediccion.remove("Debe")
    if "Net" in columnas_para_prediccion and "Haber" in columnas_para_prediccion:
        df.drop("Haber",axis="columns",inplace=True)
        df_mayor2.drop("Haber",axis="columns",inplace=True)
        columnas_para_prediccion.remove("Haber")
    
    for column in df.columns:
        nans=df[column].isna().sum()
        if nans/len(df)>0.5 and column!="Concepto" and column in columnas_para_prediccion:
            df.drop(column,axis="columns",inplace=True)
            df_mayor2.drop(column,axis="columns",inplace=True)
            columnas_para_prediccion.remove(column)
    
    if "Concepto" in df.columns:
        df["Concepto"]=df["Concepto"].fillna("NA")
        df_mayor2["Concepto"]=df_mayor2["Concepto"].fillna("NA")
        
    
    if "Cuenta" in df.columns:
        df["Cuenta"]=df["Cuenta"].fillna(0)
        df_mayor2["Cuenta"]=df_mayor2["Cuenta"].fillna(0)
    if "Nombre cuenta" in df.columns:
        df["Nombre cuenta"]=df["Nombre cuenta"].fillna("NA")
        df_mayor2["Nombre cuenta"]=df_mayor2["Nombre cuenta"].fillna("NA")
        
    
    for column in columnas_para_prediccion:
        nans=df[column].isna().sum()
        nans_mayor=df_mayor2[column].isna().sum()
        if nans/len(df)>0.1 and str(df[column].dtype)=="object" and column in columnas_para_prediccion and nans!=0:
            df[column]=df[column].fillna("NA")
        if nans/len(df)>0.1 and str(df[column].dtype)=="object" and column in columnas_para_prediccion and nans_mayor!=0:
            df_mayor2[column]=df_mayor2[column].fillna(str("NA"))
        if nans/len(df)<=0.1 and str(df[column].dtype)=="object" and column in columnas_para_prediccion and nans!=0:
            df[column]=df[column].fillna(df[column].value_counts().idxmax())
        if nans/len(df)<=0.1 and str(df[column].dtype)=="object" and column in columnas_para_prediccion and nans_mayor!=0:
            df_mayor2[column]=df_mayor2[column].fillna(df_mayor2[column].value_counts().idxmax())
        if (str(df[column].dtype)=="float64" or str(df[column].dtype)=="int64")  and nans!=0:
            df[column]=df[column].fillna(round(df[column].sum()/df[column].count()))
        if (str(df[column].dtype)=="float64" or str(df[column].dtype)=="int64") and nans_mayor!=0:
            df_mayor2[column]=df_mayor2[column].fillna(round(df_mayor2[column].sum()/df_mayor2[column].count()))
        
    
    # 1) Convertir variables categóricas a dummies
    for columna in columnas_para_prediccion:
        if columna!="Concepto" and (str(df[columna].dtype)!="float64" or (df[columna].nunique()<=10 and is_binary(df[columna])==False)):
            x=pd.get_dummies(data=df[columna],drop_first=True,prefix=columna + "_")
            df=df.drop(columna,axis='columns')
            df=pd.concat((df,x),axis=1)
            
            columnas_nuevas=list(x.columns)
           
            df_mayor2[columnas_nuevas]=0
            for columna_nueva in columnas_nuevas:
                df_mayor2[columna_nueva]=df_mayor2[columna].apply(lambda x: 1 if x==columna_nueva else 0)
            df_mayor2=df_mayor2.drop(columna,axis="columns")
            
  
    
    #2) Convertir las variables de targets a integer
    category_to_id={}
    id_to_category={}
    for target in targets:
        category_id="category_id"+ target
        df[category_id]=df[target].factorize()[0]
        category_id_df = df[[target, category_id]].drop_duplicates().sort_values(category_id)
        category_to_id[target]=dict(category_id_df.values)
        id_to_category[target] = dict(category_id_df[[category_id, target]].values)
        
        
    #3) Calcular medida numérica para variable Concepto como tf-idf (Term Frequency, Inverse Document Frequency)
    tfidf = TfidfVectorizer(sublinear_tf=True, min_df=1, norm='l2', encoding='latin-1', ngram_range=(1, 2), stop_words=["el","la","las","los","le","de","a","the","en","in","del","&","and","y","por"])
    features1=tfidf.fit_transform(df.Concepto)
    features = features1.toarray()
    palabras_clave=tfidf.get_feature_names_out()
    palabras_clave=list(map(lambda x: str(x),palabras_clave))
    
    #Convertir df_mayor2.Concepto a str
    df_mayor2["Concepto"]=df_mayor2["Concepto"].apply(lambda x: str(x))
    
    #4) Agregar tantos campos como palabras clave a df y eliminar columna "Concepto"
    df2=pd.DataFrame(data=features,columns=palabras_clave)
    df.reset_index(inplace = True,drop=True)
    df3=pd.concat((df,df2),axis=1)
    df3.drop("Concepto",axis="columns",inplace=True)
    
    
    for palabra_clave in palabras_clave:
        df_mayor2[palabra_clave]=df_mayor2["Concepto"].apply(lambda x: 1 if palabra_clave.lower() in x.lower() else 0)
    df_mayor2.drop("Concepto",axis="columns",inplace=True)
    
    for target in targets:
        category_id="category_id"+ target
        df3.drop(category_id,axis="columns",inplace=True)
        df3.drop(target,axis="columns",inplace=True)
    
    
    
    
    #5) Dividir datos en train y test y conseguir regresión lineal
    deter_coeff=[]
    model=RandomForestClassifier(random_state=12345)
 
        
    for target in targets:
        category_id="category_id"+ target
        maximo=max(id_to_category[target].keys())
        minimo=min(id_to_category[target].keys())
        
    
    
        X_train, X_test, y_train, y_test = train_test_split(df3,df[category_id],random_state=1234)
        
 
        #dfs = []
        
        #kfold = model_selection.KFold(n_splits=5, shuffle=True, random_state=90210)
        #cv_results = model_selection.cross_validate(model, X_train, y_train, cv=kfold)
        clf = model.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        y_pred=np.around(y_pred,0)
        y_pred=np.clip(y_pred,a_min=minimo,a_max=maximo)
        

        #this_df = pd.DataFrame(cv_results)
        #dfs.append(this_df)
        deter_coeff.append(round((r2_score(y_test,y_pred)),2))
            
       
        #predicción para el mayor
        X_mayor=df_mayor2
        
        y_pred_mayor = clf.predict(X_mayor)
        y_pred_mayor=np.clip(y_pred_mayor,a_min=minimo,a_max=maximo)
        
        df_mayor[target]=0
        
        df_mayor2[target]=0
        df_mayor2[target]=y_pred_mayor
        df_mayor2[target]=round(df_mayor2[target])
        
        
        x=df_mayor2.replace({target:id_to_category[target]})
        df_mayor[target]=x[target]
        df3[target]=df[category_id]
        x=pd.get_dummies(data=df3[target],drop_first=True,prefix=target)
        df3=df3.drop(target,axis='columns')
        df3=pd.concat((df3,x),axis=1)
        
        df_mayor2[list(x.columns)]=0
        for valor in x.columns:
            valor_comparar=valor.replace(target+"_","")
            df_mayor2[valor]=df_mayor2[target].apply(lambda x: 1 if str(x)==valor_comparar else 0)
            df_mayor2[valor]=df_mayor2[valor].astype(int)
            
        df_mayor2=df_mayor2.drop(target,axis="columns")
    
    
    return df_mayor,deter_coeff



def colnum_string(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string  


# # Eliminar espacios
# def eliminar_espacios(text): 
#     return  " ".join(text.split()) 

# # To lower
# def texto_to_lower(text):
#   return text.lower()

# # Tokenizador
# def tokenization(text):
#   tokens = word_tokenize(text)
#   return tokens

# # Reemplazar contractions usando la librería "contractions" https://github.com/kootenpv/contractions


# # Quitar stop words
# from nltk.corpus import stopwords
# def quitar_stopwords(tokens):
#     stop_words_en = set(stopwords.words('english')) 
#     stop_words_es = set(stopwords.words('spanish')) 
#     stop_words=set.union(stop_words_en,stop_words_es)
#     filtered_sentence = [w for w in tokens if not w in stop_words]
#     return filtered_sentence


# # Eliminar signos de puntuación (nos quedamos sólo lo alfanumérico en este caso)
# def quitar_puntuacion(tokens):
#     words=[word for word in tokens if word.isalnum()]
#     return words


# # Lemmatization
# import en_core_web_sm
# nlp = en_core_web_sm.load(disable=['parser', 'ner'])
# def lematizar(tokens):
#     sentence = " ".join(tokens)
#     mytokens = nlp(sentence)
#     # Lematizamos los tokens y los convertimos  a minusculas
#     mytokens = [ word.lemma_ if word.lemma_ != "-PRON-" else word.lower_ for word in mytokens ]
#     # Extraemos el text en una string
#     return " ".join(mytokens)


# from nltk.stem import PorterStemmer
# stemmer = PorterStemmer()
# def stem(tokens):
#     tokens = [ stemmer.stem(token) for token in tokens]
#     return tokens


# #hacer función de ratios
def similares(no_iguales_mayor,inp,asignacion_aut):

    #lista_titulos=["Cuenta","Net",""]
    no_iguales_mayor["Borrar/Nuevo"]=0
    no_iguales_mayor[asignacion_aut]=0
    no_iguales_mayor["Ratio_Net"]=0

    
    for i in range(0,len(no_iguales_mayor)):
        inp["Aux1"]=0
        inp["Aux2"]=0
        inp["Aux"]=0
        inp["Aux1"]=inp["Concepto"].apply(lambda x: similar(str(x),str(no_iguales_mayor["Concepto"].iloc[i]))/2)
        inp["Aux2"]=inp["Cuenta"].apply(lambda x: similar(str(x),str(no_iguales_mayor["Cuenta"].iloc[i]))/2)
        inp["Aux"]=inp["Aux1"]+inp["Aux2"]
        inp=inp.sort_values(by=["Aux"],ascending=False,ignore_index=True)
        no_iguales_mayor["Borrar/Nuevo"].iloc[i]=1-inp["Aux"].iloc[0]
        for target in asignacion_aut:
            no_iguales_mayor[target].iloc[i]=inp[target].iloc[0]
    no_iguales_mayor[asignacion_aut]=no_iguales_mayor[asignacion_aut].fillna("NA")
    #no_iguales_mayor["Ratio_Net"]=no_iguales_mayor["Net"].apply(lambda x: abs(x))
    #no_iguales_mayor["Borrar/Nuevo"]=no_iguales_mayor["Borrar/Nuevo"]*no_iguales_mayor["Ratio_Net"]
    #no_iguales_mayor.drop("Ratio_Net",axis="columns",inplace=True)

    no_iguales_mayor=no_iguales_mayor.sort_values(by=["Borrar/Nuevo"])
    return no_iguales_mayor
