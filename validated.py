# -*- coding: utf-8 -*-
"""
Created on Thu Sep  9 13:19:11 2021

@author: romina

Validated ID
"""

import win32com.client as win32
# from funciones import check_if_file_exists
# from xlwings import App,Book
# import psutil

def insertar_val_pl(report,mayor,hoja_mayor,hoja_report,nombre_nuevo):
    # exists1=check_if_file_exists(nombre_nuevo)
    # if exists1==False:
    #     app=App(visible=False)
    #     wb=Book()
    #     wb.save(nombre_nuevo)
    #     wb.close()
    #     app.kill()
    # for proc in psutil.process_iter():
    #     if proc.name() == "EXCEL.EXE":
    #         proc.kill()
    
    
    
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = True
    excel.DisplayAlerts = False
    
    try:
        wb_report = excel.Workbooks.Open(report,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {report}')
     
    
    try:
        wb_mayor = excel.Workbooks.Open(mayor,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {mayor}')
          
        
    ws_report=wb_report.Sheets(hoja_report)
    ws_mayor=wb_mayor.Sheets(hoja_mayor)
    
    
    ws_report.Range("A:ZZ").ClearOutline()
    ws_report.Range("1:500").ClearOutline()
        
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    if ws_report.FilterMode: ws_report.ShowAllData()
    
    ws_mayor.Activate()

    if ws_mayor.FilterMode: ws_mayor.ShowAllData()
    
    ws_mayor.Range("A7:P7").AutoFilter(Field=1,Criteria1="<>")
    ws_mayor.Range("A7").End(-4121).Select()
    fila=excel.Selection.Row
    ws_mayor.Range("8:"+str(fila)).Delete()
    if ws_mayor.FilterMode: ws_mayor.ShowAllData()

    
    #Contar cuentas del mayor
    ws_mayor.Range("B8").End(-4121).Select()
    fila=excel.Selection.Row
    cuentas_mayor=fila-7
    
    #En la columna A, poner cuenta + nombre cuenta
    formula='=RC2&"  "&RC3'
    ws_mayor.Range("A8:A"+str(fila)).FormulaR1C1=formula
    
    #Contar cuentas del report
    ws_report.Activate()
    ws_report.Range("A:A").Find("END").Select()
    ultima_fila=excel.Selection.Row
    cuentas_report=ultima_fila-3
    
    diferencia=0
    if cuentas_mayor>cuentas_report:
        diferencia=cuentas_mayor-cuentas_report
        ws_report.Range(str(ultima_fila)+":"+str(ultima_fila+diferencia-1)).Insert()
    
    ws_report.Range("C3:O"+str(ultima_fila-1+diferencia)).ClearContents()
    
    #Copiar y pegar
    ws_mayor.Activate()
    ws_mayor.Range("A8:A"+str(7+cuentas_mayor)).Copy()
    ws_report.Range("C3").PasteSpecial(Paste=-4163)
    ws_mayor.Range("D8:O"+str(fila)).Copy()
    ws_report.Range("D3").PasteSpecial(Paste=-4163)
    
    
    print(nombre_nuevo)    
    wb_report.SaveAs(nombre_nuevo.replace("/","\\"))
    wb_report.Close(True)
    wb_mayor.Close(False)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()   
