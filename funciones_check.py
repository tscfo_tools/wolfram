# -*- coding: utf-8 -*-
"""
Created on Tue Jul 20 09:54:24 2021

@author: romina
"""

from operator import contains
import win32com.client as win32
from win32com.client import constants
from xlwings import App
from currency_converter import CurrencyConverter
from time import sleep

win32c = win32.constants

def df_from_excel(path):
    app = App(visible=False)
    book = app.books.open(path)
    book.save()
    app.kill()

    
def pivot_table(wb: object, ws1: object, pt_ws: object, ws_name: str, pt_name: str, pt_rows: list, pt_cols: list, pt_filters: list, pt_fields: list,rango,anno, col=1):
    """
    wb = workbook1 reference
    ws1 = worksheet1
    pt_ws = pivot table worksheet number
    ws_name = pivot table worksheet name
    pt_name = name given to pivot table
    pt_rows, pt_cols, pt_filters, pt_fields: values selected for filling the pivot tables
    """

    # pivot table location
    pt_loc = len(pt_filters) + 2 
    
    # grab the pivot table source data
    # try:
    #     pc = wb.PivotCaches().Create(SourceType=win32c.xlDatabase, SourceData=rango)
    # except pythoncom.com_error as error:
    #     print(error.args)
    #     print(win32api.FormatMessage(error.excepinfo[5]))
    
    pc = wb.PivotCaches().Create(SourceType=win32c.xlDatabase, SourceData=rango)
    
    # create the pivot table object
    pc.CreatePivotTable(TableDestination=f'{ws_name}!R{pt_loc}C{col}', TableName=pt_name)

    # selecte the pivot table work sheet and location to create the pivot table
    pt_ws.Select()
    pt_ws.Cells(pt_loc, 1).Select()

    # Sets the rows, columns and filters of the pivot table
    for field_list, field_r in ((pt_filters, win32c.xlPageField), (pt_rows, win32c.xlRowField), (pt_cols, win32c.xlColumnField)):
        for i, value in enumerate(field_list):
            pt_ws.PivotTables(pt_name).PivotFields(value).Orientation = field_r
            pt_ws.PivotTables(pt_name).PivotFields(value).Position = i + 1

    # Sets the Values of the pivot table
    for field in pt_fields:
        pt_ws.PivotTables(pt_name).AddDataField(pt_ws.PivotTables(pt_name).PivotFields(field[0]), field[1], field[2]).NumberFormat = field[3]


    for row_name in pt_rows:
        try:
            pt_ws.PivotTables(pt_name).PivotFields(row_name).PivotItems("END").Visible = False
        except: pass
    for col_name in pt_cols:
        try:
            pt_ws.PivotTables(pt_name).PivotFields(col_name).ClearAllFilters
            pt_ws.PivotTables(pt_name).PivotFields(col_name).PivotFilters.Add2(21, None, anno) # Filter all columns from previous years #21 = Contains
        except: pass

    # Visiblity True or Valse
    pt_ws.PivotTables(pt_name).ShowValuesRow = True
    pt_ws.PivotTables(pt_name).ColumnGrand = True
    
    
    
def run_excel(filename, sheet_name: str, pivot_sheet_name:str,company, anno):
    df_from_excel(filename)
    # create excel object
    excel = win32.gencache.EnsureDispatch('Excel.Application')

    # excel can be visible or not
    excel.Visible = True
    excel.DisplayAlerts = False
    # try except for file / path
    try:
        wb = excel.Workbooks.Open(filename,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {filename}')
        

    # set worksheet
    try: 
        ws1 = wb.Sheets(sheet_name)
    except:
        sleep(5.0)
        ws1 = wb.Sheets(sheet_name)
        
    # Setup and call pivot_table
    ws2_name = pivot_sheet_name
    if company!=None:
        try:
            try:
                ws2 = wb.Sheets(ws2_name)
            except:
                sleep(5.0)
                ws2 = wb.Sheets(ws2_name)
        except:
            wb.Sheets.Add(After=ws1).Name = ws2_name
            ws2 = wb.Sheets(ws2_name)
    
    else:
        try:
            ws2 = wb.Sheets(ws2_name)
            ws2.Delete()
            wb.Sheets.Add(After=ws1).Name = ws2_name
        except:
            wb.Sheets.Add(After=ws1).Name = ws2_name

    try:
        ws2 = wb.Sheets(ws2_name)
    except:
        sleep(4.0)
        ws2 = wb.Sheets(ws2_name)
        
    col1=1

    # Currency exchange for multicompany
    try:
        # Define Aux workshett
        aux_ws = wb.Sheets("Aux")

        # Company data
        comp_data = aux_ws.Range("Companies").Value
        companies_comp = [comp_data[i][comp_data[0].index("Company")] for i in range(1, len(comp_data) - 1)]
        currencies_comp = [comp_data[i][comp_data[0].index("Currency")] for i in range(1, len(comp_data) - 1)]

        # Currency data
        curr_data = aux_ws.Range("Currencies").Value
        currencies_curr = [curr_data[i][0] for i in range(1, len(curr_data) - 1)]
        exchange_curr = [curr_data[i][1] for i in range(1, len(curr_data) - 1)]
                
        # Get currency exchange
        target_currency = currencies_comp[companies_comp.index(company)]
        target_exchange = float(exchange_curr[currencies_curr.index(target_currency)])
    except:
        target_exchange = 1.0

    if company!=None:
        #Buscar si la pivot de esta company ya existe y guardar su posición
        ws2.Activate()
        a1=ws2.Range("1:1").Find(company)
        if a1!=None:
            ws2.Range("1:1").Find(company).Select()
            col1=excel.Selection.Column-1
        else:
            try:
                col1=ws2.UsedRange.Columns.Count+1
            except:
                sleep(4.0)
                col1=ws2.UsedRange.Columns.Count+1
            if col1>2:
                ws2.Cells(5,col1).End(-4159).Select()
                col3=excel.Selection.Column
                col1=col3+20
                
        #Eliminar la company actual
        if a1!=None:
            ws2.Range(ws2.Columns(col1),ws2.Columns(col1+19)).Clear()
    
    
    pt_name = pivot_sheet_name  # must be a string
    pt_rows = ["Cuenta"]  # must be a list
    pt_cols = ["Month"]  # must be a list    
    pt_filters = ["Month"]  # must be a list
    if company!=None:
        pt_filters = ["Company"]
        pt_name = pivot_sheet_name+"_"+company
        
    # [0]: field name [1]: pivot table column name [3]: calulation method [4]: number format
    pt_fields = [['Net', 'Net: sum', win32c.xlSum, '#.##0,00']]  # must be a list of lists
    
    
    ws1.Activate()
    # Delete filter if exists
    if ws1.FilterMode: ws1.ShowAllData()
    # Define last row of reporting pivot table
    try:
        ultima_fila=ws1.Range("A:A").Find("END").Row
    except:
        ultima_fila=ws1.UsedRange.Rows.Count
    
    try:
        ultima_col=ws1.Range("1:1").Find("END").Column
    except:
        ultima_col=ws1.UsedRange.Columns.Count
        while ws1.Cells(1,ultima_col).Value==None:
            ultima_col=ultima_col-1
    
    
    rango="'" + sheet_name + "'!A1:" + colnum_string(ultima_col) + str(ultima_fila) 
    
    # Insert rows for avoiding possible next PT
    ws2.Range(colnum_string(col1 + 19) + ':' + colnum_string(col1 + 59)).EntireColumn.Insert()

    pivot_table(wb, ws1, ws2, ws2_name, pt_name, pt_rows, pt_cols, pt_filters, pt_fields,rango, anno, col1)
    
    if company!=None:
        pvtTable=ws2.Cells(5,col1).PivotTable
        pvtTable.PivotFields("Company").ClearAllFilters()
        pvtTable.PivotFields("Company").CurrentPage = company

    # Delete insert rows after filtering by current year    
    ws2.Range(colnum_string(col1 + 19) + ':' + colnum_string(col1 + 59)).EntireColumn.Delete()

    wb.Save()
    wb.Close(SaveChanges=1)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()

    return target_exchange    

#Otra función para hacer la tabla dinámica del mayor
def run_excel_mayor(filename, sheet_name : str, pivot_sheet_name : str, move, company, anno, rate):
    
    df_from_excel(filename)
    # create excel object
    excel = win32.gencache.EnsureDispatch('Excel.Application')

    # excel can be visible or not
    excel.Visible =  True
    excel.DisplayAlerts = False
    # try except for file / path
    try:
        wb = excel.Workbooks.Open(filename,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {filename}')


    # set worksheet
    try:
        ws1 = wb.Sheets(sheet_name)
    except:
        sleep(0.5)
        ws1 = wb.Sheets(sheet_name)
    
    # Setup and call pivot_table
    ws2_name = pivot_sheet_name
    try:
        ws2 = wb.Sheets(ws2_name)
        ws2.Delete()
        wb.Sheets.Add().Name = ws2_name
    except:
        wb.Sheets.Add().Name = ws2_name
    
    ws2 = wb.Sheets(ws2_name)
    

    # Primero crear columna de Month2
    ## Paso1: encontrar ultima fila y columna
    ultima_fila = ws1.UsedRange.Rows.Count
    ultima_col= ws1.Cells(1,1).End(constants.xlToRight).Column
    # Find column "Fecha"
    fecha_col = ws1.Range("1:1").Find("Fecha").Column


    ##Find column Month2 if exists and delete it 
    if ws1.Range("1:1").Find("Month2") != None:
        month2_col = ws1.Range("1:1").Find("Month2").Column
        ws1.Columns(month2_col).Clear()
    else:
        month2_col = ultima_col + 1

    ## Create column "Month2"
    ws1.Cells(1, month2_col).Value = "Month2"
    ws1.Cells(2, month2_col).Value= "=eomonth(" + chr(64 + fecha_col) +"2,0)"
    ws1.Cells(2, month2_col).Copy()
    ws1.Range(ws1.Cells(3, month2_col), ws1.Cells(ultima_fila, month2_col)).PasteSpecial(Paste = constants.xlPasteFormulas, Operation = constants.xlNone)
    ws1.Range(ws1.Cells(2, month2_col), ws1.Cells(ultima_fila, month2_col)).NumberFormat = "d-mmm"

    # Select column "Net" as value filed in pivot table
    ## If currency is not EUR, create column "Net (EUR)" 
    if company != None and rate != 1.0:
        last_col = ws1.UsedRange.Columns.Count
        net_col = ws1.Range("1:1").Find("Net").Column
        
        if ws1.Range("1:1").Find("Net (EUR)") != None:
            net_eur_col = ws1.Range("1:1").Find("Net (EUR)").Column
            ws1.Columns(net_eur_col).Clear()
        else:
            net_eur_col = last_col + 1

        ws1.Cells(1, net_eur_col).Value="Net (EUR)"
        ws1.Cells(2, net_eur_col).Value="=" + chr(64+net_col) + "2*" + str(rate)
        ws1.Cells(2, net_eur_col).Copy()
        ws1.Range(ws1.Cells(3, net_eur_col), ws1.Cells(ultima_fila, net_eur_col)).PasteSpecial(Paste = constants.xlPasteFormulas, Operation = constants.xlNone)
        pt_fields = [['Net (EUR)', 'Net (EUR): sum', win32c.xlSum, '#.##0,00']]
    else:
        pt_fields = [['Net', 'Net: sum', win32c.xlSum, '#.##0,00']  ]
    
    
    pt_name = pivot_sheet_name  # must be a string
    pt_rows = ["Cuenta"]  # must be a list
    pt_cols = ["Month2"]  # must be a list
    
    
    if move=="PL":
        movimiento  ="MovePL"
    elif move=="CF":
        movimiento="MoveCF"
    
    pt_filters  =[movimiento]  
   
    # [0]: field name [1]: pivot table column name [3]: calulation method [4]: number format
    # must be a list of lists
    ultima_fila=ws1.UsedRange.Rows.Count
    ultima_col= ws1.Cells(1,1).End(constants.xlToRight).Column
    while ws1.Cells(1,ultima_col).Value=="":
        ultima_col=ultima_col-1
    
    
    rango=ws1.Range(ws1.Cells(1,1),ws1.Cells(ultima_fila,ultima_col))
    try:
        pivot_table(wb, ws1, ws2, ws2_name, pt_name, pt_rows, pt_cols, pt_filters, pt_fields,rango, anno)
    except:
        pt_cols=["Month"]
        pivot_table(wb, ws1, ws2, ws2_name, pt_name, pt_rows, pt_cols, pt_filters, pt_fields,rango, anno)
        
    pvtTable = ws2.Range("A5").PivotTable
    if move=="PL":
        pvtTable.PivotFields("MovePL").ClearAllFilters()
        pvtTable.PivotFields("MovePL").CurrentPage = "1"
        
    elif move=="CF":
        pvtTable.PivotFields("MoveCF").ClearAllFilters()
        pvtTable.PivotFields("MoveCF").CurrentPage = "1"
    
    wb.Save()
    wb.Close(SaveChanges=1)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit() 
    


def copiar_pivot_mayor(filename_inp,filename_mayor,pivot_mayor,pivot_inp,move,company):
    df_from_excel(filename_inp)
    df_from_excel(filename_mayor)
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    
    # excel can be visible or not
    excel.Visible = True
    excel.DisplayAlerts = False
    wb_inp = excel.Workbooks.Open(filename_inp,False,None)
    excel.Calculation = -4135
    wb_mayor=excel.Workbooks.Open(filename_mayor,False,None)
    
    ws_inp=wb_inp.Worksheets(pivot_inp)
    ws_mayor=wb_mayor.Worksheets(pivot_mayor)
    ultima_fila_mayor=ws_mayor.UsedRange.Rows.Count
    ultima_col_mayor=ws_mayor.UsedRange.Columns.Count
    
    col1=1
    if company!=None:
        ws_inp.Activate()
        a=ws_inp.Range("1:1").Find(company)
        if a!=None:
            ws_inp.Range("1:1").Find(company).Select()
            col1=excel.Selection.Column
        if col1>1:
            col1=excel.Selection.Column-1
        ws_inp.Cells(4,col1).End(-4161).Select()
        ultima_col_inp=excel.Selection.Column
        ws_inp.Cells(4,col1).End(-4121).Select()
        ultima_fila_inp=excel.Selection.Row
        cuentas_inp=ultima_fila_inp-5
    else:  
        ultima_col_inp=ws_inp.UsedRange.Columns.Count
        ultima_fila_inp=ws_inp.UsedRange.Row + ws_inp.UsedRange.Rows.Count -1
        cuentas_inp=ultima_fila_inp-5

    ws_mayor.Range(ws_mayor.Cells(4,1),ws_mayor.Cells(ultima_fila_mayor,ultima_col_mayor)).Copy()
    ws_inp.Paste(ws_inp.Cells(ultima_fila_inp+4,col1))
   
    cuentas_mayor=ultima_fila_mayor-5

    primera_fila_nueva = ultima_fila_inp + 4
    ultima_fila_nueva=ultima_fila_inp+5+cuentas_mayor

    primera_fila_nueva2 = ultima_fila_nueva + 4
    mid_fila_nueva2 = primera_fila_nueva2 + (ultima_fila_nueva - (primera_fila_nueva))
    ultima_fila_nueva2 = mid_fila_nueva2 + (ultima_fila_inp - 5)

    # Copy counts from report y mayor to check table
    ws_inp.Range(ws_inp.Cells(primera_fila_nueva,col1),ws_inp.Cells(ultima_fila_nueva-1,col1)).Copy()
    ws_inp.Paste(ws_inp.Cells(primera_fila_nueva2,col1))
    ws_inp.Range(ws_inp.Cells(5,col1),ws_inp.Cells(ultima_fila_inp,col1)).Copy()
    ws_inp.Paste(ws_inp.Cells(mid_fila_nueva2,col1))
    
    # convert to numbers if string
    for i in range(primera_fila_nueva2+1, ultima_fila_nueva2-1):
        try: ws_inp.Cells(i, col1).Value = int(ws_inp.Cells(i, col1).Value)
        except: pass

    # Sort counts
    ws_inp.Sort.SortFields.Clear()
    ws_inp.Sort.SortFields.Add(Key = ws_inp.Range(ws_inp.Cells(primera_fila_nueva2, col1), ws_inp.Cells(ultima_fila_nueva2-1,col1)), \
        SortOn = win32.constants.xlSortOnValues, Order = win32.constants.xlAscending, DataOption = win32.constants.xlSortNormal)
    ws_inp.Range(ws_inp.Cells(primera_fila_nueva2, col1), ws_inp.Cells(ultima_fila_nueva2-1,col1)).Sort(\
        Key1= ws_inp.Range(ws_inp.Cells(primera_fila_nueva2, col1), ws_inp.Cells(ultima_fila_nueva2-1,col1)),
        Header = win32.constants.xlYes,
        MatchCase = False,
        Orientation = win32.constants.xlTopToBottom,
        SortMethod = win32.constants.xlPinYin)
        
    # Copy header of Counts from MAYOR pivot table
    ws_inp.Range(ws_inp.Cells(ultima_fila_inp+4,col1),ws_inp.Cells(ultima_fila_inp+4,ultima_col_mayor+col1-1)).Copy()
    ws_inp.Paste(ws_inp.Cells(ultima_fila_nueva+4,col1))
    
    # Formulas for table Check # Poner fórmula a la primera celda
    formula_mayor = "IFNA(OFFSET($" + colnum_string(col1) + "$" + str(primera_fila_nueva) + \
            ",MATCH($" + colnum_string(col1) + str(primera_fila_nueva2+1) + "," + \
            "$" + colnum_string(col1) + "$" + str(primera_fila_nueva) + ":$" + colnum_string(col1) + "$" + str(ultima_fila_nueva) + ",0)-1," + \
            "MATCH(" + colnum_string(col1+1) + "$" + str(primera_fila_nueva2) + "," + \
            "$" + colnum_string(col1) + "$" + str(primera_fila_nueva) + ":$" + colnum_string(max(ultima_col_mayor,ultima_col_inp)) + "$" + str(primera_fila_nueva) + ",0)-1),0)"
    formula_inp = "IFNA(OFFSET($" + colnum_string(col1) + "$4" + \
            ",MATCH($" + colnum_string(col1) + str(primera_fila_nueva2 + 1) + ",$" + colnum_string(col1) + "$4:" + \
            "$" + colnum_string(col1) + "$" + str(ultima_fila_inp) + ",0)-1,MATCH(" + \
            colnum_string(col1+1) + "$" + str(primera_fila_nueva2) + ",$" + colnum_string(col1) + "$4:$" + \
            colnum_string(max(ultima_col_mayor,ultima_col_inp)) + "$4,0)-1),0)"
    
    if pivot_mayor == "CheckCF":
        formula = "=" + formula_mayor + "+" + formula_inp
    if pivot_mayor == "CheckPL":
        formula = "=" + formula_mayor + "-" + formula_inp
        
    # Apply formula and arrastrar
    ws_inp.Cells(ultima_fila_nueva+5,col1+1).Value = formula
    ws_inp.Range(colnum_string(col1+1)+str(ultima_fila_nueva+5)).Copy()
    ws_inp.Range(colnum_string(col1+1)+str(ultima_fila_nueva+6)+":"+colnum_string(col1+1) +str(ultima_fila_nueva2)).PasteSpecial(Paste = constants.xlPasteFormulas,Operation = constants.xlNone)
    ws_inp.Range(colnum_string(col1+1)+str(ultima_fila_nueva+5)+":"+colnum_string(col1+1) +str(ultima_fila_nueva2)).Copy()
    ws_inp.Range(colnum_string(col1+2)+str(ultima_fila_nueva+5)+":"+colnum_string(ultima_col_mayor+col1-1)+str(ultima_fila_nueva2)).PasteSpecial(Paste = constants.xlPasteFormulas,Operation = constants.xlNone)
    
    # Paste dates format -> xlPasteFormats
    ws_inp.Range(colnum_string(col1+1)+str(4)).Copy()
    ws_inp.Range(colnum_string(col1+1)+str(primera_fila_nueva2)+":"+colnum_string(ultima_col_mayor+col1-2) +str(primera_fila_nueva2)).PasteSpecial(Paste = win32.constants.xlPasteFormats)
    ws_inp.Range(colnum_string(col1+1)+str(primera_fila_nueva)+":"+colnum_string(ultima_col_mayor+col1-2) +str(primera_fila_nueva)).PasteSpecial(Paste = win32.constants.xlPasteFormats)
    # Copy number format
    ws_inp.Range(colnum_string(col1+1)+str(primera_fila_nueva+1)).Copy()
    ws_inp.Range(colnum_string(col1+1)+str(primera_fila_nueva2+1)+":"+colnum_string(ultima_col_mayor+col1-1) +str(ultima_fila_nueva2-1)).PasteSpecial(Paste = win32.constants.xlPasteFormats)
    # Paste last row format
    ws_inp.Range(colnum_string(col1)+str(ultima_fila_nueva)+":"+colnum_string((col1 - 1) + ultima_col_mayor) +str(ultima_fila_nueva)).Copy()
    ws_inp.Range(colnum_string(col1)+str(ultima_fila_nueva2)+":"+colnum_string((col1 - 1) + ultima_col_mayor) +str(ultima_fila_nueva2)).PasteSpecial(Paste = win32.constants.xlPasteFormats)


    #Pintar cuentas diferentes
    #Primero convertir las cuentas copiadas y pegadas a números
    ws_inp.Range(ws_inp.Cells(ultima_fila_inp+5,col1),ws_inp.Cells(4+ultima_fila_inp+cuentas_mayor,col1)).TextToColumns(Destination=ws_inp.Range(ws_inp.Cells(ultima_fila_inp+5,col1),ws_inp.Cells(ultima_fila_inp+5,col1)), DataType=constants.xlDelimited,TextQualifier=constants.xlDoubleQuote,ConsecutiveDelimiter=False,
                                                                                Tab=False,Semicolon=False, Comma=False, Space=False, Other=False,    TrailingMinusNumbers=True)
    
    #Para cuentas del inp: desde la fila 5 hasta 4+cuentas_inp    
    lista_inp=ws_inp.Range(ws_inp.Cells(5,col1),ws_inp.Cells(4+cuentas_inp,col1)).Value
    
    #Para cuentas del mayor: desde la fila ultima_fila_inp +5 hasta ultima_fila_inp+4+cuentas_mayor
    lista_mayor=ws_inp.Range(ws_inp.Cells(ultima_fila_inp+5,col1),ws_inp.Cells(4+ultima_fila_inp+cuentas_mayor,col1)).Value
    
    lista_inp2=[]
    lista_mayor2=[]
    if type(lista_inp)==tuple:
        for tup in lista_inp:
            lista_inp2.append(tup[0])
    else:
        lista_inp2.append(lista_inp)
        
    if type(lista_mayor)==tuple:
        for tup in lista_mayor:
            lista_mayor2.append(tup[0])
    else:
        lista_mayor2.append(lista_mayor)
        
    dif=list(set(lista_inp2)-set(lista_mayor2))
    
    for cuenta in dif:
        fila=ws_inp.Range(ws_inp.Cells(1,col1),ws_inp.Cells(ultima_fila_nueva2,col1)).Find(cuenta,LookAt=win32.constants.xlWhole).Row
        ws_inp.Cells(fila,col1).Interior.Color=65535

    dif=list(set(lista_mayor2)-set(lista_inp2))
    for cuenta in dif:
        fila=ws_inp.Range(ws_inp.Cells(1,col1),ws_inp.Cells(ultima_fila_nueva2,col1)).Find(cuenta,LookAt=win32.constants.xlWhole).Row
        ws_inp.Cells(fila,col1).Interior.Color=65535
    
    ws_inp.Range(ws_inp.Cells(primera_fila_nueva2+1, col1), ws_inp.Cells(ultima_fila_nueva2,(col1 - 1) + ultima_col_mayor)).RemoveDuplicates(1)
    # Hide Gridlines in ws
    ws_inp.Activate()
    excel.ActiveWindow.DisplayGridlines = False
    excel.Calculation = -4105
    ws_mayor.Activate()
    excel.ActiveWindow.DisplayGridlines = False
    
    # Save and Close wbs
    wb_inp.Save()
    wb_mayor.Close()
    wb_inp.Close()
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()
    

def colnum_string(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string    