# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 11:29:36 2021

@author: romina
"""

import os
from datetime import datetime
from io import BytesIO
from PIL.Image import new
from PIL.ImageDraw import Draw
import PySimpleGUI as sg
from getpass import getuser
from funciones import insertar_filas
import xlwings as xl
from funciones import lista_asignacion
from funciones import hoja_mayor
from funciones_esconder import esconder_meses
from funciones_check import run_excel,run_excel_mayor,copiar_pivot_mayor
from sys import exc_info
from time import time as time2
from time import sleep
import pandas as pd
from os.path import basename
from validated import insertar_val_pl
from Wolfram.InputPL_InputCF.dotgis import insertar_pl
from build38_pipe import build38_pipe

from Wolfram.InputPC.InputPC import InputPC

import ctypes
myappid = 'mycompany.myproduct.subproduct.version' # arbitrary string
ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)



clientes=["Gelt","uSizy","Nora","Idoven","Gasmobi","Validated ID","Graphext",
          "Plantarse","DotGIS","Clarity","Agrosingularity","Nova","Nubentos",
          "Build38","Camelina","iFeel","Circular","Payflow","Iomob",
          "Other","Baia", "MediQuo", "The Startup CFO", "Katoo", "Hubuc", 
          "Luca", "Liux", "Geoblink", "Wheels", "Innitius", "Tenzir", "eKuore",
          "ClimateTrade", "Nebeus", "AM Polymers", "Triditive", "Amphora"]

clientes=sorted(clientes, key=lambda v: v.upper())

def controlar_fechas(doc,hoja):
    df=pd.read_excel(doc,sheet_name=hoja)
    df=df.drop(df[df["Fecha"]=="END"].index)
    try:
        columna_fecha=pd.to_datetime(df["Fecha"],format='%d/%m/%Y')
        a=True
    except:
        try:
            columna_fecha=pd.to_datetime(df["Fecha"],format='%d-%m-%Y')
            a=True
        except:
            a=False
    return a

def CBtn(BoxText):
    return sg.Checkbox(BoxText, size=(8, 1), default=False,key=BoxText)

def df_from_excel(path):
    app = xl.App(visible=False)
    book = app.books.open(path)
    book.save()
    app.kill()

def icon(check):
    box = (25, 25)
    background = (255, 255, 255, 0)
    rectangle = (3, 3, 29, 29)
    line = ((9, 17), (15, 9), (23, 9))
    im = new('RGBA', box, background)
    draw = Draw(im, 'RGBA')
    draw.rectangle(rectangle, outline='black', width=1)
    if check == 1:
        draw.line(((7, 9), (12, 17), (22, 4)), fill='black', width=2, joint='curve')
        # draw.rectangle([(8, 8), (22, 22)], fill='black', width=1)
    elif check == 2:
        draw.line(line, fill='grey', width=3, joint='curve')
    with BytesIO() as output:
        im.save(output, format="PNG")
        png = output.getvalue()
    return png

check = [icon(0), icon(1), icon(2)]

tmp_path= 'c:/tmp/'
if not os.path.isdir(tmp_path): os.mkdir(tmp_path)

heading1 = ['Column Name']
titulos = ['Sheets']

def nombre_sugerido(url_path):
    old_name=basename(url_path)
    largo=len(old_name)
    if old_name[largo-7:largo][0]=="v" or old_name[largo-8:largo][0]=="v":
        version=old_name[largo-8:old_name.index(".")]
        pos=version.index("v")
        name=old_name[0:largo-8]
        if pos!=0:
            version=old_name[largo-7:old_name.index(".")]
            name=old_name[0:largo-7]
        num=int(version[1:len(version)])
        num=num+1
        new_name=name + "v" + str(num) 
        
    else:
        name=old_name[0:old_name.index(".")]
        new_name=name + "_v1"
        
    new_url=url_path.replace(old_name, "") # .strip(old_name)
        
    return new_name, new_url
        

def win3(mayor_path,nominas_path,input_path,lista_hojas,treedataMeses,treedataPL,treedataCF,
         input_hojaPL,input_hojaCF,input_hojaPC,checkPL,checkCF,company1,move_pl,move_cf,move_pc,
         meses,other,mes,anno,client,mayor_pl_dotgis=None,mayor_hoja_dotgis=None,ventas=None,compras=None,
         hoja_report_ventas=None,hoja_report_compras=None,pipe=None,lost=None,won=None,hoja_pipe=None):
    """
    

    Parameters
    ----------
    mayor_path : url del libro mayor - str
    input_path : url del reporting - str
    lista_hojas : lista de hojas del mayor - list
    treedataMeses : metadata con hojas del reporting 
    treedataPL : metadata con títulos de la hoja InputPL
    treedataCF : metadata con títulos de la hoja InputCF
    input_hojaPL : str
    input_hojaCF : str
    checkPL : boolean
    checkCF : boolean
    company1 : str
    move_pl : boolean
    move_cf : boolean
    meses : boolean
    mes : str 
    anno : str
    move_pc : boolean
    client: str

    Returns
    -------
    excel workbook

    """
    
  
    new_name,new_url=nombre_sugerido(input_path)
    
    column1= [[sg.T("")],[sg.T(' Save in:        ', pad=(0,10)), sg.Radio('default (Reporting folder)', "RADIO2", default=True, key="-R10-"),
            sg.T(' or '), sg.FolderBrowse(key="-IN3-", target=("-IN4-")) , sg.Input(key="-IN4-", size=(35,1))],
            [sg.T('Save as:      '), sg.InputText(new_name,size=(65,1), key="new_name")]]

    columna_ledger=[[sg.Text("Ledger sheet:",font=("Helvetica","10","bold")),sg.Combo(lista_hojas,key="hoja_mayor")]]
    columna_ledger_asig=[[sg.Text("Ledger sheet:",font=("Helvetica","10","bold")),sg.Combo(lista_hojas,key="hoja_mayor2"), sg.Text("             Assignment method:",font=("Helvetica","10","bold")),sg.Combo(["Machine learning", "ML (new preprocessing)", "Words similarity (%)"],key="predictor")]]
    build_column=[[sg.T("")],[sg.Text("        Pipe:        "), sg.Input(), sg.FileBrowse(key="pipe")],
            [sg.T("")],[sg.Text("        Lost:       "), sg.Input(), sg.FileBrowse(key="lost")],
            [sg.T("")],[sg.Text("        Won:      "), sg.Input(), sg.FileBrowse(key="won")],
            [sg.T("")],[sg.Text('        Reporting pipe sheet:       '), sg.InputText("Pipe",key="hoja_pipe")]]
    
    usizy_column=[[sg.T("")],[sg.Text('        Accounts sheet:       '), sg.InputText("Accounts",key="accounts_usizy")],
            [sg.T("")],[sg.Text("        Holded workbook:       "), sg.Input(), sg.FileBrowse(key="holded_usizy")]]
    
    if move_pl==True or move_cf==True:
        pedir_ledger=False
        pedir_ledger_asig=True
    
    elif (move_pl==False and move_cf==False) and (checkPL==True or checkCF==True):
        pedir_ledger=True
        pedir_ledger_asig=False
    else:
        pedir_ledger=False
        pedir_ledger_asig=False
    
    if move_pl==True or move_cf==True:
        nuevo_doc=True
    else:
        nuevo_doc=False
        
    if client=="Build38":
        build=True
    else:
        build=False
        
    if client=="uSizy":
        usizy=True
    else:
        usizy=False
    
    #Esta ventana es para cuando se marcan las opciones insertar filas PL, CF y esconder meses
    layout2=[ 
            [sg.T("")], 
            [sg.Column(columna_ledger,visible=pedir_ledger)],
            [sg.Column(columna_ledger_asig,visible=pedir_ledger_asig)],
            [sg.T("")], 
            [sg.Text('PL automatic assignment:         ',font=("Helvetica","10","bold")),sg.Text('CF automatic assignment:    ',font=("Helvetica","10","bold")),sg.Text(" Sheets to hide months: ",font=("Helvetica","10","bold"))],
            [sg.Tree(data=treedataPL, headings=heading1[1:1], auto_size_columns=True,
            num_rows=10, col0_width=20, key='-TREEPL-', row_height=20, metadata=[],
            show_expanded=False, enable_events=move_pl,
            select_mode=sg.TABLE_SELECT_MODE_BROWSE), sg.Tree(data=treedataCF, headings=heading1[1:1], auto_size_columns=True,
            num_rows=10, col0_width=20, key='-TREECF-', row_height=20, metadata=[],
            show_expanded=False, enable_events=move_cf,
            select_mode=sg.TABLE_SELECT_MODE_BROWSE), sg.Tree(data=treedataMeses, headings=titulos[1:1], auto_size_columns=True,
            num_rows=10, col0_width=20, key='-TREEMeses-', row_height=20, metadata=[],
            show_expanded=False, enable_events=meses,
            select_mode=sg.TABLE_SELECT_MODE_BROWSE)],
             [sg.Column(usizy_column,visible=usizy)],
            [sg.Column(column1,visible=nuevo_doc)],
            [sg.T("                                   ",pad=(40,30,100,100)), sg.Button("Submit",key="submit2"), sg.T("  "), sg.Button("Cancel")]
             ]
            
    win2 = sg.Window('Insert rows PL, CF and hide months', layout2,icon="tsc.ico",finalize=True)
    treeCF = win2['-TREECF-']
    treeCF.Widget.heading("#0", text=heading1[0]) # Set heading for column #0
    treePL = win2['-TREEPL-']
    treePL.Widget.heading("#0", text=heading1[0]) # Set heading for column #0
    treeMeses = win2['-TREEMeses-']
    treeMeses.Widget.heading("#0", text=titulos[0]) # Set heading for column #0

    while True:
        ev2, vals2 = win2.read()
        if ev2 == sg.WIN_CLOSED or ev2 == 'Cancel':
            win2.Close()
            break
        elif ev2=='-TREEPL-':
            titulo = vals2['-TREEPL-'][0]
            
            if titulo in treePL.metadata:
                treePL.metadata.remove(titulo)
                treePL.update(key=titulo, icon=check[0])
            else:
                treePL.metadata.append(titulo)
                treePL.update(key=titulo, icon=check[1])
        elif ev2=="-TREECF-":
            titulo1 = vals2['-TREECF-'][0]
            
            if titulo1 in treeCF.metadata:
                treeCF.metadata.remove(titulo1)
                treeCF.update(key=titulo1, icon=check[0])
            else:
                treeCF.metadata.append(titulo1)
                treeCF.update(key=titulo1, icon=check[1])
        elif ev2=="-TREEMeses-":
            titulo1 = vals2['-TREEMeses-'][0]
            
            if titulo1 in treeMeses.metadata:
                treeMeses.metadata.remove(titulo1)
                treeMeses.update(key=titulo1, icon=check[0])
            else:
                treeMeses.metadata.append(titulo1)
                treeMeses.update(key=titulo1, icon=check[1])
        
        elif ev2=="submit2":
            
            filename=vals2["new_name"]
            suffix=".xlsx"       
            if (vals2["-IN4-"])!="":
                save_to_path=vals2["-IN4-"]+"/"
               
            elif vals2["-IN4-"]=="":
                save_to_path= new_url

            output_filename=save_to_path+filename+suffix
            
            mayor_hoja=vals2["hoja_mayor"]
            if mayor_hoja=="":
                mayor_hoja=vals2["hoja_mayor2"]
            lista_hojas_esconder_meses=treeMeses.metadata
            predictor=vals2["predictor"]
            deter_coeff2="None"
            deter_coeff1="None"            
            
            if mayor_hoja=="" and (move_pl==True or move_cf==True or checkPL==True or checkCF==True):
                sg.Popup("Missing:","Choose a ledger sheet")
            elif len(lista_hojas_esconder_meses)==0 and meses==True:
                sg.Popup('Missing:','Choose at least one sheet to hide months')
            
            else:    
                t0=time2()
                
                if move_pl==True and client!="DotGIS" and client!="Validated ID":
                    try:
                        lista_input_pl=treePL.metadata
                        deter_coeff1 = insertar_filas(mayor_path, mayor_hoja, input_path, input_hojaPL, "PL", output_filename, int(mes), int(anno), lista_input_pl, company=company1,predictor=predictor, both_moves = move_pl and move_cf)
                        if move_pl and move_cf:
                            df_from_excel(tmp_path + basename(output_filename))
                        else:
                            df_from_excel(output_filename)
                    except:
                        sg.Popup("Unexpected error inserting PL rows:", exc_info()[1])
                
                if move_pl==True and client=="DotGIS":
                    try:
                        insertar_pl(input_path,mayor_path,"PivotTable",input_hojaPL,output_filename)
                    except:
                        sg.Popup("Unexpected error inserting PL rows:","DotGIS", exc_info()[1])
                    lista_input_pl=[]
                    deter_coeff1="None"
                if move_pl==True and client=="Validated ID":
                    try:
                        insertar_val_pl(input_path,mayor_pl_dotgis,mayor_hoja_dotgis,input_hojaPL,output_filename)
                    except:
                            sg.Popup("Unexpected error inserting PL rows:","Validated ID", exc_info()[1])
                    lista_input_pl=[]
                    deter_coeff1="None"
                if move_cf==True and move_pl==True:
                    try:
                        lista_input_cf=treeCF.metadata
                        deter_coeff2 = insertar_filas(mayor_path, mayor_hoja, output_filename, input_hojaCF, "CF", output_filename, int(mes), int(anno), lista_input_cf, company=company1, predictor=predictor, both_moves = True)
                    except:
                        sg.Popup("Unexpected error inserting CF rows:", exc_info()[2])
                elif move_cf==True and move_pl==False:
                    # try:
                        lista_input_cf=treeCF.metadata
                        deter_coeff2 = insertar_filas(mayor_path, mayor_hoja, input_path, input_hojaCF, "CF", output_filename, int(mes), int(anno), lista_input_cf, company=company1, predictor=predictor)
                        df_from_excel(output_filename)
                    # except:
                    #     sg.Popup("Unexpected error inserting CF rows:", exc_info()[1])
                
                if meses==True and (move_pl==True or move_cf==True):
                    try:
                        esconder_meses(mes,anno,output_filename,lista_hojas_esconder_meses)
                    except:
                        sg.Popup("Unexpected error hidding months:", exc_info()[1])
                        
                elif meses==True and (move_pl==False or move_cf==False):
                    try:
                        esconder_meses(mes,anno,input_path,lista_hojas_esconder_meses)
                    except:
                        sg.Popup("Unexpected error hidding months:", exc_info()[1])
                        
                
                  
                if checkPL==True and (move_pl==True or move_cf==True):
                    try:
                        currency_exchange = run_excel(output_filename, input_hojaPL, "CheckPL",company1, anno)
                        run_excel_mayor(mayor_path, mayor_hoja, "CheckPL","PL",company1, anno, currency_exchange)
                        copiar_pivot_mayor(output_filename,mayor_path,"CheckPL","CheckPL","PL",company1)
                        df_from_excel(output_filename)
                    except:
                        sg.Popup("Unexpected error --CheckPL:", exc_info()[1])
                        
                    
                if checkCF==True and (move_pl==True or move_cf==True):
                    try:
                        currency_exchange = run_excel(output_filename, input_hojaCF, "CheckCF",company1, anno)
                        run_excel_mayor(mayor_path, mayor_hoja, "CheckCF","CF",company1, anno, currency_exchange)
                        copiar_pivot_mayor(output_filename,mayor_path,"CheckCF","CheckCF","CF",company1)
                        df_from_excel(output_filename)
                    except:
                        sg.Popup("Unexpected error --CheckCF:", exc_info()[1])
                        
                if checkPL==True and move_pl==False and move_cf==False:
                    try:
                        currency_exchange = run_excel(input_path, input_hojaPL, "CheckPL",company1, anno)
                        run_excel_mayor(mayor_path, mayor_hoja, "CheckPL","PL",company1, anno, currency_exchange)
                        copiar_pivot_mayor(input_path,mayor_path,"CheckPL","CheckPL","PL",company1)
                        df_from_excel(input_path)
                    except:
                        sg.Popup("Unexpected error --CheckPL:", exc_info()[1])
                        
                    
                if checkCF==True and move_pl==False and move_cf==False:
                    try:
                        currency_exchange = run_excel(input_path, input_hojaCF, "CheckCF",company1, anno)
                        run_excel_mayor(mayor_path, mayor_hoja, "CheckCF","CF",company1, anno, currency_exchange)
                        copiar_pivot_mayor(input_path,mayor_path,"CheckCF","CheckCF","CF",company1)
                        df_from_excel(input_path)
                    except:
                        sg.Popup("Unexpected error --CheckCF:", exc_info()[1])

                # Insert rows to InputPC             
                if move_pc==True and (move_cf==True or move_pl==True): 
                    InputPC(client, company1, nominas_path, output_filename, input_hojaPC, anno, mes).add_to_reporting()
                if move_pc==True and (move_cf==False and move_pl==False):
                    InputPC(client, company1, nominas_path, input_path, input_hojaPC, anno, mes).add_to_reporting()

                
                if other==True and (move_pl==True or move_cf==True):
                    if client=="Build38":
                        build38_pipe(pipe,lost,won,output_filename,hoja_pipe,mes,anno)

                if other==True and (move_pl==False and move_cf==False):
                    if client=="Build38":
                        build38_pipe(pipe,lost,won,input_path,hoja_pipe,mes,anno)
                    
                
                
                t1=time2()
                tiempo="- Time: " +str(round(t1-t0,2)) + " sec."
                win2.close()
                ML_msg =  ""
                try:
                    if move_pl==True:
                        ML_msg =  ML_msg + " · InputPL:\n"
                        for i in range(len(lista_input_pl)):
                            ML_msg = ML_msg + "   - " + lista_input_pl[i] + " = " + "%.1f" % (deter_coeff1[i] * 100) + "%\n"
                    if move_cf==True:
                        ML_msg =  ML_msg + " · InputCF:\n"
                        for i in range(len(lista_input_cf)):
                            ML_msg = ML_msg + "   - " + lista_input_cf[i] + " = " + "%.1f" % (deter_coeff2[i] * 100) + "%\n"    
                    
                    sg.Popup(tiempo, ML_msg) if (move_pl == True or move_cf==True) else sg.Popup(tiempo)
                except:
                    sg.Popup(tiempo, "ERROR")
    

def insertar_validated_pl(input_path,input_hojaPL,move_cf,meses,mayor_path,lista_hojas,
                          treedataCF,input_hojaCF,check_cf,treedataMeses,nominas_path,
                          treedataPL,input_hojaPC,move_pc,mes,anno,client,other):
    
    # Ventana tipo popup para cuando se marcan la opción de insertar PL Validated ID y también se marcó insertar CF
    
    layout2=[[sg.T("")],[sg.Text("           P&L monthly:     "), sg.Input(), sg.FileBrowse(key="mayor_dotgis_pl")],
             [sg.Text('           Sheet name:      '), sg.InputText("PyG",key="mayor_hoja_pl")],
             
         [sg.T("                                   ",pad=(40,30,100,100)), sg.Button("Submit",key="submit2"), sg.T("  "), sg.Button("Cancel")]
         ]
    win2 = sg.Window('P&L - Validated ID', layout2,icon="tsc.ico",finalize=True,resizable=True)  
    
    check_pl=False
    company1=None
    
    while True:
        ev2, vals2 = win2.read()
        
                    
        if ev2=="submit2":
                mayor_dotgis_pl=vals2["mayor_dotgis_pl"]
                mayor_hoja_pl=vals2["mayor_hoja_pl"]
                win3(mayor_path,nominas_path,input_path,lista_hojas,treedataMeses,treedataPL,treedataCF,
                     input_hojaPL,input_hojaCF,input_hojaPC,check_pl,check_cf,company1,True,move_cf,move_pc,
                     meses,other,mes,anno,client,mayor_pl_dotgis=mayor_dotgis_pl,mayor_hoja_dotgis=mayor_hoja_pl)  
                
                
        elif ev2 == sg.WIN_CLOSED or ev2 == 'Cancel':
            
            break



def win_agro(mes,anno,input_path,mayor_path,nominas_path,lista_hojas,treedataMeses,treedataPL,treedataCF,
            input_hojaPL,input_hojaCF,input_hojaPC,check_pl,check_cf,company1,move_pl,move_cf,move_pc,
            meses,client,otros):
    layout=[[sg.Titlebar("REPORTING AGROSINGULARITY")],
            [sg.T("")],[sg.Text("        Sales stock excel:            "), sg.Input(), sg.FileBrowse(key="ventas")],
            [sg.T("")],[sg.Text("        Purchases stock excel:     "), sg.Input(), sg.FileBrowse(key="compras")],
            [sg.Text('        Reporting sales sheet:       '), sg.InputText("Sales",key="hoja_report_ventas")],
            [sg.Text('        Reporting purchases sheet:'), sg.InputText("Purchases",key="hoja_report_compras")],
            [sg.T("")],
              [sg.T("                                   ",pad=(40,50,100,100)), sg.Button("Submit",key="submit2"), sg.T("  "), sg.Button("Cancel")]
            
            ]       

    win2 = sg.Window('Agrosingularity', layout,icon="tsc.ico",size=(650,345),finalize=True,resizable=True)        
    
    while True:
        ev2, vals2 = win2.read()
        if ev2 == sg.WIN_CLOSED or ev2 == 'Cancel':
            break
        
                    
        elif ev2=="submit2":
                ventas=vals2["ventas"]
                compras=vals2["compras"]      
                hoja_report_ventas=vals2["hoja_report_ventas"]
                hoja_report_compras=vals2["hoja_report_compras"]
                
                win3(mayor_path,nominas_path,input_path,lista_hojas,treedataMeses,treedataPL,treedataCF,
                     input_hojaPL,input_hojaCF,input_hojaPC,check_pl,check_cf,company1,move_pl,move_cf,move_pc,
                     meses,otros,mes,anno,client,ventas=ventas,compras=compras,
                     hoja_report_ventas=hoja_report_ventas,hoja_report_compras=hoja_report_compras)  
                
                     

def win_build38(mayor_path,nominas_path,input_path,lista_hojas,treedataMeses,treedataPL,treedataCF,
                     input_hojaPL,input_hojaCF,input_hojaPC,check_pl,check_cf,company1,move_pl,move_cf,move_pc,
                     meses,mes,anno,client,otro):
    layout=[[sg.Titlebar("REPORTING BUILD38")],
            [sg.T("")],[sg.Text("        Pipe:        "), sg.Input(), sg.FileBrowse(key="pipe")],
            [sg.T("")],[sg.Text("        Lost:       "), sg.Input(), sg.FileBrowse(key="lost")],
            [sg.T("")],[sg.Text("        Won:      "), sg.Input(), sg.FileBrowse(key="won")],
            [sg.T("")],[sg.Text('        Reporting pipe sheet:       '), sg.InputText("Pipe",key="hoja_pipe")],
            [sg.T("")],
              [sg.T("                                   ",pad=(40,50,100,100)), sg.Button("Submit",key="submit2"), sg.T("  "), sg.Button("Cancel")]
            
            ]       

    win2 = sg.Window('Build38', layout,icon="tsc.ico",size=(650,345),finalize=True,resizable=True)        
    
    while True:
        ev2, vals2 = win2.read()
        if ev2 == sg.WIN_CLOSED or ev2 == 'Cancel':
            break
        
                    
        elif ev2=="submit2":
                pipe=vals2["pipe"]
                lost=vals2["lost"]      
                won=vals2["won"]
                hoja_pipe=vals2["hoja_pipe"]
            
                win3(mayor_path,nominas_path,input_path,lista_hojas,treedataMeses,treedataPL,treedataCF,
                     input_hojaPL,input_hojaCF,input_hojaPC,check_pl,check_cf,company1,move_pl,move_cf,move_pc,
                     meses,otro,mes,anno,client,pipe=pipe,lost=lost,won=won,hoja_pipe=hoja_pipe)  





def main():
    sg.theme("LightGrey1")
    sg.set_options(font=('Helvetica', 10))
    columna1=[[sg.T("         "), sg.Checkbox('Insert rows to InputPL', default=True, key="pl")],
          [sg.T("         "), sg.Checkbox('Insert rows to InputCF', default=True, key="cf")],
          [sg.T("         "), sg.Checkbox('Insert rows to InputPC', default=True, key="pc")],]
    columna2=[[sg.T("         "), sg.Checkbox('Hide months', default=True, key="meses")],
              [sg.T("         "), sg.Checkbox('CheckPL', default=True, key="check_pl")], 
              [sg.T("         "), sg.Checkbox('CheckCF', default=True, key="check_cf")],]
    columna3=[[sg.T("      "), sg.Checkbox('Other', default=False, key="otros",tooltip="Avaliable for Build38 and Usizy")],
    [sg.T("      "),sg.Text("   options",tooltip="Avaliable for Build38 and Usizy")]]
    
    layout1 = [[sg.T("")],[sg.Text("           CLIENT:          "),sg.Combo(clientes,key="client",), sg.Text("    Month:"),sg.Combo([1,2,3,4,5,6,7,8,9,10,11,12],default_value = str(datetime.now().month-1), key="mes"),sg.Text("Year:"),sg.InputText(str(datetime.now().year),key="anno",size=(10,1))] ,
               [sg.T("")],[sg.Text("           LEDGER:        "), sg.Input(), sg.FileBrowse(key="mayor")],
               [sg.Text("           PAYROLL:      "), sg.Input(), sg.FileBrowse(key="nominas")],
              [sg.Text("           REPORT:        "), sg.Input(), sg.FileBrowse(key="input")],
              [sg.T("")],
              [sg.Text('           InputPL:'), sg.InputText("InputPL",key="input_hojaPL",size=(10,1)),
               sg.Text('     InputCF:'), sg.InputText("InputCF",key="input_hojaCF",size=(10,1)),
               sg.Text('     InputPC:'), sg.InputText("InputPC",key="input_hojaPC",size=(10,1))],
              [sg.T("")],
              [sg.Text('           Multiple companies?         '),
               sg.Radio('No      ', "RADIO1", default=True),sg.Radio('Yes     ', "RADIO1", default=False, key="-IN2-"),sg.Text(" Company:"),sg.InputText(key="company",size=(9,1))],
              [sg.T("")],
              [sg.Text("            Options:",font=('Helvetica', 12))],  
              [sg.Column(columna1),sg.Column(columna2),sg.Column(columna3)],
              [sg.T("")],
              [sg.T("                                   ",pad=(40,50,100,100)), sg.Button("Submit",key="submit1"), sg.T("  "), sg.Button("Cancel")]
             ]
    win1 = sg.Window("Wolfram", layout1, icon="tsc.ico", auto_size_text=18, resizable=True, finalize=True, size=(600,550))
    win1.normal()

    while True:
        event1, values1 = win1.read()

        if event1 == sg.WIN_CLOSED:
            break
        elif event1 == "Cancel":
            win1.close()
            break

        elif event1=="submit1":
            input_path=values1["input"]
            nominas=values1["nominas"]
            move_cf=values1["cf"]
            move_pl=values1["pl"]
            move_pc=values1["pc"]
            check_cf=values1["check_cf"]
            check_pl=values1["check_pl"]
            meses=values1["meses"]
            mayor_path=values1["mayor"]
            cliente=values1["client"]
            mes=values1["mes"]
            anno=values1["anno"]
            otros=values1["otros"]
            treedataMeses=None
            treedataCF=None
            inppl_titulos=[]
            
            
            treedataMeses = sg.TreeData()
            treedataMeses.Insert('', "None", "None", values=[1,2,3],
                    icon=check[0])
            treedataCF = sg.TreeData()
            treedataCF.Insert('', "None", "None", values=[1,2,3],
                    icon=check[0])
            treedataPL = sg.TreeData()
            treedataPL.Insert('', "None", "None", values=[1,2,3],
                    icon=check[0])
            
            lista_hojas=[]
            
            if values1["-IN2-"]==True:
                company1=values1["company"]
                if company1=="":
                    sg.Popup("Company missing")
            else:
                company1=None

            
            if move_cf==True or move_pl==True or check_cf==True or check_pl==True:
                try: 
                    sleep(0.05)
                    lista_hojas=hoja_mayor(mayor_path)
                except:
                    sg.Popup("Unexpected error - List of Mayor sheets:", exc_info()[1])
                    break
                    
            
            if meses==True:
                try: 
                    lista_hojas_input=hoja_mayor(input_path)
                except:
                    sg.Popup("Unexpected error - List of sheets to hide months:", exc_info()[1])
                    break
                data=lista_hojas_input
                treedataMeses = sg.TreeData()
                for titulo in data:
                    treedataMeses.Insert('', titulo, titulo, values=[1,2,3],
                    icon=check[0])
            
            
            input_hojaCF=values1["input_hojaCF"]
            input_hojaPL=values1["input_hojaPL"]
            input_hojaPC=values1["input_hojaPC"]
            
           
            if move_cf==True:
                try:
                    inpcf_titulos=lista_asignacion(input_path, input_hojaCF)
                except:
                    sg.Popup("Unexpected error - CF List:", exc_info()[1])
                    break
                
                data=inpcf_titulos
                treedataCF = sg.TreeData()
                for titulo in data:
                    treedataCF.Insert('', titulo, titulo, values=[1,2,3],
                    icon=check[0])
                
                if move_cf==True and ("Fecha" not in inpcf_titulos or "Concepto" not in inpcf_titulos or "Net" not in inpcf_titulos or "Cuenta" not in inpcf_titulos  or "Debe" not in inpcf_titulos or "Haber" not in inpcf_titulos) :
                        sg.Popup("One of the following columns doesn't exist in InputCF:","Fecha","Net","Concepto","Cuenta","Debe","Haber")    
                  
                
                
            if move_pl==True and (cliente!="Validated ID" and cliente!="DotGIS"): #Porque son los 3 que tienen InputCF diferente y no necesitan asignacion aut
                try:
                    inppl_titulos=lista_asignacion(input_path, input_hojaPL)
                except:
                    sg.Popup("Unexpected error - PL List:", exc_info()[1])
                    break
                data=inppl_titulos
                treedataPL = sg.TreeData()
                for titulo in data:
                    treedataPL.Insert('', titulo, titulo, values=[1,2,3],
                    icon=check[0])
    
                if move_pl==True and ("Fecha" not in inppl_titulos or "Concepto" not in inppl_titulos or "Net" not in inppl_titulos or "Cuenta" not in inppl_titulos  or "Debe" not in inppl_titulos or "Haber" not in inppl_titulos):
                        sg.Popup("One of the following columns doesn't exist in InputPL:","Fecha","Net","Concepto","Cuenta","Debe","Haber")
                    
                         
            if input_path=="":
                sg.Popup('Choose report')
           
            elif (move_pl==True or check_pl==True) and input_hojaPL=="":
                sg.Popup("Missing: InputPL sheet name")
                
            elif (move_cf==True or check_cf==True) and input_hojaCF=="":
                sg.Popup("Missing: InputCF sheet name")
                
            elif (move_cf==True or move_pl==True or check_cf==True or check_pl==True) and mayor_path=="":
                sg.Popup("Choose Mayor")
            elif move_pc==True and nominas=="":
                sg.Popup("Choose a payroll document first")
            elif mes=="":
                sg.Popup("Month missing")
            elif anno=="":
                sg.Popup("Year missing")
                
            
            elif cliente=="Validated ID" and move_pl==True:
                insertar_validated_pl(input_path,input_hojaPL,move_cf,meses,mayor_path,lista_hojas,
                                      treedataCF,input_hojaCF,check_cf,treedataMeses,nominas,
                                      treedataPL,input_hojaPC,move_pc,mes,anno,cliente,otros)
            elif cliente=="Agrosingularity" and otros==True:
                win_agro(mes,anno,input_path,mayor_path,nominas,lista_hojas,treedataMeses,treedataPL,treedataCF,
                         input_hojaPL,input_hojaCF,input_hojaPC,check_pl,check_cf,company1,move_pl,move_cf,move_pc,
                         meses,cliente,otros)
                
            elif cliente=="Build38" and otros==True:
                win_build38(mayor_path,nominas,input_path,lista_hojas,treedataMeses,treedataPL,treedataCF,
                     input_hojaPL,input_hojaCF,input_hojaPC,check_pl,check_cf,company1,move_pl,move_cf,move_pc,
                     meses,mes,anno,cliente,otros)
            elif otros==False and move_pl==False and move_cf==False and check_pl==False and check_cf==False and meses==False and move_pc==True: 
                # InputPC
                t0=time2()
                InputPC(cliente, company1, nominas, input_path, input_hojaPC, anno, mes).add_to_reporting()
                t1=time2()
                tiempo="- Time: " +str(round(t1-t0,2)) + " sec."
                sg.Popup('Payroll inserted', tiempo)   
                
            else:
                win3(mayor_path,nominas,input_path,lista_hojas,treedataMeses,treedataPL,treedataCF,
                     input_hojaPL,input_hojaCF,input_hojaPC,check_pl,check_cf,company1,move_pl,move_cf,move_pc,
                     meses,otros,mes,anno,cliente,mayor_pl_dotgis=None,mayor_hoja_dotgis=None,ventas=None,compras=None,
                     hoja_report_ventas=None,hoja_report_compras=None,pipe=None,lost=None,won=None,hoja_pipe=None)

                
    
                    
    win1.close()


             
if __name__=="__main__":
    main()