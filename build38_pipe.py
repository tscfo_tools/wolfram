# -*- coding: utf-8 -*-
"""
Created on Thu Oct  7 09:44:32 2021

@author: romina

Build38
"""

import pandas as pd
from xlwings import App,Book
from datetime import date, timedelta,datetime

def ultimo_dia(anno,mes):
    ultimo=date(anno,mes+1,1)-timedelta(days=1)
    ultimo=ultimo.strftime("%d/%m/%Y")
    return ultimo

def dataframe_difference(df1, df2, lista=None, which=None):
    '''
    Parameters
    ----------
    df1 : dataframe
    df2 : dataframe
    which : TYPE, optional
        The default is None.

    Returns
    -------
    diff_df : dataframe.

    '''
    """Find rows which are different between two DataFrames."""
    comparison_df = df1.merge(
        df2,
        indicator=True,
        how='right',
        on=lista,
        suffixes=('_DROP', ''),
    )
    if which is None:
        diff_df = comparison_df[comparison_df['_merge'] != 'both']
    else:
        diff_df = comparison_df[comparison_df['_merge'] == which]
        #diff_df.to_csv('data/diff.csv')
    return diff_df



def build38_pipe(pipe,lost,won,reporting,hoja_pipe,mes,anno):
    df_pipe=pd.read_excel(pipe,header=1)
    df_pipe=df_pipe[0:len(df_pipe)-5]
    df_pipe=df_pipe.drop(df_pipe[df_pipe["Stage"]=="END"].index)
    columnas=list(df_pipe.columns)
    columnas=columnas[0:len(columnas)-2]
    df_pipe=df_pipe.dropna(axis=0,how="all",subset=columnas)
    
    df_report=pd.read_excel(reporting,sheet_name=hoja_pipe)
    titulos=list(df_report.columns)
    total_rows=len(df_report)+1
    df_report=df_report.drop(df_report[df_report["Stage"]=="END"].index)
    
    col_iguales=list(set(df_pipe).intersection(set(df_report.columns)))
    
    df_report["Years"]=pd.to_numeric(df_report["Years"])
    df_report["Sales Cycle Duration"]=pd.to_numeric(df_report["Sales Cycle Duration"])
    
    
    diff=dataframe_difference(df_report,df_pipe,lista=col_iguales)
    diff=df_pipe   
    
    app = App(visible=True)
    wb=Book(reporting)
    
    if len(diff)>0:
        ws=wb.sheets[hoja_pipe]
        if wb.sheets[hoja_pipe].api.AutoFilterMode == True:
            wb.sheets[hoja_pipe].api.AutoFilterMode = False
        rango=str(total_rows) +":" + str(total_rows+len(diff))
        ws.range(rango).insert()
        rango="A"+str(total_rows)
        ws.range(rango).options(index=False).value=diff
        ws.range(str(total_rows)+":"+str(total_rows)).api.Delete()        
        
        rango_antes="N"+str(total_rows-1)+":P"+str(total_rows-1)
        rango_despues="N"+str(total_rows)+":P"+str(total_rows+len(diff)-1)
        ws.range(rango_antes).copy()
        ws.range(rango_despues).paste("formulas")
    
        rango_fechas="A"+str(total_rows)+":A"+str(total_rows+len(diff)-1)
        ws.range(rango_fechas).value=ultimo_dia(int(anno),int(mes))

        rango_nuevo=chr(64+len(list(df_report.columns)))+str(total_rows)+":"+chr(64+len(list(df_report.columns)))+str(total_rows+len(diff)-1)
        ws.range(rango_nuevo).value="=numbervalue("+rango_fechas+")"
        ws.range(rango_nuevo).copy()
        ws.range(rango_fechas).paste("values")        
        ws.range(rango_nuevo).clear()
        total_rows=total_rows+len(diff)
        
    df_lost=pd.read_excel(lost,header=1)
    df_lost=df_lost[0:len(df_lost)-3]
    
    col_iguales=list(set(df_lost).intersection(set(df_report.columns)))
    diff=dataframe_difference(df_report,df_lost,lista=col_iguales)
    diff=diff.drop(['_merge'],axis=1) 
    diff=df_lost
    diff=diff.fillna("")
    

    if len(diff)>0:
        ws=wb.sheets[hoja_pipe]
        if wb.sheets[hoja_pipe].api.AutoFilterMode == True:
            wb.sheets[hoja_pipe].api.AutoFilterMode = False
        rango=str(total_rows)+":"+str(total_rows+len(diff))
        ws.range(rango).insert()
        for titulo in col_iguales:
            rango=chr(65+titulos.index(titulo))+str(total_rows)
            ws.range(rango).options(index=False).value=diff[titulo]
        ws.range(str(total_rows)+":"+str(total_rows)).api.Delete() 
        rango_antes="N"+str(total_rows-1)+":P"+str(total_rows-1)
        rango_despues="N"+str(total_rows)+":P"+str(total_rows+len(diff)-1)
        ws.range(rango_antes).copy()
        ws.range(rango_despues).paste("formulas")
        
        rango_fechas="A"+str(total_rows)+":A"+str(total_rows+len(diff)-1)
        ws.range(rango_fechas).value=ultimo_dia(int(anno),int(mes))

        rango_nuevo=chr(64+len(list(df_report.columns)))+str(total_rows)+":"+chr(64+len(list(df_report.columns)))+str(total_rows+len(diff)-1)
        ws.range(rango_nuevo).value="=numbervalue("+rango_fechas+")"
        ws.range(rango_nuevo).copy()
        ws.range(rango_fechas).paste("values")        
        ws.range(rango_nuevo).clear()
        total_rows=total_rows+len(diff)
        
    
    df_won=pd.read_excel(won,header=1)
    df_won=df_won[0:len(df_won)-3]
    
    col_iguales=list(set(df_won).intersection(set(df_report.columns)))
    diff=dataframe_difference(df_report,df_won,lista=col_iguales)
    diff=diff.drop(['_merge'],axis=1)    
    diff=df_won
    diff=diff.fillna("")    
    
    
    if len(diff)>0:
        ws=wb.sheets[hoja_pipe]
        if wb.sheets[hoja_pipe].api.AutoFilterMode == True:
            wb.sheets[hoja_pipe].api.AutoFilterMode = False
        rango=str(total_rows)+":"+str(total_rows+len(diff))
        ws.range(rango).insert()
        for titulo in col_iguales:
            rango=chr(65+titulos.index(titulo))+str(total_rows)
            ws.range(rango).options(index=False).value=diff[titulo]
        ws.range(str(total_rows)+":"+str(total_rows)).api.Delete() 
        rango_antes="N"+str(total_rows-1)+":P"+str(total_rows-1)
        rango_despues="N"+str(total_rows)+":P"+str(total_rows+len(diff)-1)
        ws.range(rango_antes).copy()
        ws.range(rango_despues).paste("formulas")
        
        rango_fechas="A"+str(total_rows)+":A"+str(total_rows+len(diff)-1)
        ws.range(rango_fechas).value=ultimo_dia(int(anno),int(mes))

        rango_nuevo=chr(64+len(list(df_report.columns)))+str(total_rows)+":"+chr(64+len(list(df_report.columns)))+str(total_rows+len(diff)-1)
        ws.range(rango_nuevo).value="=numbervalue("+rango_fechas+")"
        ws.range(rango_nuevo).copy()
        ws.range(rango_fechas).paste("values")        
        ws.range(rango_nuevo).clear()
    
    wb.save()
    wb.close()
     
    app.kill()
