# -*- coding: utf-8 -*-
"""
Created on Thu Sep  9 10:58:35 2021

@author: romina

dotGIS
"""


import win32com.client as win32
from time import sleep
from Wolfram.functions.Excel_functions import colnum_string


def insertar_pl(report,mayor,hoja_mayor,hoja_report,nombre_nuevo):
    
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False
    excel.DisplayAlerts = False
    
    try:
        wb_report = excel.Workbooks.Open(report,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {report}')
     
    
    try:
        wb_mayor = excel.Workbooks.Open(mayor,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {mayor}')
          
        
    ws_report=wb_report.Sheets(hoja_report)
    ws_mayor=wb_mayor.Sheets(hoja_mayor)
    
    ws_report.Activate()
    ws_report.Range("A:ZZ").ClearOutline()
    ws_report.Range("1:500").ClearOutline()
        
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    if ws_report.FilterMode: ws_report.ShowAllData()
    
    ws_mayor.Activate()
    ws_mayor.Range("A:A").Find("Cuenta").Select()
    fila1=excel.Selection.Row+1
    ws_mayor.Range("A:A").Find("Total").Select()
    fila2=excel.Selection.Row-1
    
    #Buscar última columna en Mayor
    ws_mayor.Activate()
    ws_mayor.Range(str(fila1-1)+":"+str(fila1-1)).Find("Total").Select()
    col=excel.Selection.Column-1
    
    PL_mayor_data = ws_mayor.Range(ws_mayor.Cells(fila1,1),ws_mayor.Cells(fila2,col)).Value
    mayor_counts = [PL_mayor_data[i][0] for i in range(len(PL_mayor_data))]
    
    
    ws_report.Activate()
    excel.Application.Calculation = -4135 # to set xlCalculationManual
    sleep(0.5)
    ws_report.Range("C:C").Find("END").Select()
    fila1_report=2
    fila2_report=excel.Selection.Row-1
    
    PL_report_data = ws_report.Range("C" + str(fila1_report) + ":P" + str(fila2_report)).Value
    report_counts = [PL_report_data[i][0] for i in range(len(PL_report_data))]
    
    # Find new counts
    total_counts = list(set(mayor_counts) | set(report_counts))
    total_counts.sort() # sort counts
    
    n_total_counts = len(total_counts)
    n_new_counts = n_total_counts - len(report_counts)
    
    # Insert new files for new counts and drag formulas
    ws_report.Range(str(fila2_report+1)+":"+str(fila2_report+n_new_counts)).EntireRow.Insert()
    ws_report.Range("A" + str(fila2_report) +":B"+ str(fila2_report)).Copy(ws_report.Range("A" + str(fila2_report+1) + ":B" + str(fila2_report+n_new_counts)))
    ws_report.Range("R" + str(fila2_report) +":BA"+ str(fila2_report)).Copy(ws_report.Range("R" + str(fila2_report+1) + ":BA" + str(fila2_report+n_new_counts)))
    
    #Borrar todo de InputPL
    ws_report.Range("C2:P"+str(fila2_report)).ClearContents()
    
    row_to_copy_in = fila1_report
    for count in total_counts:
        # Insert PL from Mayor
        if count in mayor_counts:
            count_index = mayor_counts.index(count)
            ws_report.Range("C" + str(row_to_copy_in) + ":" + colnum_string(2 + len(PL_mayor_data[1])) + str(row_to_copy_in)).Value = PL_mayor_data[count_index]
    
        else:
            count_index = report_counts.index(count)
            ws_report.Range("C" + str(row_to_copy_in) + ":P" + str(row_to_copy_in)).Value = PL_report_data[count_index]
        
        row_to_copy_in += 1
    
    ws_report.Activate()
    excel.Application.Calculation = -4105 # to set xlCalculationAutomatic
    
    wb_report.SaveAs(nombre_nuevo.replace("/","\\"))
    wb_report.Close(True)
    wb_mayor.Close(False)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()    