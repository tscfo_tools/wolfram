
from time import sleep
from datetime import datetime
import win32com.client as win32



def colnum_string(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string

def find_current_month_header(ws_inp, last_row, last_col, actual_date_str):
    
    [header_row, month_col] = [None, None]
    flag = False
    for i in range(1, last_col + 1):
        for j in range(1, last_row + 1):
            if type(ws_inp.range(colnum_string(i) + str(j)).value) == datetime:
                if ws_inp.range(colnum_string(i) + str(j)).value.strftime("%b/%y") == actual_date_str:
                    [header_row, month_col] = [j, i]
                    break
            if flag == True:
                break
            
    return [header_row, month_col]


def update_forecast_func(wb_inp, sheets_to_edit, month_cols, ini_row, end_row):

    for i in range(len(sheets_to_edit)):
        # Select ws
        try:
            ws_inp = wb_inp.Sheets(sheets_to_edit[i])
        except:
            sleep(3.0)
            ws_inp = wb_inp.Sheets(sheets_to_edit[i])
        ini_range, end_range = None, None
        for current_row in range(ini_row[i], end_row[i] + 1):
            
            # Get font color
            dec_color = ws_inp.Cells(current_row, month_cols[i]).Font.Color
            # Tranform color code: decimal to rgb
            rgb_color = [int(dec_color / (256*256))/255, int((dec_color / 256) % 256)/255, int(dec_color % 256)/255]
            
            # if font color is black or grey, copy previous month formula and format 
            if (rgb_color[2] <= 244/255) and abs(rgb_color[1] - rgb_color[2]) < 10/255 and (rgb_color[0] - rgb_color[2]) < 10/255 and (rgb_color[0] - rgb_color[1]) < 10/255:
                if ini_range == None: ini_range = current_row
            else:
                if ini_range != None: end_range = current_row - 1
            
            if current_row == end_row[i]: end_range = end_row[i]
            
            # Copy format and formula
            if ini_range != None and end_range != None:
                if end_range >= ini_range:
                    ws_inp.Range(colnum_string(month_cols[i] - 1) + str(ini_range) + ":" + colnum_string(month_cols[i] - 1) + str(end_range)).Copy()
                    ws_inp.Range(colnum_string(month_cols[i]) + str(ini_range) + ":" + colnum_string(month_cols[i]) + str(end_range)).PasteSpecial(Paste = win32.constants.xlPasteFormats)
                    ws_inp.Range(colnum_string(month_cols[i]) + str(ini_range) + ":" + colnum_string(month_cols[i]) + str(end_range)).PasteSpecial(Paste = win32.constants.xlPasteFormulas)
                    
                    # Re-initialize ini_range and end_range
                    ini_range, end_range = None, None
   
    return



def update_forecast(wb_path, month, year, input_op = []):
       
    # 0. Open wb
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    excel.Visible = False
    excel.DisplayAlerts = False
    wb_inp = excel.Workbooks.Open(wb_path, False, None)
    
    # 1. Set ws to update forecast
    ## 1.1. Get all worksheet names in the spreadsheet
    try:
        ws_list = [sheet.Name for sheet in wb_inp.Sheets]
    except:
        sleep(4.0)
        ws_list = [sheet.Name for sheet in wb_inp.Sheets]
     
    
    ## 1.2. Get ws to update forecast
    sheets_name = []
    if 'InputPC' in input_op:
        ## PC and P&L("Personnel costs" section)
        sheets_name = sheets_name + [sh for sh in ws_list if (sh == "PC")]
        sheets_name = sheets_name + [sh for sh in ws_list if ("P&L" in sh and sh!= "P&L_DS" and sh!= "P&L_YTD")]
    if 'InputPL' in input_op:
        ## Metrics, P&L, OC
        sheets_name = sheets_name + [sh for sh in ws_list if ("Metrics" in sh and sh!= "Metrics_DS" and sh!= "Metrics_YTD")]
        sheets_name = sheets_name + [sh for sh in ws_list if ("P&L" in sh and sh!= "P&L_DS" and sh!= "P&L_YTD")]
        sheets_name = sheets_name + [sh for sh in ws_list if (sh == "OC")]
    if 'InputCF' in input_op:
        ## Cash, CX, F
        sheets_name = sheets_name + [sh for sh in ws_list if ("Cash" in sh and sh!= "Cash_DS" and sh!= "Cash_YTD")]
        sheets_name = sheets_name + [sh for sh in ws_list if (sh == "CX")]
        sheets_name = sheets_name + [sh for sh in ws_list if (sh == "F")]
    
    
    # 2. Set columns and rows to update forecast
    actual_date_str = datetime(year, month, 1).strftime("%b/%y")
    sheets_to_edit, month_cols, ini_row, end_row = [], [], [], []
    for i in range(len(sheets_name)):
        try:
            try:
                ws_inp = wb_inp.Sheets(sheets_name[i])
            except:
                sleep(3.0)
                ws_inp = wb_inp.Sheets(sheets_name[i])

            # If worksheet is hidden -> Go to next
            if ws_inp.Visible == win32.constants.xlSheetVisible:
                # 2.1. Ungroup rows before copy formulas and formats (error copying if exist)
                ws_inp.Outline.ShowLevels(RowLevels = 2)
                
                current_month_header_cell = ws_inp.UsedRange.Find(actual_date_str, SearchDirection = win32.constants.xlNext, SearchOrder = win32.constants.xlByRows, LookIn=win32.constants.xlValues)
                [header_row, month_col] = [current_month_header_cell.Row, current_month_header_cell.Column]
                last_row = ws_inp.Range(colnum_string(month_col) + ":" + colnum_string(month_col)).Find("*", SearchDirection = win32.constants.xlPrevious, SearchOrder = win32.constants.xlByColumns).Row
                
                if "P&L" in sheets_name[i]:
                    PC_cell = ws_inp.UsedRange.Find('Personnel cost', SearchDirection = win32.constants.xlNext, SearchOrder = win32.constants.xlByColumns, LookIn=win32.constants.xlValues, LookAt=win32.constants.xlPart)
                    # If not PC section in P&L, next
                    if PC_cell != None:
                        [PC_row, PC_col] = [PC_cell.Row, PC_cell.Column]
                        PC_ini_row = PC_row - 1 # row "% over Fixed costs" before "Personnel costs" row
                        PC_end_row = ws_inp.Cells(PC_ini_row + 1, PC_col).End(win32.constants.xlDown).Row if ws_inp.Cells(PC_ini_row + 2, PC_col).Value != None else PC_ini_row + 1
                        
                        if 'InputPC' in input_op:
                            sheets_to_edit.append(sheets_name[i])
                            month_cols.append(month_col)
                            ini_row.append(PC_ini_row)
                            end_row.append(PC_end_row)
                            
                        if 'InputPL' in input_op:
                            # Avoid Personnel costs block
                            # 1st block: before PC
                            sheets_to_edit.append(sheets_name[i])
                            month_cols.append(month_col)
                            ini_row.append(header_row + 1)
                            end_row.append(PC_ini_row - 1)
                            
                            # 2nd block: after PC
                            sheets_to_edit.append(sheets_name[i])
                            month_cols.append(month_col)
                            ini_row.append(PC_end_row + 1)
                            end_row.append(last_row)
                    
                    elif 'InputPL' in input_op:
                        # Update whole P&L sheet
                        sheets_to_edit.append(sheets_name[i])
                        month_cols.append(month_col)
                        ini_row.append(header_row + 1)
                        end_row.append(last_row)
                else:
                    sheets_to_edit.append(sheets_name[i])
                    month_cols.append(month_col)
                    ini_row.append(header_row + 1)
                    end_row.append(last_row)
            
           
        except:
            pass
    
    
    # 4. Apply forecast update
    update_forecast_func(wb_inp, sheets_to_edit, month_cols, ini_row, end_row)
    
    # 5. Save and close wb
    wb_inp.SaveAs(wb_path.replace("/","\\"))
    wb_inp.Close(True)
    
    excel.Visible = False 
    excel.DisplayAlerts = False
    excel.Application.Quit() 
    
    return
