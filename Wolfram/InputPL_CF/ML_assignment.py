

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier # Random forest

def unique(list1):
      
    # insert the list to the set
    list_set = set(list1)
    # convert the set to the list
    unique_list = (list(list_set))
    x = []
    for x_ in unique_list:
        x.append(x_)
    return x


def preprocess_data_for_prediction(data, cuenta_col_label, row_index_col_label, NA_accounts_ini):
    
    # 1. Columns nan as ""
    data[cuenta_col_label] = data[cuenta_col_label].fillna('')
    # 2. Account column as string
    try:
        data[cuenta_col_label] = ['00' if data[cuenta_col_label][i] == '' else str(int(data[cuenta_col_label][i])) for i in range(len(data))]
    except:
        data[cuenta_col_label] = ['00' if data[cuenta_col_label][i] == '' else str(data[cuenta_col_label][i]) for i in range(len(data))]

    # 3. Find NA accounts
    NA_row_index = [data[row_index_col_label][i] for i in range(len(data)) if data[cuenta_col_label][i][:2] in NA_accounts_ini]
    NA_filt = [True if data[row_index_col_label][i] not in NA_row_index else False for i in range(len(data))]
    # 3.1. Filt NA accounts from data
    data = data[NA_filt].reset_index(drop = True)

    # 4. Split data depending Accoutns first number -> Income or Cost
    account_first_num = [data[cuenta_col_label][i][0] for i in range(len(data))]
    account_ini_unique = unique(account_first_num)
    # 4.1. Splitting data into subsets
    data_subsets = []
    for i in range(len(account_ini_unique)):
        account_filt = [True if data[cuenta_col_label][j][0] == account_ini_unique[i] else False for j in range(len(data))]
        data_subsets.append(data[account_filt].reset_index(drop = True))

    return data_subsets, account_ini_unique, NA_row_index


def predict_random_forest(x, y, x_new):

    # 1. Split data into train and test: 80/20
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state = 1000)

    # 2. Preprocessing input data -> vectorization
    stop_words = ["and", "or", "to", "the", "of", "from", "for", "y", "e", "o", "a", "el", "la", "de"]
    vectorizer = CountVectorizer(stop_words = stop_words)
    vectorizer.fit(x_train)
    # 2.1. Words collection into a sparse matrix
    X_train = vectorizer.transform(x_train)
    X_test  = vectorizer.transform(x_test)
    X_new = vectorizer.transform(x_new)

    # 3. Building the model -> Random forest
    model=RandomForestClassifier(random_state = 12345)
    model.fit(X_train, y_train)
    ## 3.1. Model accuracy
    model_acc = model.score(X_test, y_test)

    # 4. Prediction
    y_new = model.predict(X_new)
    y_new_prob_matrix = model.predict_proba(X_new)
    y_new_prob = [y_new_prob_matrix[i].max() for i in range(len(y_new_prob_matrix))]
    y_new = [str(y_new[j]) for j in range(len(y_new))]
   
    return y_new, y_new_prob, model_acc


def ML_assignment(reporting_df, new_from_ledger, cuenta_col_label, row_index_col_label, labels_to_use, target_labels, NA_accounts_ini):    


    # 0. Check target labels
    input_labels = []
    for label in list(reporting_df.columns):
        if label in labels_to_use: input_labels.append(label)
    ## If not any of them, "Cuenta" -> other prediction method
    if len(input_labels) == 0: input_labels = ["Cuenta"]
    ## Check if they are in Ledger
    for label in input_labels:
        if label not in list(new_from_ledger.columns): input_labels.remove(label)

    
    # 1. Preprocess data
    # 1.1. Test data
    pre_reporting_df = reporting_df.copy().reset_index(drop = True)
    pre_reporting_df[target_labels] = pre_reporting_df[target_labels].fillna("NA")
    test_subsets, test_account_ini_unique, test_NA_row_index = preprocess_data_for_prediction(pre_reporting_df, cuenta_col_label, row_index_col_label, NA_accounts_ini)
    # 1.2. New data
    new_data = new_from_ledger.copy().reset_index(drop = True)
    new_data["indexes"] = new_data.index
    new_subsets, new_account_ini_unique, new_NA_row_index = preprocess_data_for_prediction(new_data, cuenta_col_label, row_index_col_label, NA_accounts_ini)
        
    # Loop for every target varaible
    model_accuracy_per_target = []
    for target_label in target_labels:
        # 2. Prediction for each first number of the accounts
        new_target, new_target_prob, new_row_index, model_accuracy_performance = [], [], [], []
        # Loop
        for account_ini in new_account_ini_unique:
            # 2.1. Select subset for predicting
            new_subset = new_subsets[new_account_ini_unique.index(account_ini)]
            # Check there are movements in new_subset.
            if len(new_subset) > 0:
                # Check if initial account number of new data subset, exists in test data
                if account_ini in test_account_ini_unique:
                    # 2.2. Select test subset for training
                    test_subset = test_subsets[test_account_ini_unique.index(account_ini)]
                    
                    # 2.3. Build "Inputs" and for training model: Account info = Account concept + Account name
                    ## 2.3.1. Test inputs
                    test_account_mix = ["" for k in range(len(test_subset))]
                    for account_label in input_labels:
                        test_account_mix = [(test_account_mix[l] + test_subset[account_label][l] + " ") for l in range(len(test_subset))]
                    ## 2.3.2. New input
                    new_account_mix = ["" for k in range(len(new_subset))]
                    for account_label in input_labels:
                        new_account_mix = [(new_account_mix[l] + new_subset[account_label][l] + " ") for l in range(len(new_subset))]
                    
                    # 2.4. Select Test target
                    test_target = test_subset[target_label].to_list()
                    unique_target = unique(test_target)
                    
                    # 2.5. Make prediction
                    ## Check there is more than a posible target
                    if len(unique_target) > 1:
                        ## 2.5.1. Random forest
                        new_target_account, new_target_account_prob, model_accuracy_account = predict_random_forest(test_account_mix, test_target, new_account_mix)
                    else:
                        ## 2.5.2. If only one target possibility -> all movements that possibility and 100% accuracy
                        new_target_account, new_target_account_prob, model_accuracy_account = [unique_target[0] for ii in range(len(new_account_mix))], [1.00 for ii in range(len(new_account_mix))], 1.0
                    
                    # 2.6. Save model accuracy
                    model_accuracy_performance.append([len(new_target_account), model_accuracy_account])
                        
                else:
                    new_target_account = ["" for ii in range(len(new_subset))]
                    new_target_account_prob = [0 for ii in range(len(new_subset))]
                
                # 2.7. Save new target assignments
                new_target = new_target + new_target_account
                new_target_prob = new_target_prob + new_target_account_prob
                new_row_index = new_row_index + new_subset[row_index_col_label].to_list()
                    
        # 3. Add NA account rows if exist
        if len(new_NA_row_index):
            new_target = new_target + ["NA" for ii in range(len(new_NA_row_index))]
            new_target_prob = new_target_prob + [1.00 for ii in range(len(new_NA_row_index))]
            new_row_index = new_row_index + new_NA_row_index
            
            ## 3.1. Add NA prediction accuracy -> 100%
            model_accuracy_performance.append([len(new_NA_row_index), 1.0]) # "NA" is always OK
        
        # 4. Sort target predictions after putting all together
        sort_new_target = ["" for i in range(len(new_data))]
        sort_new_target_prob = [None for i in range(len(new_data))]
        for ind in new_row_index:
            sort_new_target[ind] = new_target[new_row_index.index(ind)]
            sort_new_target_prob[ind] = new_target_prob[new_row_index.index(ind)]
        
        # 5. Add target columns to new_from_ledger dataframe
        new_from_ledger[target_label] = sort_new_target
        new_from_ledger["Acc - " + target_label] = [f'{i*100:.0f}%' for i in sort_new_target_prob]
        new_from_ledger["Diff - " + target_label] = [round(abs((1 - sort_new_target_prob[i]) * new_from_ledger["Net"].iloc[i]), 2) for i in range(len(new_target))]
        ## 5.1. Add also this columns to reporting_df (emprty)
        if "Acc - " + target_label in reporting_df.columns: reporting_df = reporting_df.drop(["Acc - " + target_label], axis=1)
        if "Diff - " + target_label in reporting_df.columns: reporting_df = reporting_df.drop(["Diff - " + target_label], axis=1)
        reporting_df["Acc - " + target_label] = ""
        reporting_df["Diff - " + target_label] = ""

        # 6. Manage Prediction accuracy
        acc_num, acc_den = 0, 0
        for ii in range(len(model_accuracy_performance)):
            acc_num += model_accuracy_performance[ii][0] * model_accuracy_performance[ii][1]
            acc_den += model_accuracy_performance[ii][0]
        ## 6.1. Target accuracy
        model_accuracy = None if acc_den == 0 else acc_num/acc_den
        ## Add to output
        model_accuracy_per_target.append(round(model_accuracy, 3))
        
    return reporting_df, new_from_ledger, model_accuracy_per_target

