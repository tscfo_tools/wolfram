
from Wolfram.InputPL_CF.update_forecast import update_forecast

class InputPC:
    def __init__(self, client, company, PC_path, reporting_path, reporting_sheet, year, month):
        self.client = client
        self.company = company
        self.PC_path = PC_path
        self.reporting_path = reporting_path
        self.reporting_sheet = reporting_sheet
        self.year = year
        self.month = month

    def add_to_reporting(self):

        # PC from Berenguela
        if  self.client=="Wheels" or self.client=="Geoblink" or self.client=="Luca" \
                or self.client=="Liux" or self.client=="Katoo" or self.client=="The Startup CFO" or self.client=="Baia" \
                or self.client=="Gelt" or (self.client=="Gelt" and (self.company=="ESP" or self.company=="Tech" or self.company=="ProsegurCrypto")) \
                or self.client=="Gasmobi" or (self.client == "Payflow" and self.company=="DIG") or self.client=="uSizy" or self.client=="MediQuo" \
                or self.client == "Nebeus" or self.client == "Triditive" or self.client == "Other" or self.client == "Amphora":
            from .Clients.pc_from_berenguela import pc_from_berenguela
            pc_from_berenguela(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])
        
        if self.client=="Agrosingularity":
            from .Clients.agrosingularity_pc import agrosingularity_pc
            agrosingularity_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])

        if self.client=="Build38" and self.company=="ESP":
            from .Clients.build38_esp_pc import build_esp_pc
            build_esp_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])

        if self.client=="Build38" and self.company=="GER":
            from .Clients.build38_ger_pc import build38_ger_pc
            build38_ger_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])

        if self.client=="Camelina":
            from .Clients.camelina_pc import camelina_pc
            camelina_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])

        if self.client=="Circular":
            from .Clients.circular_pc import circular_pc
            circular_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])

        if self.client=="Cobee":
            from .Clients.cobee_pc import cobee_pc
            cobee_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])

        if self.client=="Graphext":
            from .Clients.graphext_pc import graphext_pc
            graphext_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])

        if self.client=="Nora":
            from .Clients.nora_pc import nora_pc
            nora_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])

        if self.client=="Nova" and self.company=="ESP":
            from .Clients.nova_esp_pc import nova_esp_pc
            nova_esp_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])

        if self.client=="Nova" and self.company=="SWE":
            from .Clients.nova_swe_pc import nova_swe_pc
            nova_swe_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])

        if self.client=="Validated ID":
            from .Clients.validatedid_pc import validated_pc
            validated_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])
                        
        if self.client=="Gelt" and self.company=="Brasil":
            from .Clients.gelt_brazil_pc import gelt_brazil_pc
            gelt_brazil_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])
        
        if self.client=="Innitius":
            from .Clients.innitius_pc import innitius_pc
            innitius_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])
        
        if self.client=="Iomob" and self.company=="ESP":
            from .Clients.iomob_esp_pc import iomob_esp_pc
            iomob_esp_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])
        
        if self.client=="Tenzir":
            from .Clients.tenzir_pc import tenzir_pc
            tenzir_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])
        
        if self.client=="eKuore":
            from .Clients.ekuore_pc import ekuore_pc
            ekuore_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])
        
        if self.client=="ClimateTrade":
            from .Clients.climatetrade_pc import climatetrade_pc
            climatetrade_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])
        
        if self.client=="iFeel":
            from .Clients.ifeel_pc import ifeel_pc
            ifeel_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])
        
        if self.client=="Hubuc":
            from .Clients.hubuc_pc import hubuc_pc
            hubuc_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])
        
        if self.client=="AM Polymers":
            from .Clients.am_polymers_pc import am_polymers_pc
            am_polymers_pc(self.PC_path, self.reporting_path, self.reporting_sheet, self.year, self.month)
            # update forecast
            update_forecast(self.reporting_path, int(self.month), int(self.year), ['InputPC'])

        return


        