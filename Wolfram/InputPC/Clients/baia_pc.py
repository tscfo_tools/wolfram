# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 11:33:56 2021

@author: romina

Baïa
"""


import pandas as pd
from Wolfram.functions.dates_functions import ultimo_dia
import win32com.client as win32
from win32com.client import constants

def baia_pc(nomina,reporting,hoja_reporting,anno,mes):
    #importar tabla de nominas como pandas dataframe para saber cuantas filas de empleados hay

    nomina_df=pd.read_excel(nomina,header=None)
    filas_nombre=list(nomina_df[nomina_df[0]=="NOMBRE TRABAJADOR"].axes[0])
    last_col=len(nomina_df.columns)
    nombres=nomina_df.iloc[filas_nombre[0]][1:last_col]
    filas_nombre.pop(0)
    for linea in filas_nombre:
        nombres=nombres.append(nomina_df.iloc[linea][1:last_col])
        
    nombres.dropna(inplace=True)
    nombres=nombres[0:len(nombres)-1]
    
    filas_coste=list(nomina_df[nomina_df[0]=="COSTE TOTAL EMPRESA"].axes[0])
    costes=nomina_df.iloc[filas_coste[0]][1:last_col]
    filas_coste.pop(0)
    for linea in filas_coste:
        costes=costes.append(nomina_df.iloc[linea][1:last_col])
    costes=costes[costes!=0]
    costes=costes[0:len(costes)-1]
    
    
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False
    excel.DisplayAlerts = False
    
    try:
        wb_report = excel.Workbooks.Open(reporting,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {reporting}')
       
    ws_report=wb_report.Sheets(hoja_reporting)
    ws_report.Range("A:ZZ").ClearOutline()
    ws_report.Range("1:500").ClearOutline()
    
    if ws_report.FilterMode: ws_report.AutoFilterMode = False
    
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    #Insertar filas en report
    ws_report.Activate()
    ends=ws_report.Range("A:A").Find("END").Row
    
    ws_report.Range(str(ends)+":"+str(ends+len(nombres)-1)).Insert()
    
    j=0
    for i in range(ends,ends+len(nombres)):
        ws_report.Range("B"+str(i)).Value=nombres.tolist()[j]
        ws_report.Range("C"+str(i)).Value=costes.tolist()[j]
        j=j+1
        
    
    ws_report.Range("A"+str(ends)+":A"+str(ends+len(nombres)-1)).Value=ultimo_dia(int(anno),int(mes))
    ws_report.Range("A"+str(ends)+":A"+str(ends+len(nombres)-1)).TextToColumns(Destination=ws_report.Range("A"+str(ends)), DataType=constants.xlDelimited,TextQualifier=constants.xlDoubleQuote,ConsecutiveDelimiter=False,
                                                                                Tab=False,Semicolon=False, Comma=False, Space=False, Other=False,    TrailingMinusNumbers=True)
    
    ws_report.Range("D"+str(ends-1)+":H"+str(ends-1)).Copy(ws_report.Range("D"+str(ends)+":H"+str(ends+len(nombres)-1)))
    
    wb_report.Save()
    wb_report.Close(True)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()              