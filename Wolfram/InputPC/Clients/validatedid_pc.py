# -*- coding: utf-8 -*-
"""
Created on Fri Oct  1 13:13:17 2021

@author: romina

ValidatedID
"""

import win32com.client as win32
from win32com.client import constants

def validated_pc(nomina,reporting,hoja_reporting,mes,anno):
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False
    excel.DisplayAlerts = False
    
    try:
        wb_report = excel.Workbooks.Open(reporting,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {reporting}')
       
    
    try:
        wb_nomina = excel.Workbooks.Open(nomina,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {nomina}')
    
    ws_report=wb_report.Sheets(hoja_reporting)
    wb_nomina.Activate()
    hoja_nomina=wb_nomina.ActiveSheet.Name
    ws_nomina=wb_nomina.Sheets(hoja_nomina)
    
    ws_report.Range("A:ZZ").ClearOutline()
    ws_report.Range("1:500").ClearOutline()
    
    if ws_report.FilterMode: ws_report.AutoFilterMode = False
    
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    ws_nomina.Range("A:ZZ").ClearOutline()
    ws_nomina.Range("1:500").ClearOutline()
    
    try:
        ws_nomina.Columns.EntireColumn.Hidden=False
        ws_nomina.EntireRow.Hidden=False
    except:
        pass
    
    
    used = ws_nomina.UsedRange
    nrows = used.Row + used.Rows.Count - 1
    ws_nomina.Range("A2:AC"+str(nrows)).AutoFilter(Field=1,Criteria1="<>")
    
    
    ws_report.Activate()
    ends=ws_report.Range("A:A").Find("END").Row

    ws_report.Rows(str(ends)+":"+str(ends+nrows)).Insert(-4121)
    ws_nomina.Range("A3:AC"+str(nrows)).Copy()
    ws_report.Range("A"+str(ends)).PasteSpecial(Paste = constants.xlPasteValues)
    
    penultima=ws_report.Range("A1").End(-4121).Row
    ultima=ws_report.Range("A"+str(penultima)).End(-4121).Row

    ws_report.Range(str(penultima+1)+":"+str(ultima-1)).Delete()


    #Copiar fórmulas
    ends2=ws_report.Range("A:A").Find("END").Row
    ws_report.Range("AD"+str(ends-1)+":AF"+str(ends-1)).Copy(ws_report.Range("AD"+ str(ends)+":AF"+str(ends2-1)))
    
    wb_report.Save()
    wb_report.Close(True)
    wb_nomina.Close(False)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()     
    
def validated_other(reporting,accounts,oneshot,pipeline,hoja_accounts,hoja_one,
                    hoja_pipe):
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = True
    excel.DisplayAlerts = False
    
    try:
        wb_report = excel.Workbooks.Open(reporting,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {reporting}')
       
    
    try:
        wb_acc = excel.Workbooks.Open(accounts,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {accounts}')
    
    ws_report=wb_report.Sheets(hoja_accounts)
    wb_acc.Activate()
    hoja_acc=wb_acc.ActiveSheet.Name
    ws_acc=wb_acc.Sheets(hoja_acc)
    
    try:
        ws_report.Range("A:ZZ").ClearOutline()
        ws_report.Range("1:500").ClearOutline()
    except:
        pass
    
    if ws_report.FilterMode: ws_report.AutoFilterMode = False
    
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    try:
        ws_acc.Range("A:ZZ").ClearOutline()
    except:
        pass
    
    try:
        ws_acc.Range("1:500").ClearOutline()
    except:
        pass
    
    if ws_acc.FilterMode: ws_report.AutoFilterMode = False
    
    try:
        ws_acc.Columns.EntireColumn.Hidden=False
        ws_acc.EntireRow.Hidden=False
    except:
        pass
    
    
    #Buscar fila de ENDs
    ends=ws_report.Range("A:A").Find("END").Row

    #Contar cuántas filas nuevas hay
    used = ws_acc.UsedRange
    nrows = used.Row + used.Rows.Count - 1
    
    if ends-1<nrows:
        dif=nrows-(ends-1)
    
        #Insertar filas nuevas
        ws_report.Rows(str(ends)+":"+str(ends+dif-1)).Insert(-4121)
        
        #Copiar fórmulas
        ws_report.Range(str("U"+str(ends-1)+":ZZ"+str(ends-1))).Copy()
        ws_report.Range(str("U"+str(ends)+":ZZ"+str(ends+dif-1))).PasteSpecial(Paste=constants.xlPasteFormulas)
        
    ws_acc.Activate()
    ws_acc.Range("A2:T"+str(nrows)).Copy()
    ws_report.Range("A2").PasteSpecial(Paste = constants.xlPasteValues)

    #Averiguar cuántos clientes únicos hay
    ws_report.Activate()
    ends2=ws_report.Range("A:A").Find("END").Row
    clients=ws_report.Range("E2:E"+str(ends2)).Value
    clients=set(clients)
    clients=list(clients)
    total_clients=len(clients)
    
    #Contar cuantos clientes hay en el la hoja "Accounts" del reporting
    ws_accounts=wb_report.Sheets("Accounts")
    ws_accounts.Activate()
    total_report=ws_accounts.Range("A:A").Find("END").Row
    
    if total_clients>total_report-2:
        dif=total_clients-total_report-2
        ws_accounts.Rows(str(total_report)+":"+str(total_report+dif-1)).Insert(-4121)
        #Copiar fórmulas desde la B hasta la columna ZZ
        ws_accounts.Range("B"+str(total_report-1)+":ZZ"+str(total_report-1)).Copy()
        ws_accounts.Range("B"+str(total_report)+":ZZ"+str(total_report+dif-1)).PasteSpecial(Paste=constants.xlPasteFormulas)        
        total_report=ws_accounts.Range("A:A").Find("END").Row
        
        
    formula=ws_accounts.Range("A2").Formula
    ws_accounts.Range("A2:A"+str(total_report-1)).FormulaArray=formula
    
    #Arreglar hoja Partners
    ws_partners=wb_report.Sheets("Partners")
    ws_partners.Activate()
    
    
    try:
        ws_partners.Range("1:500").ClearOutline()
    except:
        pass
    
    if ws_partners.FilterMode: ws_partners.AutoFilterMode = False
    
    try:
        ws_partners.Columns.EntireColumn.Hidden=False
        ws_partners.EntireRow.Hidden=False
    except:
        pass
    
    total_report=ws_partners.Range("A:A").Find("END").Row
    clients=ws_report.Range("X2:X"+str(ends2)).Value
    clients=set(clients)
    clients=list(clients)
    total_clients=len(clients)
    
    if total_clients>total_report-2:
        dif=total_clients-total_report-2
        ws_partners.Rows(str(total_report)+":"+str(total_report+dif-1)).Insert(-4121)
        #Copiar fórmulas desde la B hasta la columna ZZ
        ws_partners.Range("B"+str(total_report-1)+":ZZ"+str(total_report-1)).Copy()
        ws_partners.Range("B"+str(total_report)+":ZZ"+str(total_report+dif-1)).PasteSpecial(Paste=constants.xlPasteFormulas)        
        total_report=ws_partners.Range("A:A").Find("END").Row
    
    formula=ws_partners.Range("A2").Formula
    ws_partners.Range("A2:A"+str(total_report-1)).FormulaArray=formula
    
    wb_acc.Close(False)
    wb_report.Save()
    
    # Hoja OS
    ws_report=wb_report.Sheets(hoja_one)
    try:
        ws_report.Range("A:ZZ").ClearOutline()
        ws_report.Range("1:500").ClearOutline()
    except:
        pass
    
    if ws_report.FilterMode: ws_report.AutoFilterMode = False
    
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    #Buscar fila de ENDs
    ends=ws_report.Range("A:A").Find("END").Row
    
    try:
        wb_os = excel.Workbooks.Open(oneshot,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {oneshot}')
    
    wb_os.Activate()
    hoja_acc=wb_os.ActiveSheet.Name
    ws_os=wb_os.Sheets(hoja_acc)
    
    try:
        ws_os.Range("A:ZZ").ClearOutline()
    except:
        pass
    
    try:
        ws_os.Range("1:500").ClearOutline()
    except:
        pass
    
    if ws_os.FilterMode: ws_os.AutoFilterMode = False
    
    try:
        ws_os.Columns.EntireColumn.Hidden=False
        ws_os.EntireRow.Hidden=False
    except:
        pass
    
    #Contar cuántas filas nuevas hay
    used = ws_os.UsedRange
    nrows = used.Row + used.Rows.Count - 1
    
    if ends-1<nrows:
        dif=nrows-(ends-1)
    
        #Insertar filas nuevas
        ws_report.Rows(str(ends)+":"+str(ends+dif-1)).Insert(-4121)
        
        #Copiar fórmulas
        ws_report.Range(str("U"+str(ends-1)+":ZZ"+str(ends-1))).Copy()
        ws_report.Range(str("U"+str(ends)+":ZZ"+str(ends+dif-1))).PasteSpecial(Paste=constants.xlPasteFormulas)
        
    ws_os.Activate()
    ws_os.Range("A2:T"+str(nrows)).Copy()
    ws_report.Range("A2").PasteSpecial(Paste = constants.xlPasteValues)
    
    wb_os.Close(False)
    
    ##Hoja SF
    
    ws_report=wb_report.Sheets(hoja_pipe)
    try:
        ws_report.Range("A:ZZ").ClearOutline()
        ws_report.Range("1:500").ClearOutline()
    except:
        pass
    
    if ws_report.FilterMode: ws_report.AutoFilterMode = False
    
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    #Buscar fila de ENDs
    ends=ws_report.Range("A:A").Find("END").Row
    
    try:
        wb_os = excel.Workbooks.Open(pipeline,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {pipeline}')
    
    wb_os.Activate()
    hoja_acc=wb_os.ActiveSheet.Name
    ws_os=wb_acc.Sheets(hoja_acc)
    
    try:
        ws_os.Range("A:ZZ").ClearOutline()
    except:
        pass
    
    try:
        ws_os.Range("1:500").ClearOutline()
    except:
        pass
    
    if ws_os.FilterMode: ws_os.AutoFilterMode = False
    
    try:
        ws_os.Columns.EntireColumn.Hidden=False
        ws_os.EntireRow.Hidden=False
    except:
        pass
    
    #Contar cuántas filas nuevas hay
    used = ws_os.UsedRange
    nrows = used.Row + used.Rows.Count - 1
    
    if ends-1<nrows:
        dif=nrows-(ends-1)
    
        #Insertar filas nuevas
        ws_report.Rows(str(ends)+":"+str(ends+dif-1)).Insert(-4121)
        
        #Copiar fórmulas
        ws_report.Range(str("U"+str(ends-1)+":ZZ"+str(ends-1))).Copy()
        ws_report.Range(str("U"+str(ends)+":ZZ"+str(ends+dif-1))).PasteSpecial(Paste=constants.xlPasteFormulas)
        
    ws_os.Activate()
    ws_os.Range("A2:O"+str(nrows)).Copy()
    ws_report.Range("A2").PasteSpecial(Paste = constants.xlPasteValues)
    wb_os.Close(False)
    wb_report.Save()
    wb_report.Close(True)
    