# -*- coding: utf-8 -*-
"""
Created on Tue Oct  5 11:31:29 2021

@author: romina

Camelina
"""

import os
import shutil
from xlwings import App,Book
from Wolfram.functions.Excel_functions import insert_empty_rows, get_last_row_and_column, drag_formula, find_col, month_in_InputPC, colnum_string
from Wolfram.functions.math import transpose_list

def camelina_pc(PC_path, reporting_path, reporting_sheet, year, month):
    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    
    PC_filename = os.path.basename(PC_path)
    PC_folder_path = os.path.dirname(PC_path)
    PC_tmp_path = os.path.join(tmp_path, PC_filename)

    shutil.copy(PC_path, tmp_path)
    
    # Open PC wb
    app = App(visible=False)
    wb_PC = Book(PC_tmp_path)
    ws_PC = wb_PC.sheets['DATOS']

    # Extracting header of PC table
    ## Finding out column "Trabajador" (exact match)
    for i in range(1, 1000):
        if ws_PC.api.Range(colnum_string(i) + "1").Value == 'Trabajador':
            employee_col = i
            break
        
    ## Finding out column "Total Coste Trabaj."
    cost_col = ws_PC.api.Range("A1:DK1").Find("Total Coste Trabaj.").Column
    ## Last row of PC table
    last_row = ws_PC[0, employee_col - 1].end('down').row

    employees_name = ws_PC.range(
            colnum_string(employee_col) + "2:" + colnum_string(employee_col) + str(last_row)
            ).value
    employees_name = [employees_name[i].rstrip() for i in range(len(employees_name))] # delete spaces at the end of the name

    employees_cost = ws_PC.range(
            colnum_string(cost_col) + "2:" + colnum_string(cost_col) + str(last_row)
            ).value
    wb_PC.close()
    
    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    report_filename = os.path.basename(reporting_path)
    report_folder_path = os.path.dirname(reporting_path)
    report_tmp_path = os.path.join(tmp_path, report_filename)

    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    shutil.copy(reporting_path, tmp_path)
    
    # Open Reporting Excel -> WorkSheet: InputPC
    app = App(visible=False)
    wb_inp = Book(report_tmp_path)
    ws_inp = wb_inp.sheets[reporting_sheet]

    # get last row and last column (wo/ taking into accound "END" if exists)
    last_row, last_col =  get_last_row_and_column(ws_inp)

    # Add new rows for Employees costs
    ## Add empty rows before "END"
    insert_empty_rows(ws_inp, last_row + 1, len(employees_name))
    ## define range to copy = (new_first_row, nel_last_row)
    new_first_row = last_row + 1
    new_last_row = last_row + len(employees_name)

    # Add new PC data
    ## Add Employee names in column "Employee"
    employee_col = find_col(ws_inp, 1, last_col, ["Employee"], default_col = "B")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_col + str(new_first_row) +":" + employee_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_name)

    ## Add Employee costs in column "Company cost" or "Net"
    employee_cost_col = find_col(ws_inp, 1, last_col, ["Company cost"], default_col = "D")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_cost_col + str(new_first_row) +":" + employee_cost_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_cost)

    ## Add Account column
    account_col = find_col(ws_inp, 1, last_col, ["Account"], default_col = "C")
    ### Copy in ws "InputPC"
    ws_inp.range(account_col + str(new_first_row) +":" + account_col +  str(new_last_row)).options(index=False).value = ' Sueldos y salarios'

    # Add dates to column "Month"
    month_in_InputPC(ws_inp, last_col, new_first_row, new_last_row, year, month, month_header = "Date")

    # drag formulas and cell font yellow if #N/A
    for i in range(last_col):
        drag_formula(ws_inp, i, new_first_row-1, new_last_row, (255,255,0))

    # save and close
    wb_inp.save()
    wb_inp.close()
    
    app.kill()          
    
    shutil.copy(report_tmp_path, reporting_path)
    os.remove(report_tmp_path)
    os.remove(PC_tmp_path)