# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 09:46:30 2021

@author: romina

Circular
"""

import win32com.client as win32
from win32com.client import constants
import pandas as pd
from Wolfram.functions.dates_functions import ultimo_dia


def circular_pc(nomina,reporting,hoja_reporting,anno,mes):
    
    df=pd.read_excel(nomina,header=None)


    nom_ape=pd.concat([df.iloc[1],df.iloc[0]],axis=1)
    nom_ape=nom_ape[1]+" "+nom_ape[0]
    
    coste_index=df[df[0]=="Coste empresa"].index[0]
    costes=df.iloc[coste_index]
    
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = True
    excel.DisplayAlerts = False
    
    try:
        wb_report = excel.Workbooks.Open(reporting,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {reporting}')
       
    ws_report=wb_report.Sheets(hoja_reporting)
    ws_report.Range("A:ZZ").ClearOutline()
    ws_report.Range("1:500").ClearOutline()
    
    if ws_report.FilterMode: ws_report.AutoFilterMode = False
    
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    ws_report.Activate()
    ends=ws_report.Range("A:A").Find("END").Row
    
    empleados=len(nom_ape)-1
    
    ws_report.Rows(str(ends)+":"+str(ends+empleados-1)).Insert(-4121)

    for i in range(empleados):
        ws_report.Range("C"+str(ends+i)).Value=nom_ape[i+1]
        ws_report.Range("D"+str(ends+i)).Value=costes[i]
    
    ws_report.Range("A"+str(ends)+":A"+str(ends+empleados-1)).Value=ultimo_dia(int(anno),int(mes))
    ws_report.Range("A"+str(ends)+":A"+str(ends+empleados-1)).TextToColumns(Destination=ws_report.Range("A"+str(ends)), DataType=constants.xlDelimited,TextQualifier=constants.xlDoubleQuote,ConsecutiveDelimiter=False,
                                                                                Tab=False,Semicolon=False, Comma=False, Space=False, Other=False,    TrailingMinusNumbers=True)
    
    ws_report.Range("B"+str(ends)+":B"+str(ends+empleados-1)).Value="ESP"
    ws_report.Range("E"+str(ends-1)+":J"+str(ends-1)).Copy(ws_report.Range("E"+str(ends)+":J"+str(ends+empleados-1)))

    wb_report.Save()
    wb_report.Close(True)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()      