# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 15:34:47 2022

@author: Alberto Cañizares
"""
import os
import shutil
from time import sleep
from xlwings import App,Book
from Wolfram.functions.Excel_functions import *
from Wolfram.functions.math import transpose_list
from pandas import read_excel

         
###############################################################################    
    
def hubuc_pc(PC_path, reporting_path, reporting_sheet, year, month):  
    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
   
    # Open PC wb
    # Loading data
    PC_table = read_excel(PC_path).fillna("")

    # Locatting data
    employee_ini_row = PC_table.iloc[:,0].to_list().index("(00116) 001 SALARIO BA") - 4
    ini_col = 2

    # PC data
    PC_data = []
    ## Employee
    name = PC_table.iloc[employee_ini_row, ini_col:].to_list()
    surname_1 = PC_table.iloc[employee_ini_row + 1, ini_col:].to_list()
    surname_2 = PC_table.iloc[employee_ini_row + 2, ini_col:].to_list()

    PC_data.append([(name[i] + " " + surname_1[i] + " " + surname_2[i]).strip() for i in range(len(name))])

    ## Total devengos
    devengos_row = [i for i in range(len(PC_table)) if PC_table.iloc[i,0].strip() == "TOTAL DEVENGOS"][0]
    PC_data.append(PC_table.iloc[devengos_row, ini_col:].to_list())

    ## Variables y Bonus = 0
    PC_data.append([0.0 for i in range(len(PC_data[0]))])
    PC_data.append([0.0 for i in range(len(PC_data[0]))])

    # SS
    ss_row = [i for i in range(len(PC_table)) if PC_table.iloc[i,0].strip() == "COSTE S.S. EMPR."][0]
    PC_data.append(PC_table.iloc[ss_row, ini_col:].to_list())

    # PC vars
    PC_vars = [["Employee"], ["Total devengos"], ["Variable"], ["Bonus"], ["SS"]]

    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    
    report_filename = os.path.basename(reporting_path)
    report_folder_path = os.path.dirname(reporting_path)
    report_tmp_path = os.path.join(tmp_path, report_filename)

    shutil.copy(reporting_path, tmp_path)
    
    # Open Reporting Excel -> WorkSheet: InputPC
    app = App(visible=False)
    wb_inp = Book(report_tmp_path)
    ws_inp = wb_inp.sheets[reporting_sheet]

    # get last row and last column (wo/ taking into accound "END" if exists)
    try:
        last_row, last_col =  get_last_row_and_column(ws_inp)
    except:
        sleep(4.0)
        last_row, last_col =  get_last_row_and_column(ws_inp)

    # Add new rows for Employees costs
    ## Add empty rows before "END"
    insert_empty_rows(ws_inp, last_row + 1, len(PC_data[0]))
    ## define range to copy = (new_first_row, nel_last_row)
    new_first_row = last_row + 1
    new_last_row = last_row + len(PC_data[0])

    # Add new PC data
    for i in range(len(PC_data)):
        ## Find col to write
        col_of_int = find_col(ws_inp, 1, last_col, PC_vars[i])
        ### Copy in ws "InputPC"
        if col_of_int != None:
            ws_inp.range(col_of_int + str(new_first_row) +":" + col_of_int +  str(new_last_row)).options(index=False).value = transpose_list(PC_data[i])

    # Add dates to column "Month"
    month_in_InputPC(ws_inp, last_col, new_first_row, new_last_row, year, month, month_header = "Month")

    # drag formulas and cell font yellow if #N/A
    for i in range(last_col):
        drag_formula(ws_inp, i, new_first_row-1, new_last_row, (255,255,0))

    # save and close
    wb_inp.save()
    wb_inp.close()
    
    app.kill()
    shutil.copy(report_tmp_path, reporting_path)
    os.remove(report_tmp_path)