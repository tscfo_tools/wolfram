# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 16:10:59 2021

@author: romina

Build38 ESP
"""
from time import sleep
import win32com.client as win32
from win32com.client import constants
from os.path import basename
from Wolfram.functions.dates_functions import colnum_string, ultimo_dia


def build_esp_pc(nomina,reporting,hoja_reporting,anno,mes):
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False
    excel.DisplayAlerts = False
    
    try:
        wb_report = excel.Workbooks.Open(reporting,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {reporting}')
       
    
    try:
        try:
            wb_nomina = excel.Workbooks.Open(nomina,False,None)
        except:
            sleep(4.0)
            wb_nomina = excel.Workbooks.Open(nomina,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {nomina}')
    
    try:
        ws_report=wb_report.Sheets(hoja_reporting)
    except:
        sleep(4.0)
        ws_report=wb_report.Sheets(hoja_reporting)
    wb_nomina.Activate()
    hoja_nomina1="1"
    ws_nomina=wb_nomina.Sheets(hoja_nomina1)
    
    ws_report.Range("A:ZZ").ClearOutline()
    ws_report.Range("1:500").ClearOutline()
    
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    ws_nomina.Range("A:ZZ").ClearOutline()
    ws_nomina.Range("1:500").ClearOutline()
    
    try:
        ws_nomina.Columns.EntireColumn.Hidden=False
        ws_nomina.EntireRow.Hidden=False
    except:
        pass
    if ws_report.FilterMode: ws_report.AutoFilterMode = False
    
    lastCol = ws_nomina.UsedRange.Columns.Count
    
    empleados=lastCol-3
    
    ws_report.Activate()
    ends=ws_report.Range("A:A").Find("END").Row
    
    #Insertar filas en InputPC
    ws_report.Range(str(ends)+":"+str(ends+empleados-1)).EntireRow.Insert()
    
    nominas_name=basename(nomina)
    ws_report.Range("B"+str(ends)).Value="='[" + nominas_name + "]" + hoja_nomina1 +"'!D$7&\" \"&'[" + nominas_name + "]" + hoja_nomina1 + "'!D$5&\" \"&'[" + nominas_name + "]" + hoja_nomina1 + "'!D$6"
    
    hasta=colnum_string(empleados+1)
    ws_report.Range("B"+str(ends)).Copy(ws_report.Range("C"+str(ends)+":"+hasta+str(ends)))
    
    ws_report.Range("B"+str(ends)+":"+hasta+str(ends)).Copy()
    ws_report.Range("B"+str(ends)+":"+hasta+str(ends)).PasteSpecial(Paste = constants.xlPasteValues)  
    ws_report.Range("C"+str(ends)+":"+hasta+str(ends)).Copy()
    ws_report.Range("B"+str(ends+1)).PasteSpecial(Transpose=True)    
    ws_report.Range("C"+str(ends)+":"+hasta+str(ends)).Clear()
    
    #Poner mes
    ws_report.Range("A"+str(ends)+":A"+str(ends+empleados-1)).Value=ultimo_dia(int(anno), int(mes))
    ws_report.Range("A"+str(ends)+":A"+str(ends+empleados-1)).TextToColumns(Destination=ws_report.Range("A"+str(ends)), DataType=constants.xlDelimited,TextQualifier=constants.xlDoubleQuote,ConsecutiveDelimiter=False,
                                                                                Tab=False,Semicolon=False, Comma=False, Space=False, Other=False,    TrailingMinusNumbers=True)
    
    #Copiar coste empresa
    ws_nomina.Activate()
    coste=ws_nomina.Range("A:A").Find("COSTE EMPRESA").Row
    ws_nomina.Range("D" + str(coste) + ":" + colnum_string(lastCol) + str(coste)).Copy()
    ws_report.Range("C"+str(ends)).PasteSpecial(Paste = constants.xlPasteValues,Transpose=True) 
    
    #Copiar formatos de columnas A,B y C
    ws_report.Range("A" + str(ends-1)+":C"+str(ends-1)).Copy()
    ws_report.Range("A" + str(ends) +":C" +str(ends+empleados-1)).PasteSpecial(-4122)
    


    #Copiar fórmulas de la D a la H
    ws_report.Range("D" + str(ends-1)+":H"+str(ends-1)).Copy()
    ws_report.Range("D" + str(ends) +":H" +str(ends+empleados-1)).PasteSpecial(-4123)

    try:
        hoja_nomina2="3"
        ws_nomina=wb_nomina.Sheets(hoja_nomina2)
        ends=ws_report.Range("A:A").Find("END").Row
        ws_report.Range(str(ends)+":"+str(ends)).Insert()
        ws_report.Range("B"+str(ends)).Value="='[" + nominas_name + "]" + hoja_nomina2 +"'!D$7&\" \"&'[" + nominas_name + "]" + hoja_nomina2 + "'!D$5&\" \"&'[" + nominas_name + "]" + hoja_nomina2 + "'!D$6"
        ws_report.Range("B"+str(ends)).Copy()
        ws_report.Range("B"+str(ends)).PasteSpecial(Paste = constants.xlPasteValues)  
        ws_report.Range("C"+str(ends)).Value=ws_nomina.Range("D33").Value
        ws_report.Range("A"+str(ends)).Value=ultimo_dia(int(anno), int(mes))
        ws_report.Range("A"+str(ends)).TextToColumns(Destination=ws_report.Range("A"+str(ends)), DataType=constants.xlDelimited,TextQualifier=constants.xlDoubleQuote,ConsecutiveDelimiter=False,
                                                                                Tab=False,Semicolon=False, Comma=False, Space=False, Other=False,    TrailingMinusNumbers=True)
        ws_report.Range("D" + str(ends-1)+":H"+str(ends-1)).Copy()
        ws_report.Range("D" + str(ends) +":H" +str(ends)).PasteSpecial(-4123)
    except:
        pass



    try:
        PC_ws_name = "5"
        ws_nomina = wb_nomina.Sheets(PC_ws_name)

        lastCol = ws_nomina.UsedRange.Columns.Count
    
        empleados=lastCol-3
        
        ws_report.Activate()
        ends=ws_report.Range("A:A").Find("END").Row
        
        #Insertar filas en InputPC
        ws_report.Range(str(ends)+":"+str(ends+empleados-1)).EntireRow.Insert()
        
        nominas_name=basename(nomina)
        ws_report.Range("B"+str(ends)).Value="='[" + nominas_name + "]" + PC_ws_name +"'!D$7&\" \"&'[" + nominas_name + "]" + PC_ws_name + "'!D$5&\" \"&'[" + nominas_name + "]" + PC_ws_name + "'!D$6"
        
        hasta=colnum_string(empleados+1)
        ws_report.Range("B"+str(ends)).Copy(ws_report.Range("C"+str(ends)+":"+hasta+str(ends)))
        
        ws_report.Range("B"+str(ends)+":"+hasta+str(ends)).Copy()
        ws_report.Range("B"+str(ends)+":"+hasta+str(ends)).PasteSpecial(Paste = constants.xlPasteValues)  
        ws_report.Range("C"+str(ends)+":"+hasta+str(ends)).Copy()
        ws_report.Range("B"+str(ends+1)).PasteSpecial(Transpose=True)    
        ws_report.Range("C"+str(ends)+":"+hasta+str(ends)).Clear()
        
        #Poner mes
        ws_report.Range("A"+str(ends)+":A"+str(ends+empleados-1)).Value=ultimo_dia(int(anno), int(mes))
        ws_report.Range("A"+str(ends)+":A"+str(ends+empleados-1)).TextToColumns(Destination=ws_report.Range("A"+str(ends)), DataType=constants.xlDelimited,TextQualifier=constants.xlDoubleQuote,ConsecutiveDelimiter=False,
                                                                                    Tab=False,Semicolon=False, Comma=False, Space=False, Other=False,    TrailingMinusNumbers=True)
        
        #Copiar coste empresa
        ws_nomina.Activate()
        coste=ws_nomina.Range("A:A").Find("COSTE EMPRESA").Row
        ws_nomina.Range("D" + str(coste) + ":" + colnum_string(lastCol) + str(coste)).Copy()
        ws_report.Range("C"+str(ends)).PasteSpecial(Paste = constants.xlPasteValues,Transpose=True) 
        
        #Copiar formatos de columnas A,B y C
        ws_report.Range("A" + str(ends-1)+":C"+str(ends-1)).Copy()
        ws_report.Range("A" + str(ends) +":C" +str(ends+empleados-1)).PasteSpecial(-4122)
        


        #Copiar fórmulas de la D a la H
        ws_report.Range("D" + str(ends-1)+":H"+str(ends-1)).Copy()
        ws_report.Range("D" + str(ends) +":H" +str(ends+empleados-1)).PasteSpecial(-4123)
    except:
        pass

    wb_report.Save()
    wb_report.Close(True)
    wb_nomina.Close(False)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit() 
