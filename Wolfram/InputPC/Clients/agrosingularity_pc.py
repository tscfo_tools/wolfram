# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 16:26:08 2021

@author: romina

Agrosingularity

"""

import win32com.client as win32
import pandas as pd
from Wolfram.functions.dates_functions import ultimo_dia
from win32com.client import constants


def pdf_to_excel(nominas):
    nuevo_nombre=nominas.replace(".PDF",".xlsx").replace(".pdf",".xlsx").replace("/","\\")
    
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False
    excel.DisplayAlerts = False
    
    try:
        wb_nomina = excel.Workbooks.Add()
    except:
        print('Failed to open spreadsheet.')
        
    ws=wb_nomina.ActiveSheet.Name
    ws=wb_nomina.Sheets(ws)
    
    wb_nomina.Queries.Add(Name="Page001", Formula=
                               "let" + chr(13) + "" + chr(10) + "    Source = Pdf.Tables(File.Contents(\""+nominas+"\"), [Implementation=\"1.3\"])," + chr(13) + "" + chr(10) + "    Page1 = Source{[Id=\"Page001\"]}[Data]," + chr(13) + "" + chr(10) + "    #\"Promoted Headers\" = Table.PromoteHeaders(Page1, [PromoteAllScalars=true])," + chr(13) + "" + chr(10) + "    #\"Changed Type\" = Table.TransformColumnTypes(#\"Promoted Headers\",{})" + chr(13) + "" + chr(10) + "in" + chr(13) + "" + chr(10) + "    #\"Changed Type\"")
    
    
    x=ws.ListObjects.Add(SourceType=0,Source="OLEDB;Provider=Microsoft.Mashup.OleDb.1;Data Source=$Workbook$;Location=Page001;Extended Properties=\"\"",
                            Destination=ws.Range("$A$1")).QueryTable
    x.CommandType = constants.xlCmdSql
    x.CommandText = ("SELECT * FROM [Page001]")
    x.RowNumbers = False
    x.FillAdjacentFormulas = False
    x.PreserveFormatting = True
    x.RefreshOnFileOpen = False
    x.BackgroundQuery = True
    x.RefreshStyle = constants.xlInsertDeleteCells
    x.SavePassword = False
    x.AdjustColumnWidth = True
    x.SaveData = True
    x.RefreshPeriod = 0
    x.PreserveColumnInfo = True
    x.ListObject.DisplayName = "Page001"
    x.BackgroundQuery=False
    x.Refresh()==True
    
    
    wb_nomina.SaveAs(nuevo_nombre)
    wb_nomina.Close()
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()  
    return nuevo_nombre


def agrosingularity_pc(nominas,reporting,hoja_reporting,anno,mes):
    nomina=pdf_to_excel(nominas)
    df=pd.read_excel(nomina,header=5)
    empleados=df.TRABAJADOR.iloc[0:len(df)-3]
    costes=df["COSTE EMPR"].iloc[0:len(df)-3]
    
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False
    excel.DisplayAlerts = False
    
    try:
        wb_report = excel.Workbooks.Open(reporting,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {reporting}')
       
    ws_report=wb_report.Sheets(hoja_reporting)
    ws_report.Range("A:ZZ").ClearOutline()
    ws_report.Range("1:500").ClearOutline()
    
    if ws_report.FilterMode: ws_report.AutoFilterMode = False
    
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    ws_report.Activate()
    ends=ws_report.Range("A:A").Find("END").Row
    
    ws_report.Rows(str(ends)+":"+str(ends+len(empleados)-1)).Insert(-4121)
    for i in range(0,len(empleados)):
        ws_report.Range("C"+str(ends+i)).Value=empleados[i]
        ws_report.Range("D"+str(ends+i)).Value=costes[i]
        ws_report.Range("A"+str(ends+i)).Value=anno
        
    ws_report.Range("B"+str(ends)+":B"+str(ends+len(empleados)-1)).Value=ultimo_dia(int(anno),int(mes))
    ws_report.Range("B"+str(ends)+":B"+str(ends+len(empleados)-1)).TextToColumns(Destination=ws_report.Range("B"+str(ends)), DataType=constants.xlDelimited,TextQualifier=constants.xlDoubleQuote,ConsecutiveDelimiter=False,
                                                                                Tab=False,Semicolon=False, Comma=False, Space=False, Other=False,    TrailingMinusNumbers=True)
    ws_report.Range("E"+str(ends-1)+":G"+str(ends-1)).Copy(ws_report.Range("E"+str(ends)+":G"+str(ends+len(empleados)-1)))
    
    wb_report.Save()
    wb_report.Close(True)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()   
    