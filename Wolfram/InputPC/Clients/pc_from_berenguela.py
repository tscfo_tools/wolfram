# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 15:34:47 2022

@author: Alberto Cañizares
"""

import os
import shutil
from xlwings import App,Book
from pandas import read_excel
from Wolfram.functions.Excel_functions import insert_empty_rows, get_last_row_and_column, drag_formula, find_col, month_in_InputPC
from Wolfram.functions.math import transpose_list


def extract_data_from_PC(PC_table, row_name, data_ref = ''):
    # Rows if interest
    rows_of_int = list(PC_table[PC_table[0]==row_name].axes[0])
    # Extract data from rows of interest
    data = []
    for i in rows_of_int:
        data = data + list(PC_table.iloc[i][1:])

    # Drop last element of list because is the total number
    data = data[:-1] 
    # Drop empty elements
    if data_ref != '':
        # for deleting last data where there is no employee
        data = [data[i] for i in range(len(data)) if str(data_ref[i]) != 'nan'] 
        return data
    else:
        data_filt = [x for x in data if str(x) != 'nan'] # for "NOMBRE TRABAJADOR"
    
        return data_filt, data



         
###############################################################################    
    
def pc_from_berenguela(PC_path, reporting_path, reporting_sheet, year, month):  
    # Load PC file
    PC_table = read_excel(PC_path, header=None)

    ## Extract data from PC table
    ### Extract Employees name
    employees_name, employees_name_raw = extract_data_from_PC(PC_table, "NOMBRE TRABAJADOR")
    ### Extract Employees cost
    employees_cost = extract_data_from_PC(PC_table, "COSTE TOTAL EMPRESA", employees_name_raw)
    ### Extract Gross salary -> DEVENGADO EMPRESA
    gross_salary = extract_data_from_PC(PC_table, "DEVENGADO EMPRESA", employees_name_raw)
    ### Extract Seguridad Social -> SEG. SOCIAL C/EMPRESA
    ss = extract_data_from_PC(PC_table, "SEG. SOCIAL C/EMPRESA", employees_name_raw)
    ## Extra vars for new model
    ### Extract "Total devengos" -> TOTAL DEVENGOS
    total_devengos_values = extract_data_from_PC(PC_table, "TOTAL DEVENGOS", employees_name_raw)
    ### Extract "Variable" -> "VARIABLE"
    if "VARIABLE" in list(PC_table.iloc[:,0]):
        variable_values = extract_data_from_PC(PC_table, "VARIABLE", employees_name_raw)
    else:
        variable_values = [0.0 for i in range(len(employees_cost))]
    ### Extract "Bonus" -> "BONUS" or "BONUS EXTRA NO CONSOL.""
    if "BONUS" in list(PC_table.iloc[:,0]):
        bonus_values = extract_data_from_PC(PC_table, "BONUS", employees_name_raw)
    elif "BONUS EXTRA NO CONSOL." in list(PC_table.iloc[:,0]):
        bonus_values = extract_data_from_PC(PC_table, "BONUS EXTRA NO CONSOL.", employees_name_raw)
    else:
        bonus_values = [0.0 for i in range(len(employees_cost))]

    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    report_filename = os.path.basename(reporting_path)
    report_folder_path = os.path.dirname(reporting_path)
    report_tmp_path = os.path.join(tmp_path, report_filename)

    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    shutil.copy(reporting_path, tmp_path)
    
    # Open Reporting Excel -> WorkSheet: InputPC
    app = App(visible=False)
    wb_inp = Book(report_tmp_path)
    ws_inp = wb_inp.sheets[reporting_sheet]
    
    # delete posible filters and displaying possible hidden columns
    ws_inp.api.Rows.EntireRow.Hidden = False
    ws_inp.api.AutoFilterMode = False


    # get last row and last column (wo/ taking into accound "END" if exists)
    last_row, last_col =  get_last_row_and_column(ws_inp)

    # Add new rows for Employees costs
    ## Add empty rows before "END"
    insert_empty_rows(ws_inp, last_row + 1, len(employees_name))
    ## define range to copy = (new_first_row, nel_last_row)
    new_first_row = last_row + 1
    new_last_row = last_row + len(employees_name)

    # Add new PC data
    ## Add Employee names in column "Employee"
    employee_col = find_col(ws_inp, 1, last_col, ["Employee"], default_col = "B")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_col + str(new_first_row) +":" + employee_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_name)

    ## Add Employee costs in column "Company cost" or "Net"
    employee_cost_col = find_col(ws_inp, 1, last_col, ["Company cost", "Net"], default_col = "C")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_cost_col + str(new_first_row) +":" + employee_cost_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_cost)

    # Add dates to column "Month"
    month_in_InputPC(ws_inp, last_col, new_first_row, new_last_row, year, month, month_header = "Month")

    # Other possible cols to add: "Gross Salary", "Total devengos", "Variable", "Bonus" or "SS"
    gross_col = find_col(ws_inp, 1, last_col, ["Gross salary"])
    if gross_col != None:
        # Copy in ws
        ws_inp.range(gross_col + str(new_first_row) +":" + gross_col +  str(new_last_row)).options(index=False).value = transpose_list(gross_salary)

    gross_col = find_col(ws_inp, 1, last_col, ["Total devengos"])
    if gross_col != None:
        # Copy in ws
        ws_inp.range(gross_col + str(new_first_row) +":" + gross_col +  str(new_last_row)).options(index=False).value = transpose_list(total_devengos_values)

    gross_col = find_col(ws_inp, 1, last_col, ["Variable"])
    if gross_col != None:
        # Copy in ws
        ws_inp.range(gross_col + str(new_first_row) +":" + gross_col +  str(new_last_row)).options(index=False).value = transpose_list(variable_values)

    gross_col = find_col(ws_inp, 1, last_col, ["Bonus"])
    if gross_col != None:
        # Copy in ws
        ws_inp.range(gross_col + str(new_first_row) +":" + gross_col +  str(new_last_row)).options(index=False).value = transpose_list(bonus_values)

    ss_col = find_col(ws_inp, 1, last_col, ["SS"])
    if ss_col != None:
        # Copy in ws
        ws_inp.range(ss_col + str(new_first_row) +":" + ss_col +  str(new_last_row)).options(index=False).value = transpose_list(ss)
      

    # drag formulas and cell font yellow if #N/A
    for i in range(last_col):
        drag_formula(ws_inp, i, new_first_row-1, new_last_row, (255,255,0))

    # save and close
    wb_inp.save()
    wb_inp.close()
    
    app.kill()

    shutil.copy(report_tmp_path, reporting_path)
    os.remove(report_tmp_path)
