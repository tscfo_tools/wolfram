
import os
import shutil
from xlwings import App, Book
from Wolfram.functions.Excel_functions import *
from Wolfram.functions.math import transpose_list

###############################################################################    
    
def climatetrade_pc(PC_path, reporting_path, reporting_sheet, year, month):
    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    
    PC_filename = os.path.basename(PC_path)
    PC_folder_path = os.path.dirname(PC_path)
    PC_tmp_path = os.path.join(tmp_path, PC_filename)

    shutil.copy(PC_path, tmp_path)
    
    # Open PC wb
    app = App(visible=False)
    wb_mayor = Book(PC_tmp_path)
    ws_mayor = wb_mayor.sheets[0]

    # Extracting header of PC table
    ## Header row of PC table
    header_row = ws_mayor.api.Range("A1:AA1000").Find("TRABAJADOR").Row
    ## Extracting columns for each variable of interest
    employee_col = ws_mayor.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("TRABAJADOR").Column
    cost_col = ws_mayor.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("C.TOTAL").Column
    ## Last row of PC table -> There an empty row btw header and table body
    last_row = ws_mayor[(header_row - 1) + 2, employee_col - 1].end('down').row

    # Extracting data from Excel
    ## Employees name
    employees_name = ws_mayor.range(
        colnum_string(employee_col) + str(header_row + 2) + ":" + colnum_string(employee_col) + str(last_row)
        ).value
    ## Company cost
    Company_cost = ws_mayor.range(
        colnum_string(cost_col) + str(header_row + 2) + ":" + colnum_string(cost_col) + str(last_row)
        ).value

    # Close PC wb
    wb_mayor.close()
    
    shutil.copyfile(PC_tmp_path, PC_path)

    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    
    report_filename = os.path.basename(reporting_path)
    report_folder_path = os.path.dirname(reporting_path)
    report_tmp_path = os.path.join(tmp_path, report_filename)

    shutil.copy(reporting_path, tmp_path)
    
    # Open Reporting Excel -> WorkSheet: InputPC
    app = App(visible=False)
    wb_inp = Book(report_tmp_path)
    ws_inp = wb_inp.sheets[reporting_sheet]

    # get last row and last column (wo/ taking into accound "END" if exists)
    last_row, last_col =  get_last_row_and_column(ws_inp)

    # Add new rows for Employees costs
    ## Add empty rows before "END"
    insert_empty_rows(ws_inp, last_row + 1, len(employees_name))
    ## define range to copy = (new_first_row, nel_last_row)
    new_first_row = last_row + 1
    new_last_row = last_row + len(employees_name)

    # Add new PC data
    ## Add Employee names in column "Employee"
    employee_col = find_col(ws_inp, 1, last_col, ["Employee"], default_col = "B")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_col + str(new_first_row) +":" + employee_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_name)

    ## Add Employee costs in column "Company cost" or "Net"
    employee_cost_col = find_col(ws_inp, 1, last_col, ["Company cost"], default_col = "C")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_cost_col + str(new_first_row) +":" + employee_cost_col +  str(new_last_row)).options(index=False).value = transpose_list(Company_cost)

    # Add dates to column "Month"
    month_in_InputPC(ws_inp, last_col, new_first_row, new_last_row, year, month, month_header = "Month")

    # drag formulas and cell font yellow if #N/A
    for i in range(last_col):
        drag_formula(ws_inp, i, new_first_row-1, new_last_row, (255,255,0))

    # save and close
    wb_inp.save()
    wb_inp.close()

    app.kill()
    
    shutil.copy(report_tmp_path, reporting_path)
    os.remove(report_tmp_path)
    os.remove(PC_tmp_path)