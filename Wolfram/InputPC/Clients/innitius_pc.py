import os
import shutil
from xlwings import App, Book
from Wolfram.functions.Excel_functions import insert_empty_rows, get_last_row_and_column, drag_formula, find_col, colnum_string, month_in_InputPC
from Wolfram.functions.math import transpose_list

###############################################################################    
    
def innitius_pc(PC_path, reporting_path, reporting_sheet, year, month):
    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    
    PC_filename = os.path.basename(PC_path)
    PC_folder_path = os.path.dirname(PC_path)
    PC_tmp_path = os.path.join(tmp_path, PC_filename)

    shutil.copy(PC_path, tmp_path)
    
    # Open PC wb
    app = App(visible=False)
    wb_mayor = Book(PC_tmp_path)
    ws_mayor = wb_mayor.sheets[0]

    # Extracting header of PC table
    ## Header row of PC table
    header_row = ws_mayor.api.Range("A1:AA1000").Find("Apellidos y Nombre").Row
    ## Extracting columns for each variable of interest
    employee_col = ws_mayor.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Apellidos y Nombre").Column
    IRPF_col = ws_mayor.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Base IRPF").Column
    SS_col = ws_mayor.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("S.S. Empresa").Column
    Dietas_col = ws_mayor.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Dietas").Column
    ## Last row of PC table
    last_row = ws_mayor[header_row - 1, employee_col - 1].end('down').row

    # Extracting data from Excel
    ## Employees name
    employees_name = ws_mayor.range(
        colnum_string(employee_col) + str(header_row+1) + ":" + colnum_string(employee_col) + str(last_row)
        ).value
    ## Company cost = IRPF + SS + Dietas
    IRPF = ws_mayor.range(
        colnum_string(IRPF_col) + str(header_row+1) + ":" + colnum_string(IRPF_col) + str(last_row)
        ).value
    SS = ws_mayor.range(
        colnum_string(SS_col) + str(header_row+1) + ":" + colnum_string(SS_col) + str(last_row)
        ).value
    Dietas = ws_mayor.range(
        colnum_string(Dietas_col) + str(header_row+1) + ":" + colnum_string(Dietas_col) + str(last_row)
        ).value
    ### Sum for Company Cost
    Company_cost = ['%.2f' % (float(IRPF[i]) + float(SS[i]) + float(Dietas[i])) for i in range(len(IRPF))]

    # Close PC wb
    wb_mayor.close()


    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    
    report_filename = os.path.basename(reporting_path)
    report_folder_path = os.path.dirname(reporting_path)
    report_tmp_path = os.path.join(tmp_path, report_filename)

    shutil.copy(reporting_path, tmp_path)
    
    # Open Reporting Excel -> WorkSheet: InputPC
    app = App(visible=False)
    wb_inp = Book(report_tmp_path)
    ws_inp = wb_inp.sheets[reporting_sheet]

    # get last row and last column (wo/ taking into accound "END" if exists)
    last_row, last_col =  get_last_row_and_column(ws_inp)

    # Add new rows for Employees costs
    ## Add empty rows before "END"
    insert_empty_rows(ws_inp, last_row + 1, len(employees_name))
    ## define range to copy = (new_first_row, nel_last_row)
    new_first_row = last_row + 1
    new_last_row = last_row + len(employees_name)

    # Add new PC data
    ## Add Employee names in column "Employee"
    employee_col = find_col(ws_inp, 1, last_col, ["Employee"], default_col = "B")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_col + str(new_first_row) +":" + employee_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_name)

    ## Add Employee costs in column "Company cost" or "Net"
    employee_cost_col = find_col(ws_inp, 1, last_col, ["Company cost"], default_col = "C")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_cost_col + str(new_first_row) +":" + employee_cost_col +  str(new_last_row)).options(index=False).value = transpose_list(Company_cost)

    # Add dates to column "Month"
    month_in_InputPC(ws_inp, last_col, new_first_row, new_last_row, year, month, month_header = "Month")

    # drag formulas and cell font yellow if #N/A
    for i in range(last_col):
        drag_formula(ws_inp, i, new_first_row-1, new_last_row, (255,255,0))

    # save and close
    wb_inp.save()
    wb_inp.close()

    app.kill()
    shutil.copy(report_tmp_path, reporting_path)
    os.remove(report_tmp_path)
    os.remove(PC_tmp_path)