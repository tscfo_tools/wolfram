# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 13:31:33 2021

@author: romina

Nova ESP
"""

import win32com.client as win32
from win32com.client import constants
from Wolfram.functions.dates_functions import ultimo_dia

def nova_esp_pc(nomina,reporting,hoja_reporting,anno,mes):
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False
    excel.DisplayAlerts = False
    
    try:
        wb_report = excel.Workbooks.Open(reporting,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {reporting}')
       
    
    try:
        wb_nomina = excel.Workbooks.Open(nomina,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {nomina}')
    
    ws_report=wb_report.Sheets(hoja_reporting)
    wb_nomina.Activate()
    hoja_nomina=wb_nomina.ActiveSheet.Name
    ws_nomina=wb_nomina.Sheets(hoja_nomina)
    
    ws_report.Range("A:ZZ").ClearOutline()
    ws_report.Range("1:500").ClearOutline()
    
    if ws_report.FilterMode: ws_report.AutoFilterMode = False
    
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    ws_nomina.Range("A:ZZ").ClearOutline()
    ws_nomina.Range("1:500").ClearOutline()
    
    try:
        ws_nomina.Columns.EntireColumn.Hidden=False
        ws_nomina.EntireRow.Hidden=False
    except:
        pass
    
    
    ultima=ws_nomina.Range("A2").End(-4121).Row
    total=ultima-2
    
    ws_report.Activate()
    ends=ws_report.Range("A:A").Find("END").Row
    
    ws_report.Range(str(ends)+":"+str(ends+total-1)).Insert()
    
    ws_nomina.Range("A3"+":A"+str(ultima)).Copy(ws_report.Range("C"+str(ends)))
    ws_nomina.Range("B3"+":B"+str(ultima)).Copy(ws_report.Range("D"+str(ends)))
    
    ws_report.Range("B"+str(ends)+":B"+str(ends+total-1)).Value="ESP"
    ws_report.Range("A"+str(ends)+":A"+str(ends+total-1)).Value=ultimo_dia(int(anno),int(mes))
    ws_report.Range("A"+str(ends)+":A"+str(ends+total-1)).TextToColumns(Destination=ws_report.Range("A"+str(ends)), DataType=constants.xlDelimited,TextQualifier=constants.xlDoubleQuote,ConsecutiveDelimiter=False,
                                                                                Tab=False,Semicolon=False, Comma=False, Space=False, Other=False,    TrailingMinusNumbers=True)
    
    
    ws_report.Range("F"+str(ends-1)+":J"+str(ends-1)).Copy(ws_report.Range("F"+str(ends)+":J"+str(ends+total-1)))
    
    
    wb_report.Save()
    wb_report.Close(True)
    wb_nomina.Close(False)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()  