# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 16:10:59 2021

@author: romina

Build38 ESP
"""

import os
import shutil
from xlwings import App,Book
from pandas import read_excel
from Wolfram.functions.Excel_functions import insert_empty_rows, get_last_row_and_column, drag_formula, find_col, month_in_InputPC
from Wolfram.functions.math import transpose_list


def nora_pc(PC_path, reporting_path, reporting_sheet, year, month):  
    # Load PC file
    PC_tab = read_excel(PC_path)

    # Only columns of interests: Empleados and 'Coste total empresa' for current month
    months_str = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic']
    cols_of_int = [0] # Col 0 for "Empleados"
    for i in range(len(PC_tab.columns)):
        if (str(year)[-2:] in PC_tab.columns[i]) and (months_str[month-1] in PC_tab.columns[i]) and (PC_tab.iloc[0, i] == 'Coste total empresa'):
            cols_of_int.append(i)
        else:
            pass
    ## Extract data from PC table
    ### Extract Employees name
    employees_name = list(PC_tab.iloc[1:,cols_of_int[0]])
    ### Extract Employees cost
    employees_cost = list(PC_tab.iloc[1:,cols_of_int[1]])

    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    report_filename = os.path.basename(reporting_path)
    report_folder_path = os.path.dirname(reporting_path)
    report_tmp_path = os.path.join(tmp_path, report_filename)

    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    shutil.copy(reporting_path, tmp_path)
    
    # Open Reporting Excel -> WorkSheet: InputPC
    app = App(visible=False)
    wb_inp = Book(report_tmp_path)
    ws_inp = wb_inp.sheets[reporting_sheet]

    # get last row and last column (wo/ taking into accound "END" if exists)
    last_row, last_col =  get_last_row_and_column(ws_inp)

    # Add new rows for Employees costs
    ## Add empty rows before "END"
    insert_empty_rows(ws_inp, last_row + 1, len(employees_name))
    ## define range to copy = (new_first_row, nel_last_row)
    new_first_row = last_row + 1
    new_last_row = last_row + len(employees_name)

    # Add new PC data
    ## Add Employee names in column "Employee"
    employee_col = find_col(ws_inp, 1, last_col, ["Employee"], default_col = "B")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_col + str(new_first_row) +":" + employee_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_name)

    ## Add Employee costs in column "Company cost" or "Net"
    employee_cost_col = find_col(ws_inp, 1, last_col, ["Net"], default_col = "C")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_cost_col + str(new_first_row) +":" + employee_cost_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_cost)

    # Add dates to column "Month"
    month_in_InputPC(ws_inp, last_col, new_first_row, new_last_row, year, month, month_header = "Month")

    # drag formulas and cell font yellow if #N/A
    for i in range(last_col):
        drag_formula(ws_inp, i, new_first_row-1, new_last_row, (255,255,0))

    # save and close
    wb_inp.save()
    wb_inp.close()
    
    app.kill()

    shutil.copy(report_tmp_path, reporting_path)
    os.remove(report_tmp_path)