# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 17:06:51 2021

@author: romina

Usizy
"""

from xlwings import App,Book
import pandas as pd
from Wolfram.functions.dates_functions import ultimo_dia, colnum_string
import datetime as dt

def usizy_pc(nomina,reporting,hoja_reporting,anno,mes):
    #importar tabla de nominas como pandas dataframe para saber cuantas filas de empleados hay

    nomina_df=pd.read_excel(nomina,header=None)
    filas_nombre=list(nomina_df[nomina_df[0]=="NOMBRE TRABAJADOR"].axes[0])
    last_col=len(nomina_df.columns)
    nombres=nomina_df.iloc[filas_nombre[0]][1:last_col]
    filas_nombre.pop(0)
    for linea in filas_nombre:
        nombres=nombres.append(nomina_df.iloc[linea][1:last_col])
        
    nombres.dropna(inplace=True)
    nombres=nombres[0:len(nombres)-1]
    
    filas_coste=list(nomina_df[nomina_df[0]=="COSTE TOTAL EMPRESA"].axes[0])
    costes=nomina_df.iloc[filas_coste[0]][1:last_col]
    filas_coste.pop(0)
    for linea in filas_coste:
        costes=costes.append(nomina_df.iloc[linea][1:last_col])
    costes=costes[costes!=0]
    costes=costes[0:len(costes)-1]
    
    
    app = App(visible=False)
    
    wb_inp=Book(reporting)
    ws_inp=wb_inp.sheets[hoja_reporting]
    
    used_range_rows = (ws_inp.api.UsedRange.Row,	ws_inp.api.UsedRange.Row + ws_inp.api.UsedRange.Rows.Count)[1]-1
    #Añadir filas antes del END
    rango=str(used_range_rows) +":" + str(used_range_rows + len(nombres)-1)
    ws_inp.range(rango).insert()
    
    
    #Añadir nombres
    rango_nombres="B" + str(used_range_rows) +":B" +  str(used_range_rows + len(nombres)-1)
    ws_inp.range(rango_nombres).options(index=False).value=nombres

    #Añadir costos
    rango_costo="C" + str(used_range_rows) +":C" +  str(used_range_rows + len(nombres)-1)
    ws_inp.range(rango_costo).options(index=False).value=costes

    #Añadir fechas
    rango_fecha="A" + str(used_range_rows) +":A" +  str(used_range_rows + len(nombres)-1)
    ws_inp.range(rango_fecha).value=(dt.datetime.strptime(ultimo_dia(int(anno),int(mes)),"%d/%m/%Y")-dt.datetime(1900,1,1)).days+1
    
    #Copiar fórmulas
    rango_antes="D" + str(used_range_rows-1) +":F" +  str(used_range_rows -1)
    rango_formulas="D" + str(used_range_rows) +":F" +  str(used_range_rows + len(nombres)-1)
    ws_inp.range(rango_antes).copy()
    ws_inp.range(rango_formulas).paste("formulas")


    #Copiar formato fecha
    rango_antes="A" + str(used_range_rows-1) +":A" +  str(used_range_rows -1)
    ws_inp.range(rango_antes).copy()
    ws_inp.range(rango_fecha).paste("formats")


    wb_inp.save()
    wb_inp.close()
     
    app.kill()
    
def usizy_accounts(reporting, holded, hoja_accounts, mes):
    
    #Primero editar la hoja de holded
    
    app = App(visible=False)
    
    wb_hol=Book(holded)
    ws_hol=wb_hol.sheets[0]
    last_row=ws_hol.range('A' + str(ws_hol.cells.last_cell.row)).end('up').row
    
    #Descubrir cuál es la fila de títulos
    fila_titulos=0
    content=""
    while content!="Fecha":
        fila_titulos=fila_titulos+1
        content=ws_hol.range("A"+str(fila_titulos)).value
    
    #Encontrar columna "Cliente
    column_cliente=0
    while content!="Cliente":
        column_cliente=column_cliente+1
        content=ws_hol.range(colnum_string(column_cliente)+str(fila_titulos)).value
    
    #Agregar dos columnas después de la columna cliente
    rango=colnum_string(column_cliente+1)+":"+colnum_string(column_cliente+2)
    ws_hol.range(rango).insert()
    ws_hol.range(colnum_string(column_cliente+1)+str(fila_titulos)).value="Mapeo cliente"
    ws_hol.range(colnum_string(column_cliente+2)+str(fila_titulos)).value="Mapeo currency"
    
    #Poner fórmulas de mapeo de cliente y currency a la primera fila de fórmulas
    ws_hol.range(colnum_string(column_cliente+1)+str(fila_titulos+1)).value="=INDEX('G:/.shortcut-targets-by-id/1lG8MCvQFKu6INsmzzLQAzaTRhwK0Patf/R6.uSizy/Tools/[Mapeo clientes Usizy.xlsx]Sheet1'!$C:$C,MATCH("+colnum_string(column_cliente)+str(fila_titulos+1)+",'G:/.shortcut-targets-by-id/1lG8MCvQFKu6INsmzzLQAzaTRhwK0Patf/R6.uSizy/Tools/[Mapeo clientes Usizy.xlsx]Sheet1'!$B:$B,0))"
    ws_hol.range(colnum_string(column_cliente+2)+str(fila_titulos+1)).value="=INDEX('G:/.shortcut-targets-by-id/1lG8MCvQFKu6INsmzzLQAzaTRhwK0Patf/R6.uSizy/Tools/[Mapeo clientes Usizy.xlsx]Sheet1'!$D:$D,MATCH("+colnum_string(column_cliente+1)+str(fila_titulos+1)+",'G:/.shortcut-targets-by-id/1lG8MCvQFKu6INsmzzLQAzaTRhwK0Patf/R6.uSizy/Tools/[Mapeo clientes Usizy.xlsx]Sheet1'!$C:$C,0))"
    
    #Copiar fórmulas hasta la última fila
    rango_copiar=colnum_string(column_cliente+1)+str(fila_titulos+1)+":"+colnum_string(column_cliente+2)+str(fila_titulos+1)
    rango_pegar=colnum_string(column_cliente+1)+str(fila_titulos+2)+":"+colnum_string(column_cliente+2)+str(last_row)
    ws_hol.range(rango_copiar).copy()
    ws_hol.range(rango_pegar).paste("formulas")
    
    #Buscar columna con subtotal
    col_subtotal=0
    while content!="Subtotal":
        col_subtotal=col_subtotal+1
        content=ws_hol.range(colnum_string(col_subtotal)+str(fila_titulos)).value
    #Lista con los clientes en la columna Mapeo cliente
    accounts_hol=ws_hol.range(colnum_string(column_cliente+1)+str(fila_titulos+1)+":"+colnum_string(column_cliente+1)+str(last_row)).value
    #Borrar los None de accounts_hol

    
    holded_edited=holded[0:len(holded)-5] + " edited.xlsx"
    wb_hol.save(holded_edited)
    
    
    #Ahora abrir reporting y pegar datos nuevos de la columna "Subtotal"
    wb_rep=Book(reporting)
    ws_rep=wb_rep.sheets[hoja_accounts]
    if ws_rep.api.AutoFilterMode ==True: ws_rep.api.AutoFilterMode = False
    
    #Encontrar primera columna con END y encontrar la columna del mes restando 13-mes
    col_end=0
    while content!="END":
        col_end=col_end+1
        content=ws_rep.range(colnum_string(col_end)+str(1)).value
    
    col_mes=col_end-(13-mes)
    
    #Encontrar columna account del reporting
    col_account=0
    while content!="Account":
        col_account=col_account+1
        content=ws_rep.range(colnum_string(col_account)+"1").value
        
    last_row_rep=ws_rep.range('A' + str(ws_rep.cells.last_cell.row)).end('up').row     
    accounts_rep=ws_rep.range(colnum_string(col_account)+"2:"+colnum_string(col_account)+str(last_row_rep-1)).value
    
    #Poner las cuentas del reporting y las cuentas de hoja holded todas en minúscula
    accounts_hol=[each_account.lower() for each_account in accounts_hol if each_account]
    accounts_rep=[each_account.lower() for each_account in accounts_rep]
    
    #Si hay cuentas diferentes agregar filas antes del end y poner las cuentas nuevas
    cuentas_diferentes=[]
    for cuenta in accounts_hol:
        if cuenta not in accounts_rep and cuenta!=None:
            cuentas_diferentes.append(cuenta)
    
    if len(cuentas_diferentes)>0:
        ws_rep.range(str(last_row_rep)+":"+str(last_row_rep+len(cuentas_diferentes)-1)).insert()
        i=0
        for cuenta in cuentas_diferentes:
            ws_rep.range(colnum_string(col_account)+str(last_row_rep+i)).value=cuenta
            ws_rep.range(colnum_string(col_account-1)+str(last_row_rep+i)).value=ws_hol.range(colnum_string(column_cliente+2)+str(fila_titulos+accounts_hol.index(cuenta)+1))
            i=i+1
        #Volver a extraeer lista de cuentas con las nuevas cuentas añadidas
        last_row_rep=ws_rep.range('A' + str(ws_rep.cells.last_cell.row)).end('up').row  
        accounts_rep=ws_rep.range(colnum_string(col_account)+"2:"+colnum_string(col_account)+str(last_row_rep-1)).value
        accounts_rep=[each_account.lower() for each_account in accounts_rep]
        
    #Pegar valores de Subtotal
    i=1
    for cuenta in accounts_rep:
        i=i+1
        #copiar
        try:
            fila=accounts_hol.index(cuenta)+fila_titulos+1
            ws_hol.range(colnum_string(col_subtotal)+str(fila)).copy()
            ws_rep.range(colnum_string(col_mes)+str(i)).paste()
        except:
            ws_rep.range(colnum_string(col_mes)+str(i)).value=0
    
    #copiar formato de columna anterior
    ws_rep.range(colnum_string(col_mes-1)+":"+colnum_string(col_mes-1)).copy()
    ws_rep.range(colnum_string(col_mes)+":"+colnum_string(col_mes)).paste("formats")
    
    wb_rep.save()
    wb_rep.close()
    wb_hol.close()
     
    app.kill()
