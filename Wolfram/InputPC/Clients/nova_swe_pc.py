# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 14:44:30 2021

@author: romina

Nova SWE
"""

import PyPDF2
import win32com.client as win32
from win32com.client import constants
from Wolfram.functions.dates_functions import ultimo_dia


def nova_swe_pc(nominas,reporting,hoja_reporting,anno,mes):
    pdfFileObj=open(nominas,"rb")
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    total=pdfReader.numPages
    
    empleados=[]
    salary=[]
    company=[]
    
    for i in range(0,total):
        pageObj1=pdfReader.getPage(i)
        texto=pageObj1.extractText().split("\n")
        index_bank=texto.index("Bankkonto:")
        empleados.append(texto[index_bank+2])
        
        index_arbeit=texto.index("Arbetsgivaravgift")
        company.append(float(texto[index_arbeit+1].replace(" ","").replace(",",".")))
        
        texto.pop(texto.index("Bruttolön"))
        index_brutto=texto.index("Bruttolön")
        salary.append(float(texto[index_brutto+1].replace(" ","").replace(",",".")))
        
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False
    excel.DisplayAlerts = False
    
    try:
        wb_report = excel.Workbooks.Open(reporting,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {reporting}')
    
    ws_report=wb_report.Sheets(hoja_reporting)
    
    ws_report.Range("A:ZZ").ClearOutline()
    ws_report.Range("1:500").ClearOutline()
    
    if ws_report.FilterMode: ws_report.AutoFilterMode = False
    
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    ws_report.Activate()
    ends=ws_report.Range("A:A").Find("END").Row
    
    ws_report.Range(str(ends)+":"+str(ends+total-1)).Insert()
    
    for i in range(0,total):
        ws_report.Range("C"+str(ends+i)).Value=empleados[i]
        ws_report.Range("D"+str(ends+i)).Value=salary[i]
        ws_report.Range("E"+str(ends+i)).Value=company[i]
        
    ws_report.Range("B"+str(ends)+":B"+str(ends+total-1)).Value="SWE"
    ws_report.Range("A"+str(ends)+":A"+str(ends+total-1)).Value=ultimo_dia(int(anno),int(mes))
    ws_report.Range("A"+str(ends)+":A"+str(ends+total-1)).TextToColumns(Destination=ws_report.Range("A"+str(ends)), DataType=constants.xlDelimited,TextQualifier=constants.xlDoubleQuote,ConsecutiveDelimiter=False,
                                                                                Tab=False,Semicolon=False, Comma=False, Space=False, Other=False,    TrailingMinusNumbers=True)
    ws_report.Range("F"+str(ends-1)+":J"+str(ends-1)).Copy(ws_report.Range("F"+str(ends)+":J"+str(ends+total-1)))
    
    wb_report.Save()
    wb_report.Close(True)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()  
    pdfFileObj.close()
    
