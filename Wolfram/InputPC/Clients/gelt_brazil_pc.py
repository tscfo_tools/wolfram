# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 15:34:47 2022

@author: Alberto Cañizares
"""
import os
import shutil
import pandas as pd
from xlwings import App,Book
# from tabula import read_pdf
from Wolfram.functions.Excel_functions import insert_empty_rows, get_last_row_and_column, drag_formula, find_col, month_in_InputPC, colnum_string
from Wolfram.functions.math import transpose_list


def extract_data_from_PC(df):
    
    # Extract data from PC
    employee_names, salary, INSS, FGTS, Intern = [], [], [], [], []
    all_df = df[0]
    for i in range(1, len(df)-1): # Last page is a summary
        df[i].columns = df[0].columns
        all_df = pd.concat([all_df, df[i]])
    all_df = all_df.reset_index(drop=True)   

    # Extract employees
    nome_rows = list(all_df[all_df.iloc[:,0].str.find('Nome: ')>0].index)
    nome_cells = list(all_df.iloc[nome_rows,0])
    for j in range(len(nome_cells)):
        employee_names.append(nome_cells[j][nome_cells[j].find('Nome:')+6:])

    # Salario -> 1 Salário
    salary_rows = list(all_df[(all_df.iloc[:,0].str.find('1 Salário')>=0) | (all_df.iloc[:,0].str.find('27 Bolsa Auxilio')>=0)].index) 
    salary_cells = list(all_df.iloc[salary_rows,1]) if len(salary_rows)>0 else []
    
    for j in range(len(salary_cells)):
        salary.append(salary_cells[j].split(' ')[1].replace('.', '').replace(',', '.'))
        
    # INSS -> 11 INSS Sobre Salário
    INSS_rows = list(all_df[(all_df.iloc[:,2].str.find('11 INSS Sobre Salário')>=0) | (all_df.iloc[:,0].str.find('27 Bolsa Auxilio')>=0)].index) 
    
    INSS_cells = []
    if len(INSS_rows)>0:
        for row_ in INSS_rows:
            if all_df.iloc[row_,0] == '27 Bolsa Auxilio':
                Intern.append(1)
                INSS_cells.append('0,0 0,0')
            else:
                Intern.append(0)
                INSS_cells.append(all_df.iloc[row_, -1])

    for j in range(len(INSS_cells)):
        INSS.append(INSS_cells[j].split(' ')[1].replace('.', '').replace(',', '.'))
        
    
    # FGTS ->  F.G.T.S.:
    FGTS_rows = list(all_df[all_df.iloc[:,2].str.find(' F.G.T.S.:')>=0].index) 
    FGTS_cells = list(all_df.iloc[FGTS_rows, -1]) if len(FGTS_rows)>0 else []
    
    for j in range(len(FGTS_cells)):
        FGTS.append(FGTS_cells[j].replace('.', '').replace(',', '.'))    
    
    return employee_names, salary, INSS, FGTS, Intern



         
###############################################################################    
    
def gelt_brazil_pc(PC_path, reporting_path, reporting_sheet, year, month):  
    
    # Load PC file
    PC_table = 0# read_pdf(PC_path, pages = 'all',encoding='cp1252')

    ## Extract data from PC table
    employees_name, employees_cost, INSS, FGTS, Intern = extract_data_from_PC(PC_table)

    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    
    report_filename = os.path.basename(reporting_path)
    report_folder_path = os.path.dirname(reporting_path)
    report_tmp_path = os.path.join(tmp_path, report_filename)

    shutil.copy(reporting_path, tmp_path)
    
    # Open Reporting Excel -> WorkSheet: InputPC
    app = App(visible=False)
    wb_inp = Book(report_tmp_path)
    ws_inp = wb_inp.sheets[reporting_sheet]

    # get last row and last column (wo/ taking into accound "END" if exists)
    last_row, last_col =  get_last_row_and_column(ws_inp)

    # Add new rows for Employees costs
    ## Add empty rows before "END"
    insert_empty_rows(ws_inp, last_row + 1, len(employees_name))
    ## define range to copy = (new_first_row, nel_last_row)
    new_first_row = last_row + 1
    new_last_row = last_row + len(employees_name)

    # Add new PC data
    ## Add Employee names in column "Employee"
    employee_col = find_col(ws_inp, 1, last_col, ["Employee"], default_col = "B")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_col + str(new_first_row) +":" + employee_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_name)
    
    ## Add Employee costs in column "Company cost" or "Net"
    employee_cost_col = find_col(ws_inp, 1, last_col, ["Salario"], default_col = "C")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_cost_col + str(new_first_row) +":" + employee_cost_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_cost)

    # Add dates to column "Month"
    month_in_InputPC(ws_inp, last_col, new_first_row, new_last_row, year, month, month_header = "Month")

    # Other  cols to add: INSS and FGTS
    gross_col = find_col(ws_inp, 1, last_col, ["INSS"])
    if gross_col != None:
        # Copy in ws
        ws_inp.range(gross_col + str(new_first_row) +":" + gross_col +  str(new_last_row)).options(index=False).value = transpose_list(INSS)

    ss_col = find_col(ws_inp, 1, last_col, ["FGTS"])
    if ss_col != None:
        # Copy in ws
        ws_inp.range(ss_col + str(new_first_row) +":" + ss_col +  str(new_last_row)).options(index=False).value = transpose_list(FGTS)
    
    # drag formulas and cell font yellow if #N/A
    for i in range(last_col):
        drag_formula(ws_inp, i, new_first_row-1, new_last_row, (255,255,0))

    # Intense Orange if intern
    i = 0
    for current_row in range(new_first_row, new_last_row):
        if Intern[i]: 
            ws_inp.range(employee_col + str(current_row)).color = (255, 192, 0) 
        i += 1

    # save and close
    wb_inp.save()
    wb_inp.close()
    
    app.kill()

    shutil.copy(report_tmp_path, reporting_path)
    os.remove(report_tmp_path)