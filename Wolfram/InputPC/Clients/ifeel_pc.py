import os
import shutil
from xlwings import App, Book
from Wolfram.functions.Excel_functions import *
from Wolfram.functions.math import transpose_list

###############################################################################    
    
def ifeel_pc(PC_path, reporting_path, reporting_sheet, year, month):
    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    
    PC_filename = os.path.basename(PC_path)
    PC_folder_path = os.path.dirname(PC_path)
    PC_tmp_path = os.path.join(tmp_path, PC_filename)

    shutil.copy(PC_path, tmp_path)
    
    # Open PC wb
    app = App(visible=False)
    wb_mayor = Book(PC_tmp_path)
    ws_mayor = wb_mayor.sheets[0]

    # Extracting header of PC table
    ## Header row of PC table
    header_row = ws_mayor.api.Range("A1:AA1000").Find("TRABAJADOR").Row
    ## Extracting columns for each variable of interest
    employee_col = ws_mayor.api.Range("A" + str(header_row) + ":DD" + str(header_row)).Find("TRABAJADOR").Column
    ## Company Cost = Salary + Social Security
    ### I.R.P.F. DINERARIA
    salary_col = ws_mayor.api.Range("A" + str(header_row) + ":DD" + str(header_row)).Find("I.R.P.F. DINERARIA").Column
    ### SOCIAL EMPRESA
    SS_col = ws_mayor.api.Range("A" + str(header_row) + ":DD" + str(header_row)).Find("SOCIAL EMPRESA").Column
    ### "Dto. Retribucion Flexible (RF)"
    RF_col = ws_mayor.api.Range("A" + str(header_row) + ":DD" + str(header_row)).Find("Dto. Retribucion Flexible (RF)").Column
    ### "Dcto.Conceptos en Especie"
    Dcto_col = ws_mayor.api.Range("A" + str(header_row) + ":DD" + str(header_row)).Find("Dcto.Conceptos en Especie").Column
    ## Last row of PC table -> Last row is the Total
    last_row = ws_mayor[header_row - 1, employee_col - 1].end('down').row - 1 

    # Extracting data from Excel
    ## Employees name
    employees_name = ws_mayor.range(
        colnum_string(employee_col) + str(header_row + 1) + ":" + colnum_string(employee_col) + str(last_row)
        ).value
    ##  I.R.P.F. DINERARIA
    salary = ws_mayor.range(
        colnum_string(salary_col) + str(header_row + 1) + ":" + colnum_string(salary_col) + str(last_row)
        ).value
    ## SOCIAL EMPRESA
    SS = ws_mayor.range(
        colnum_string(SS_col) + str(header_row + 1) + ":" + colnum_string(SS_col) + str(last_row)
        ).value
    ## "Dto. Retribucion Flexible (RF)"
    RF = ws_mayor.range(
        colnum_string(RF_col) + str(header_row + 1) + ":" + colnum_string(RF_col) + str(last_row)
        ).value
    ## "Dcto.Conceptos en Especie"
    Dcto = ws_mayor.range(
        colnum_string(Dcto_col) + str(header_row + 1) + ":" + colnum_string(Dcto_col) + str(last_row)
        ).value
    # Company Cost = I.R.P.F. DINERARIA + SOCIAL EMPRESA +  Dto. Retribucion Flexible (RF) + Dcto.Conceptos en Especie
    company_cost = [float(salary[i]) + float(SS[i]) + float(RF[i]) + float(Dcto[i]) for i in range(len(employees_name))]
    # Close PC wb
    wb_mayor.close()


    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    
    report_filename = os.path.basename(reporting_path)
    report_folder_path = os.path.dirname(reporting_path)
    report_tmp_path = os.path.join(tmp_path, report_filename)

    shutil.copy(reporting_path, tmp_path)
    
    # Open Reporting Excel -> WorkSheet: InputPC
    app = App(visible=False)
    wb_inp = Book(report_tmp_path)
    ws_inp = wb_inp.sheets[reporting_sheet]

    # get last row and last column (wo/ taking into accound "END" if exists)
    last_row, last_col =  get_last_row_and_column(ws_inp)

    # Add new rows for Employees costs
    ## Add empty rows before "END"
    insert_empty_rows(ws_inp, last_row + 1, len(employees_name))
    ## define range to copy = (new_first_row, nel_last_row)
    new_first_row = last_row + 1
    new_last_row = last_row + len(employees_name)

    # Add new PC data
    ## Add Employee names in column "Employee"
    employee_col = find_col(ws_inp, 1, last_col, ["Employee"], default_col = "B")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_col + str(new_first_row) +":" + employee_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_name)

    ## Add Company cost
    salary_col = find_col(ws_inp, 1, last_col, ["Company cost"], default_col = "C")
    ### Copy in ws "InputPC"
    ws_inp.range(salary_col + str(new_first_row) +":" + salary_col +  str(new_last_row)).options(index=False).value = transpose_list(company_cost)

    # Add dates to column "Month"
    month_in_InputPC(ws_inp, last_col, new_first_row, new_last_row, year, month, month_header = "Date")

    # drag formulas and cell font yellow if #N/A
    for i in range(last_col):
        drag_formula(ws_inp, i, new_first_row-1, new_last_row, (255,255,0))

    # save and close
    wb_inp.save()
    wb_inp.close()

    app.kill()
    shutil.copy(report_tmp_path, reporting_path)
    os.remove(report_tmp_path)
    os.remove(PC_tmp_path)