# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 17:26:30 2021

@author: romina

Build38 SGP
"""

import csv
import pandas as pd
from time import sleep
import win32com.client as win32
from win32com.client import constants
from Wolfram.functions.dates_functions import ultimo_dia


def build38_ger_pc(nomina,reporting,hoja_reporting,anno,mes):
    lst=[]
    with open(nomina,"r") as f:
        csv_reader=csv.reader(f,delimiter=";")
        line_count=0
        for row in csv_reader:
            if line_count==12:
                titulos=row
            if line_count>12:
                lst.append(row)
            line_count=line_count+1
        
    df=pd.DataFrame(lst,columns=titulos)
    df=df[df["Buchungstext"].str.contains("Verbindl.")]
    df.Umsatz=df["Umsatz"].apply(lambda x: x.replace(".","").replace(",",".") if  ("." in x and "," in x) else x.replace(",","."))
    df.Umsatz=pd.to_numeric(df["Umsatz"])
    
    total=len(df.Name.unique())
    
    empleados=[]
    costes=[]
    
    for i in range(0,total):
        empleados.append(df.Name.unique()[i])
        df2=df[df.Name==df.Name.unique()[i]]
        costes.append(sum(df2.Umsatz))
        
    
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False
    excel.DisplayAlerts = False
    
    try:
        wb_report = excel.Workbooks.Open(reporting,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {reporting}')
    try:
        ws_report=wb_report.Sheets(hoja_reporting)
    except:
        sleep(4.0)
        ws_report=wb_report.Sheets(hoja_reporting)
        
    ws_report.Range("A:ZZ").ClearOutline()
    ws_report.Range("1:500").ClearOutline()
    
    try:
        ws_report.Columns.EntireColumn.Hidden=False
        ws_report.EntireRow.Hidden=False
    except:
        pass
    
    ws_report.Activate()
    ends=ws_report.Range("A:A").Find("END").Row
    
    ws_report.Range(str(ends)+":"+str(ends+total-1)).EntireRow.Insert()
    
    for i in range(0,total):
        ws_report.Range("B"+str(ends+i)).Value=empleados[i]
        ws_report.Range("C"+str(ends+i)).Value=costes[i]
        
    #Fecha columna A
    ws_report.Range("A"+str(ends)+":A"+str(ends+total-1)).Value=ultimo_dia(int(anno), int(mes))
    ws_report.Range("A"+str(ends)+":A"+str(ends+total-1)).TextToColumns(Destination=ws_report.Range("A"+str(ends)), DataType=constants.xlDelimited,TextQualifier=constants.xlDoubleQuote,ConsecutiveDelimiter=False,
                                                                            Tab=False,Semicolon=False, Comma=False, Space=False, Other=False,    TrailingMinusNumbers=True)
    #Copiar fórmulas de la D a la H
    ws_report.Range("D" + str(ends-1)+":H"+str(ends-1)).Copy()
    ws_report.Range("D" + str(ends) +":H" +str(ends+total-1)).PasteSpecial(-4123)
    
    wb_report.Save()
    wb_report.Close(True)
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit() 
    