# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 17:31:09 2021

@author: romina
"""


import win32com.client as win32, constants
import pandas as pd
from Wolfram.functions.dates_functions import ultimo_dia


def pdf_to_excel(nominas):
    nuevo_nombre=nominas.replace(".PDF",".xlsx").replace(".pdf",".xlsx").replace("/","\\")
    
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False
    excel.DisplayAlerts = False
    
    try:
        wb_nomina = excel.Workbooks.Add()
    except:
        print('Failed to open spreadsheet.')
        
    ws=wb_nomina.ActiveSheet.Name
    ws=wb_nomina.Sheets(ws)
    
    wb_nomina.Queries.Add(Name="Table001", Formula=
                               "let" + chr(13) + "" + chr(10) + "    Source = Pdf.Tables(File.Contents(\""+nominas+"\"), [Implementation=\"1.3\"])," + chr(13) + "" + chr(10) + "    Page1 = Source{[Id=\"Table001\"]}[Data]," + chr(13) + "" + chr(10) + "    #\"Promoted Headers\" = Table.PromoteHeaders(Page1, [PromoteAllScalars=true])," + chr(13) + "" + chr(10) + "    #\"Changed Type\" = Table.TransformColumnTypes(#\"Promoted Headers\",{})" + chr(13) + "" + chr(10) + "in" + chr(13) + "" + chr(10) + "    #\"Changed Type\"")
    
    
    x=ws.ListObjects.Add(SourceType=0,Source="OLEDB;Provider=Microsoft.Mashup.OleDb.1;Data Source=$Workbook$;Location=Table001;Extended Properties=\"\"",
                            Destination=ws.Range("$A$1")).QueryTable
    x.CommandType = constants.xlCmdSql
    x.CommandText = ("SELECT * FROM [Table001]")
    x.RowNumbers = False
    x.FillAdjacentFormulas = False
    x.PreserveFormatting = True
    x.RefreshOnFileOpen = False
    x.BackgroundQuery = True
    x.RefreshStyle = constants.xlInsertDeleteCells
    x.SavePassword = False
    x.AdjustColumnWidth = True
    x.SaveData = True
    x.RefreshPeriod = 0
    x.PreserveColumnInfo = True
    x.ListObject.DisplayName = "Table001"
    x.BackgroundQuery=False
    x.Refresh()==True
    
    
    wb_nomina.SaveAs(nuevo_nombre)
    wb_nomina.Close()
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()  
    return nuevo_nombre
