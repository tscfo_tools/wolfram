# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 15:34:47 2022

@author: Alberto Cañizares
"""
import os
import shutil
from xlwings import App,Book
from Wolfram.functions.Excel_functions import *
from Wolfram.functions.math import transpose_list


         
###############################################################################    
    
def graphext_pc(PC_path, reporting_path, reporting_sheet, year, month):  
    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    
    PC_filename = os.path.basename(PC_path)
    PC_folder_path = os.path.dirname(PC_path)
    PC_tmp_path = os.path.join(tmp_path, PC_filename)

    shutil.copy(PC_path, tmp_path)
    
    # Open PC wb
    app = App(visible=False)
    wb_PC = Book(PC_tmp_path)
    ws_PC = wb_PC.sheets[0]

    # Extracting header of PC table
    ## Header row of PC table
    header_row = ws_PC.api.Range("A1:AA1000").Find("Date").Row
    ## Extracting columns for each variable of interest
    date_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Date").Column
    employee_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Employee").Column
    Description_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Description").Column
    Account_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Account").Column
    Tags_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Tags").Column
    Salary_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Salary").Column
    TotalSS_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Total social security").Column
    for i in range(1, 1000):
        if ws_PC.api.Range(colnum_string(i) + str(header_row)).Value == "Social Security":
            SS_col = i
            break
    Retention_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Retention").Column
    Extra_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Extra").Column
    ToPay_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("To pay").Column
    Paid_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Paid").Column
    Pending_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Pending").Column
    Status_col = ws_PC.api.Range("A" + str(header_row) + ":AA" + str(header_row)).Find("Status").Column
    ## Last row of PC table
    last_row = ws_PC[header_row - 1, employee_col - 1].end('down').row

    # Extracting data from Excel
    ## Employees name
    employees_name = ws_PC.range(
        colnum_string(employee_col) + str(header_row+1) + ":" + colnum_string(employee_col) + str(last_row)
        ).value
    ## Company cost = IRPF + SS + Dietas
    date = ws_PC.range(
        colnum_string(date_col) + str(header_row+1) + ":" + colnum_string(date_col) + str(last_row)
        ).value
    Description = ws_PC.range(
        colnum_string(Description_col) + str(header_row+1) + ":" + colnum_string(Description_col) + str(last_row)
        ).value
    Account = ws_PC.range(
        colnum_string(Account_col) + str(header_row+1) + ":" + colnum_string(Account_col) + str(last_row)
        ).value
    Tags = ws_PC.range(
        colnum_string(Tags_col) + str(header_row+1) + ":" + colnum_string(Tags_col) + str(last_row)
        ).value
    Salary = ws_PC.range(
        colnum_string(Salary_col) + str(header_row+1) + ":" + colnum_string(Salary_col) + str(last_row)
        ).value
    TotalSS = ws_PC.range(
        colnum_string(TotalSS_col) + str(header_row+1) + ":" + colnum_string(TotalSS_col) + str(last_row)
        ).value
    SS = ws_PC.range(
        colnum_string(SS_col) + str(header_row+1) + ":" + colnum_string(SS_col) + str(last_row)
        ).value
    Retention = ws_PC.range(
        colnum_string(Retention_col) + str(header_row+1) + ":" + colnum_string(Retention_col) + str(last_row)
        ).value
    Extra = ws_PC.range(
        colnum_string(Extra_col) + str(header_row+1) + ":" + colnum_string(Extra_col) + str(last_row)
        ).value
    ToPay = ws_PC.range(
        colnum_string(ToPay_col) + str(header_row+1) + ":" + colnum_string(ToPay_col) + str(last_row)
        ).value
    Paid = ws_PC.range(
        colnum_string(Paid_col) + str(header_row+1) + ":" + colnum_string(Paid_col) + str(last_row)
        ).value
    Pending = ws_PC.range(
        colnum_string(Pending_col) + str(header_row+1) + ":" + colnum_string(Pending_col) + str(last_row)
        ).value
    Status = ws_PC.range(
        colnum_string(Status_col) + str(header_row+1) + ":" + colnum_string(Status_col) + str(last_row)
        ).value
    # Close PC wb
    wb_PC.close()

    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    
    report_filename = os.path.basename(reporting_path)
    report_folder_path = os.path.dirname(reporting_path)
    report_tmp_path = os.path.join(tmp_path, report_filename)

    shutil.copy(reporting_path, tmp_path)
    
    # Open Reporting Excel -> WorkSheet: InputPC
    app = App(visible=False)
    wb_inp = Book(report_tmp_path)
    ws_inp = wb_inp.sheets[reporting_sheet]

    # get last row and last column (wo/ taking into accound "END" if exists)
    last_row, last_col =  get_last_row_and_column(ws_inp)

    # Add new rows for Employees costs
    ## Add empty rows before "END"
    insert_empty_rows(ws_inp, last_row + 1, len(employees_name))
    ## define range to copy = (new_first_row, nel_last_row)
    new_first_row = last_row + 1
    new_last_row = last_row + len(employees_name)

    # Add new PC data
    ## Add Employee names in column "Employee"
    employee_col = find_col(ws_inp, 1, last_col, ["Employee"], default_col = "C")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_col + str(new_first_row) +":" + employee_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_name)

    ## "Date"
    date_col = find_col(ws_inp, 1, last_col, ["Date"])
    ### Copy in ws "InputPC"
    ws_inp.range(date_col + str(new_first_row) +":" + date_col +  str(new_last_row)).options(index=False).value = transpose_list(date)
    
    ## "Description"
    Description_col = find_col(ws_inp, 1, last_col, ["Description"])
    ### Copy in ws "InputPC"
    ws_inp.range(Description_col + str(new_first_row) +":" + Description_col +  str(new_last_row)).options(index=False).value = transpose_list(Description)

    ## "Account"
    Account_col = find_col(ws_inp, 1, last_col, ["Account"])
    ### Copy in ws "InputPC"
    ws_inp.range(Account_col + str(new_first_row) +":" + Account_col +  str(new_last_row)).options(index=False).value = transpose_list(Account)

    ## "Tags"
    Tags_col = find_col(ws_inp, 1, last_col, ["Tags"])
    ### Copy in ws "InputPC"
    ws_inp.range(Tags_col + str(new_first_row) +":" + Tags_col +  str(new_last_row)).options(index=False).value = transpose_list(Tags)

    ## "Salary"
    Salary_col = find_col(ws_inp, 1, last_col, ["Salary"])
    ### Copy in ws "InputPC"
    ws_inp.range(Salary_col + str(new_first_row) +":" + Salary_col +  str(new_last_row)).options(index=False).value = transpose_list(Salary)

    ## "TotalSS"
    TotalSS_col = find_col(ws_inp, 1, last_col, ["Total social security"])
    ### Copy in ws "InputPC"
    ws_inp.range(TotalSS_col + str(new_first_row) +":" + TotalSS_col +  str(new_last_row)).options(index=False).value = transpose_list(TotalSS)

    ## "Social Security"
    SS_col = find_col(ws_inp, 1, last_col, ["Social Security"])
    ### Copy in ws "InputPC"
    ws_inp.range(SS_col + str(new_first_row) +":" + SS_col +  str(new_last_row)).options(index=False).value = transpose_list(SS)

    ## "Retention"
    Retention_col = find_col(ws_inp, 1, last_col, ["Retention"])
    ### Copy in ws "InputPC"
    ws_inp.range(Retention_col + str(new_first_row) +":" + Retention_col +  str(new_last_row)).options(index=False).value = transpose_list(Retention)

    ## "Extra"
    Extra_col = find_col(ws_inp, 1, last_col, ["Extra"])
    ### Copy in ws "InputPC"
    ws_inp.range(Extra_col + str(new_first_row) +":" + Extra_col +  str(new_last_row)).options(index=False).value = transpose_list(Extra)

    ## "To pay"
    ToPay_col = find_col(ws_inp, 1, last_col, ["To pay"])
    ### Copy in ws "InputPC"
    ws_inp.range(ToPay_col + str(new_first_row) +":" + ToPay_col +  str(new_last_row)).options(index=False).value = transpose_list(ToPay)

    ## "Paid"
    Paid_col = find_col(ws_inp, 1, last_col, ["Paid"])
    ### Copy in ws "InputPC"
    ws_inp.range(Paid_col + str(new_first_row) +":" + Paid_col +  str(new_last_row)).options(index=False).value = transpose_list(Paid)

    ## "Pending"
    Pending_col = find_col(ws_inp, 1, last_col, ["Pending"])
    ### Copy in ws "InputPC"
    ws_inp.range(Pending_col + str(new_first_row) +":" + Pending_col +  str(new_last_row)).options(index=False).value = transpose_list(Pending)

    ## "Status"
    Status_col = find_col(ws_inp, 1, last_col, ["Status"])
    ### Copy in ws "InputPC"
    ws_inp.range(Status_col + str(new_first_row) +":" + Status_col +  str(new_last_row)).options(index=False).value = transpose_list(Status)


    # Add dates to column "Month"
    month_in_InputPC(ws_inp, last_col, new_first_row, new_last_row, year, month, month_header = "Month")

    # drag formulas and cell font yellow if #N/A
    for i in range(last_col):
        drag_formula(ws_inp, i, new_first_row-1, new_last_row, (255,255,0))

    # save and close
    wb_inp.save()
    wb_inp.close()
    
    app.kill()

    shutil.copy(report_tmp_path, reporting_path)
    os.remove(report_tmp_path)
    os.remove(PC_tmp_path)