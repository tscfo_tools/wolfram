# -*- coding: utf-8 -*-
"""
Created on Thu Oct  7 13:16:46 2021

@author: romina

Dinero Gelt 
"""
import os
import shutil
from xlwings import App,Book
import pandas as pd
from Wolfram.functions.dates_functions import ultimo_dia
import datetime as dt
from numpy import where

def gelt_tech_pc(nomina,reporting,hoja_reporting,anno,mes):
    #importar tabla de nominas como pandas dataframe para saber cuantas filas de empleados hay

    nomina_df=pd.read_excel(nomina,header=None)
    filas_nombre=list(nomina_df[nomina_df[0]=="NOMBRE TRABAJADOR"].axes[0])
    last_col=len(nomina_df.columns)
    nombres=nomina_df.iloc[filas_nombre[0]][1:last_col]
    filas_nombre.pop(0)
    for linea in filas_nombre:
        nombres=nombres.append(nomina_df.iloc[linea][1:last_col])
        
    nombres.dropna(inplace=True)
    nombres=nombres[0:len(nombres)-1]
 
    
    filas_nombre=list(nomina_df[nomina_df[0]=="NOMBRE TRABAJADOR"].axes[0])
    filas_coste=list(nomina_df[nomina_df[0]=="COSTE TOTAL EMPRESA"].axes[0])
    costes=[]
    nombres2=list(nombres)
    for nombre in nombres2:
        i, j = where(nomina_df == nombre)
        i=filas_coste[filas_nombre.index(int(i[0]))]
        costes.append(nomina_df[j[0]].iloc[int(i)])
        
    
    
    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    
    report_filename = os.path.basename(reporting_path)
    report_folder_path = os.path.dirname(reporting_path)
    report_tmp_path = os.path.join(tmp_path, report_filename)

    shutil.copy(reporting_path, tmp_path)
    
    # Open Reporting Excel -> WorkSheet: InputPC
    app = App(visible=False)
    wb_inp = Book(report_tmp_path)
    ws_inp = wb_inp.sheets[reporting_sheet]
    
    used_range_rows = (ws_inp.api.UsedRange.Row,	ws_inp.api.UsedRange.Row + ws_inp.api.UsedRange.Rows.Count)[1]-1
    #Añadir filas antes del END
    rango=str(used_range_rows) +":" + str(used_range_rows + len(nombres)-1)
    ws_inp.range(rango).insert()
    
    
    #Añadir nombres
    rango_nombres="B" + str(used_range_rows) +":B" +  str(used_range_rows + len(nombres)-1)
    ws_inp.range(rango_nombres).options(index=False).value=nombres

    #Añadir costos
    
    for pos in range(0,len(costes)):
        rango_nombres="B" + str(used_range_rows+pos)
        ws_inp.range(rango_nombres).options(index=False).value=nombres2[pos]
        rango_costo="C" + str(used_range_rows+pos)
        ws_inp.range(rango_costo).options(index=False).value=costes[pos]

    #Añadir fechas
    rango_fecha="A" + str(used_range_rows) +":A" +  str(used_range_rows + len(nombres)-1)
    ws_inp.range(rango_fecha).value=(dt.datetime.strptime(ultimo_dia(int(anno),int(mes)),"%d/%m/%Y")-dt.datetime(1900,1,1)).days+2
    
    #Copiar fórmulas
    rango_antes="D" + str(used_range_rows-1) +":F" +  str(used_range_rows -1)
    rango_formulas="D" + str(used_range_rows) +":F" +  str(used_range_rows + len(nombres)-1)
    ws_inp.range(rango_antes).copy()
    ws_inp.range(rango_formulas).paste("formulas")


    #Copiar formato fecha
    rango_antes="A" + str(used_range_rows-1) +":A" +  str(used_range_rows -1)
    ws_inp.range(rango_antes).copy()
    ws_inp.range(rango_fecha).paste("formats")


    wb_inp.save()
    wb_inp.close()
     
    app.kill()
    shutil.copy(report_tmp_path, reporting_path)
    os.remove(report_tmp_path)