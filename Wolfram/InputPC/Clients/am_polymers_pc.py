
import os
import shutil
from xlwings import App,Book
import pandas as pd
from Wolfram.functions.Excel_functions import insert_empty_rows, get_last_row_and_column, drag_formula, find_col, month_in_InputPC
from Wolfram.functions.math import transpose_list

  
def am_polymers_pc(PC_path, reporting_path, reporting_sheet, year, month):  
    # Load PC file
    if PC_path[-3:] == 'csv':
        PC_csv = open(PC_path, 'r')
        PC_csv_text = PC_csv.readlines()

        for i in range(len(PC_csv_text)):
            if 'Steuer-Brutto' in PC_csv_text[i]:
                header_row = i
                first_row = i + 1
            if '; ; ; ; ; ; ; ; ; ; ; ; ; ; ' in PC_csv_text[i][:29]:
                last_row = i - 1
                break

        PC_tab_header = PC_csv_text[header_row].split(';')
        PC_tab = [PC_csv_text[i].split(';') for i in range(first_row, last_row+1)]

        # FInd right columns
        name_col = PC_tab_header.index("Name ") 
        Salary_col = PC_tab_header.index("Steuer-Brutto ")
        SS_cols = [PC_tab_header.index("KV-Beitrag AN "), PC_tab_header.index("RV-Beitrag AN "), PC_tab_header.index("AV-Beitrag AN "), PC_tab_header.index("PV-Beitrag AN "), \
                    PC_tab_header.index("Umlage 1 "), PC_tab_header.index("Umlage 2 "), PC_tab_header.index("Umlage Insolv. ")]

        employees_name = [PC_tab[i][name_col] for i in range(len(PC_tab))]
        employees_cost, SS_cost, ss = [], [], []
        for i in range(len(PC_tab)):
            try: 
                employees_cost.append(float(PC_tab[i][Salary_col].replace('Z','').replace('.','').replace(',','.')))
            except:
                employees_cost.append(PC_tab[i][Salary_col].strip())
                
            SS_cost_per_emp = []
            for j in range(len(SS_cols)):   
                try: 
                    SS_cost_per_emp.append(float(PC_tab[i][SS_cols[j]].replace('Z','').replace('.','').replace(',','.')))
                except:
                    SS_cost_per_emp.append(PC_tab[i][SS_cols[j]])
            SS_cost.append(SS_cost_per_emp)
            try:
                ss.append(float('%.2f' % sum(SS_cost_per_emp)))
            except:
                ss.append('')
    else:
        PC_tab = pd.read_excel(PC_path)
        header_row = [i for i in range(len(PC_tab)) if ((PC_tab.iloc[i,1] == "Name ") or (PC_tab.iloc[i,1] == "Name"))][0]
        end_row = [i for i in range(header_row, len(PC_tab)) if ((PC_tab.iloc[i,1] == "") or (PC_tab.iloc[i,1] == " ") or (PC_tab.iloc[i,1] == None))][0]

        header = list(PC_tab.iloc[header_row])
        PC_tab_data = PC_tab.iloc[header_row + 1: end_row].reset_index(drop=True)
        # employee name
        employees_name = list(PC_tab_data.iloc[:, header.index("Name ")])

        employees_cost = list(PC_tab_data.iloc[:, header.index("Steuer-Brutto ")])
        employees_cost  = [0 if type(employees_cost[i]) == str else employees_cost[i] for i in range(len(employees_cost))]

        ss_cols = [header.index("KV-Beitrag AN "), header.index("RV-Beitrag AN "), header.index("AV-Beitrag AN "), header.index("PV-Beitrag AN "), \
                            header.index("Umlage 1 "), header.index("Umlage 2 "), header.index("Umlage Insolv. ")]

        ss = []
        for i in range(len(PC_tab_data)):
            ss_per_employee = 0
            for j in ss_cols:
                ss_per_employee = ss_per_employee + (0 if str(PC_tab_data.iloc[i, j]).strip() == "" else float(str(PC_tab_data.iloc[i, j]).replace('Z','').replace(',','.').strip()))
                
            ss.append(ss_per_employee)


    # move reporting to local because xlwings do not find excel in shared folders
    tmp_path= 'c:/tmp/'
    report_filename = os.path.basename(reporting_path)
    report_folder_path = os.path.dirname(reporting_path)
    report_tmp_path = os.path.join(tmp_path, report_filename)

    if not os.path.isdir(tmp_path): os.mkdir(tmp_path)
    shutil.copy(reporting_path, tmp_path)
    
    # Open Reporting Excel -> WorkSheet: InputPC
    app = App(visible=False)
    wb_inp = Book(report_tmp_path)
    ws_inp = wb_inp.sheets[reporting_sheet]

    # get last row and last column (wo/ taking into accound "END" if exists)
    last_row, last_col =  get_last_row_and_column(ws_inp)

    # Add new rows for Employees costs
    ## Add empty rows before "END"
    insert_empty_rows(ws_inp, last_row + 1, len(employees_name))
    ## define range to copy = (new_first_row, nel_last_row)
    new_first_row = last_row + 1
    new_last_row = last_row + len(employees_name)

    # Add new PC data
    ## Add Employee names in column "Employee"
    employee_col = find_col(ws_inp, 1, last_col, ["Employee"], default_col = "A")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_col + str(new_first_row) +":" + employee_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_name)

    ## Add Employee costs in column "Company cost" or "Net"
    employee_cost_col = find_col(ws_inp, 1, last_col, ["Gross salary"], default_col = "C")
    ### Copy in ws "InputPC"
    ws_inp.range(employee_cost_col + str(new_first_row) +":" + employee_cost_col +  str(new_last_row)).options(index=False).value = transpose_list(employees_cost)

    # Add dates to column "Month"
    month_in_InputPC(ws_inp, last_col, new_first_row, new_last_row, year, month, month_header = "Month")

    ss_col = find_col(ws_inp, 1, last_col, ["SS"])
    ws_inp.range(ss_col + str(new_first_row) +":" + ss_col +  str(new_last_row)).options(index=False).value = transpose_list(ss)
      

    # drag formulas and cell font yellow if #N/A
    for i in range(last_col):
        drag_formula(ws_inp, i, new_first_row-1, new_last_row, (255,255,0))

    # save and close
    wb_inp.save()
    wb_inp.close()
    
    app.kill()

    shutil.copy(report_tmp_path, reporting_path)
    os.remove(report_tmp_path)
