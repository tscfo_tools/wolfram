
# Functions that interacts with Excel
from xlwings import App,Book
from datetime import datetime
from Wolfram.functions.dates_functions import last_day_of_month_int

def colnum_string(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string 


def insert_empty_rows(ws, row_to_insert_above, n_rows):
    ws.range(str(row_to_insert_above) + ":" + str((row_to_insert_above + n_rows) - 1)).insert()


def get_last_row_and_column(ws1):
    # If last row or col is an "END", 
    # last row/column is considered the previous one
    # try:
    end_value = ''
    ultima_fila = 1
    while end_value != 'END' :
        if ultima_fila != 1: ultima_fila += 2
        ultima_fila= ws1.api.Range("A" + str(ultima_fila) + ":A200000").Find("END").Row - 1
        end_value = ws1.range("A" + str(ultima_fila + 1)).value # For check exact match

    end_value = ''
    ultima_col = 1
    while end_value != 'END':
        if ultima_col != 1: ultima_col += 2
        ultima_col = ws1.api.Range(colnum_string(ultima_col + 1) + "1:AA1").Find("END").Column - 1
        end_value = ws1.range(colnum_string(ultima_col + 1) + "1").value
    
    return ultima_fila, ultima_col


def drag_formula(ws, col_to_drag, row_to_drag, new_last_row, error_font_color = ()):
    # drag formulas if exists
    """
    If cell(row_to_drag, col_to_drag) in worksheet "ws_ino" have a formula, it is dragged until row "new_last_row".
    error_font_color = cell font color if formula result os #N/A
    """
    if len(ws[colnum_string(col_to_drag+1) + str(row_to_drag)].formula) > 0:
        if ws[colnum_string(col_to_drag+1) + str(row_to_drag)].formula[0] == "=":
           ws.range(colnum_string(col_to_drag+1) + str(row_to_drag) +":" + colnum_string(col_to_drag+1) +  str(row_to_drag)).copy()
           ws.range(colnum_string(col_to_drag+1) + str(row_to_drag + 1) +":" + colnum_string(col_to_drag+1) +  str(new_last_row)).paste("formulas")
           # Apply cell font color if  #N/A
           if len(error_font_color) > 0:
               for n_row in range(row_to_drag, new_last_row):
                   if ws.range(colnum_string(col_to_drag+1) + str(n_row+1)).value == None:
                       ws.range(colnum_string(col_to_drag+1) + str(n_row+1)).color = error_font_color 
                   else: pass
           else: pass
    else: pass
    
    return

def find_col(ws, header_row, last_col, col_name, default_col = None):
    # Find column in a table by their exact header name (=col_name) between cols 1 and last_col
    col = None
    for i in range(last_col):
        for j in range(len(col_name)):
            if ws.range((header_row,i+1)).value == col_name[j]: 
                col = colnum_string(i+1)
                break
        # This is written for break when first conicidence is found
        if col != None: break 
    if col == None: col = default_col
    
    return col

def month_in_InputPC(ws, last_col, new_first_row, new_last_row, year, month, month_header = "Month"):
    
    date_col = find_col(ws, 1, last_col, [month_header], default_col = "A")
    # Copy in ws "InputPC"
    month_date_int = last_day_of_month_int(datetime(int(year), int(month), 1))
    ws.range(date_col + str(new_first_row) +":" + date_col +  str(new_last_row)).options(index=False).value = month_date_int
    # Copy date format
    ws.range(date_col + str(new_first_row-1) +":" + date_col +  str(new_first_row-1)).copy()
    ws.range(date_col + str(new_first_row) +":" + date_col + str(new_last_row)).paste("formats")
    
    return