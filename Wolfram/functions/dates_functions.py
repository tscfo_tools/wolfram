# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 13:03:34 2021

@author: romina
"""
from datetime import date, timedelta, datetime

def colnum_string(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string  

def ultimo_dia(anno,mes):
    if mes == 12: anno += 1
    ultimo=date(anno,(mes+1)%12,1)-timedelta(days=1)
    ultimo=ultimo.strftime("%d/%m/%Y")
    return ultimo

def last_day_of_month_int(any_day):
    # this will never fail
    # get close to the end of the month for any day, and add 4 days 'over'
    next_month = any_day.replace(day=28) + timedelta(days=4)
    # subtract the number of remaining 'overage' days to get last day of current month, or said programattically said, the previous day of the first of next month
    current_date = next_month - timedelta(days=next_month.day)
    return (current_date - datetime(1900,1,1)).days + 2
