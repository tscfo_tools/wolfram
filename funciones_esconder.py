# -*- coding: utf-8 -*-
"""
Created on Wed Jul 21 10:49:32 2021

@author: romina

mejoras: en lugar de hoja, lista_hojas
"""
import win32com.client as win32
from time import sleep

def convert_date(fecha):
    try:
        mes=int(fecha.strftime('%m'))
        anno=int(fecha.strftime('%Y'))
        resultado=str(mes)+"/"+str(anno)
    except:
        resultado=fecha
    return resultado

def buscar_rango(col1,col2):
    rango1=colnum_string(col1)+":"+colnum_string(col2)
    return rango1

def colnum_string(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string  


def short_date(month,year):
    """

    Parameters
    ----------
    month : int
    year : int

    Returns
    -------
    short_date: str

    """        
    
    months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    year=str(year)[2:4]
    
    short_date=months[int(month)-1]+"-"+year
    
    return short_date    
    
        
def esconder_meses(mes,anno,file,lista_hojas):
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    # excel can be visible or not
    excel.Visible = False
    excel.DisplayAlerts = False
    
    try:
        wb = excel.Workbooks.Open(file,False,None)
    except:
        print(f'Failed to open spreadsheet.  Invalid filename or location: {file}')
       
    
    
    for hoja in lista_hojas:
        try:
            ws=wb.Sheets(hoja)
        except:
            sleep(1.0)
            ws=wb.Sheets(hoja)
            
        ws.Columns.EntireColumn.Hidden=False
        ws.Range("A:ZZ").ClearOutline()
        
        ws.Activate()
        ws.Columns.AutoFit()

        #Encontrar fila de títulos
        # fila_titulos=ws.UsedRange.Find("ACTUALS").Row
        fila_titulos = ws.UsedRange.Find("ACTUALS",SearchDirection=win32.constants.xlNext,SearchOrder=win32.constants.xlByRows).Row
        #Contar columnas
        ncols=ws.UsedRange.Find("*",SearchDirection=win32.constants.xlPrevious,SearchOrder=win32.constants.xlByColumns).Column
        #Encontrar columna 1 de títulos=ACTUALS
        actuals1=ws.UsedRange.Find("ACTUALS",SearchDirection=win32.constants.xlNext,SearchOrder=win32.constants.xlByRows,LookAt=win32.constants.xlWhole).Column
        #Encontrar columna en Blanco que separa a los YTD
        blankcol=ws.Cells(fila_titulos,actuals1).End(win32.constants.xlToRight).Column+1
        
        # Primera columna YTD y ultima de variable de meses
        variable_names_column = ws.UsedRange.Find("REPORTING", SearchDirection=win32.constants.xlNext, SearchOrder=win32.constants.xlByColumns, LookIn=win32.constants.xlValues, LookAt=win32.constants.xlPart).Column
        YTD_last_column = ws.Range(ws.Cells(fila_titulos + 1, 1), ws.Cells(fila_titulos + 1, 1000)).Find("YTD", SearchDirection=win32.constants.xlPrevious, SearchOrder=win32.constants.xlByColumns, LookAt=win32.constants.xlWhole).Column
        YTD_first_column = ws.Range(ws.Cells(fila_titulos + 1, 1), ws.Cells(fila_titulos + 1, 1000)).Find("YTD", SearchDirection=win32.constants.xlNext, SearchOrder=win32.constants.xlByColumns, LookAt=win32.constants.xlWhole).Column
        month_last_column = ws.Range(ws.Cells(fila_titulos + 1, variable_names_column), ws.Cells(fila_titulos + 1, YTD_first_column - 1)).Find("*", SearchDirection=win32.constants.xlPrevious, SearchOrder=win32.constants.xlByColumns, LookAt=win32.constants.xlWhole).Column
        
        #ACTUALS
        #Buscar primer y último actuals
        
        actuals_last=ws.Range(ws.Cells(fila_titulos,actuals1),ws.Cells(fila_titulos,blankcol)).Find("ACTUALS",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole).Column
        #Entre primer y último actuals buscar mes
        fecha=short_date(mes,anno)
        try:
            actuals_mes=ws.Range(ws.Cells(fila_titulos+1,actuals1),ws.Cells(fila_titulos+1,actuals_last)).Find(fecha,LookIn=win32.constants.xlValues).Column
        except:
            actuals_mes=ws.Range(ws.Cells(fila_titulos+1,actuals1),ws.Cells(fila_titulos+1,actuals_last)).Find(fecha.replace("-","/"),LookIn=win32.constants.xlValues).Column
        #AGRUPAR:
        if mes > 3:
            rango=buscar_rango(actuals1,actuals_mes - 3)
            ws.Range(rango).Group()
        else:
            actuals_ini_year = ws.UsedRange.Find("Jan-" + anno[-2:],LookAt=win32.constants.xlWhole).Column
            if actuals1!=actuals_ini_year:
                rango = buscar_rango(actuals1, actuals_ini_year-1)
                ws.Range(rango).Group()

        if actuals_mes!=actuals_last:
            rango=buscar_rango(actuals_mes+1, actuals_last)
            ws.Range(rango).Group()
        
        
        
        try:
            #BP
            #buscar primer y último BP
            bp1=ws.UsedRange.Find("BP",LookAt=win32.constants.xlWhole,SearchDirection=win32.constants.xlNext,SearchOrder=win32.constants.xlByRows).Column
            bp_last=ws.Range(ws.Cells(fila_titulos,bp1),ws.Cells(fila_titulos,blankcol)).Find("BP",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            if bp1 == bp_last:
                new_blankcol=ws.Cells(fila_titulos,bp1).End(win32.constants.xlToRight).Column+1
                bp_last=ws.Range(ws.Cells(fila_titulos,bp1),ws.Cells(fila_titulos,new_blankcol)).Find("BP",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            #Entre primer y último BP, buscar mes
            try:
                bp_mes=ws.Range(ws.Cells(fila_titulos+1,bp1),ws.Cells(fila_titulos+1,bp_last)).Find(fecha,LookIn=win32.constants.xlValues).Column
            except:
                bp_mes=ws.Range(ws.Cells(fila_titulos+1,bp1),ws.Cells(fila_titulos+1,bp_last)).Find(fecha.replace("-","/"),LookIn=win32.constants.xlValues).Column
            #AGRUPAR
            if bp_mes!=bp1:
                rango=buscar_rango(bp1,bp_mes-1)
                ws.Range(rango).Group()
            if bp_mes!=bp_last:
                rango=buscar_rango(bp_mes+1,bp_last)
                ws.Range(rango).Group()
        except:
            pass
            
        
        try:
            #DELTA
            #buscar primer y último DELTA
            delta1=ws.UsedRange.Find("DELTA",LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            delta_last=ws.Range(ws.Cells(fila_titulos,delta1),ws.Cells(fila_titulos,blankcol)).Find("DELTA",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            if delta1 == delta_last:
                new_blankcol=ws.Cells(fila_titulos,delta1).End(win32.constants.xlToRight).Column+1
                delta_last=ws.Range(ws.Cells(fila_titulos,delta1),ws.Cells(fila_titulos,new_blankcol)).Find("DELTA",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            #Entre primer y último BP, buscar mes
            try:
                delta_mes=ws.Range(ws.Cells(fila_titulos+1,delta1),ws.Cells(fila_titulos+1,delta_last)).Find(fecha,LookIn=win32.constants.xlValues).Column
            except:
                delta_mes=ws.Range(ws.Cells(fila_titulos+1,delta1),ws.Cells(fila_titulos+1,delta_last)).Find(fecha.replace("-","/"),LookIn=win32.constants.xlValues).Column
            #AGRUPAR
            if delta_mes!=delta1:
                rango=buscar_rango(delta1,delta_mes-1)
                ws.Range(rango).Group()
            if delta_mes!=delta_last:
                rango=buscar_rango(delta_mes+1,delta_last)
                ws.Range(rango).Group()
        except:
            pass
        
        try:
            #DELTA %
            #buscar primer y último DELTA %
            delta1=ws.UsedRange.Find("DELTA %",LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            delta_last=ws.Range(ws.Cells(fila_titulos,delta1),ws.Cells(fila_titulos,blankcol)).Find("DELTA %",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            if delta1 == delta_last:
                new_blankcol=ws.Cells(fila_titulos,delta1).End(win32.constants.xlToRight).Column+1
                delta_last=ws.Range(ws.Cells(fila_titulos,delta1),ws.Cells(fila_titulos,new_blankcol)).Find("DELTA %",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            #Entre primer y último BP, buscar mes
            try:
                delta_mes=ws.Range(ws.Cells(fila_titulos+1,delta1),ws.Cells(fila_titulos+1,delta_last)).Find(fecha,LookIn=win32.constants.xlValues).Column
            except:
                delta_mes=ws.Range(ws.Cells(fila_titulos+1,delta1),ws.Cells(fila_titulos+1,delta_last)).Find(fecha.replace("-","/"),LookIn=win32.constants.xlValues).Column
            #AGRUPAR
            if delta_mes!=delta1:
                rango=buscar_rango(delta1,delta_mes-1)
                ws.Range(rango).Group()
            if delta_mes!=delta_last:
                rango=buscar_rango(delta_mes+1,delta_last)
                ws.Range(rango).Group()
        except:
            pass
        
        
        try:
            #DELTA%
            #buscar primer y último DELTA %
            delta1=ws.UsedRange.Find("DELTA%",LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            delta_last=ws.Range(ws.Cells(fila_titulos,delta1),ws.Cells(fila_titulos,blankcol)).Find("DELTA%",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            if delta1 == delta_last:
                new_blankcol=ws.Cells(fila_titulos,delta1).End(win32.constants.xlToRight).Column+1
                delta_last=ws.Range(ws.Cells(fila_titulos,delta1),ws.Cells(fila_titulos,new_blankcol)).Find("DELTA%",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            #Entre primer y último BP, buscar mes
            try:
                delta_mes=ws.Range(ws.Cells(fila_titulos+1,delta1),ws.Cells(fila_titulos+1,delta_last)).Find(fecha,LookIn=win32.constants.xlValues).Column
            except:
                delta_mes=ws.Range(ws.Cells(fila_titulos+1,delta1),ws.Cells(fila_titulos+1,delta_last)).Find(fecha.replace("-","/"),LookIn=win32.constants.xlValues).Column
            #AGRUPAR
            if delta_mes!=delta1:
                rango=buscar_rango(delta1,delta_mes-1)
                ws.Range(rango).Group()
            if delta_mes!=delta_last:
                rango=buscar_rango(delta_mes+1,delta_last)
                ws.Range(rango).Group()
        except:
            pass
        

        
        try:
            #GROWTH
            #buscar primer y último GROWTH
            gro1=ws.UsedRange.Find("GROWTH",LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            gro_last=ws.Range(ws.Cells(fila_titulos,gro1),ws.Cells(fila_titulos,blankcol)).Find("GROWTH",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            if gro1 == gro_last:
                new_blankcol=ws.Cells(fila_titulos,gro1).End(win32.constants.xlToRight).Column+1
                gro_last=ws.Range(ws.Cells(fila_titulos,gro1),ws.Cells(fila_titulos,new_blankcol)).Find("GROWTH",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            #Entre primer y último BP, buscar mes
            try:
                gro_mes=ws.Range(ws.Cells(fila_titulos+1,gro1),ws.Cells(fila_titulos+1,gro_last)).Find(fecha,LookIn=win32.constants.xlValues).Column
            except:
                gro_mes=ws.Range(ws.Cells(fila_titulos+1,gro1),ws.Cells(fila_titulos+1,gro_last)).Find(fecha.replace("-","/"),LookIn=win32.constants.xlValues).Column
            #AGRUPAR
            if gro_mes!=gro1:
                rango=buscar_rango(gro1,gro_mes-1)
                ws.Range(rango).Group()
            if gro_mes!=gro_last:
                rango=buscar_rango(gro_mes+1,gro_last)
                ws.Range(rango).Group()
        except:
            pass
        
 
        
        try:
            #FORECAST
            #buscar primer y último FORECAST
            for1=ws.Rows(fila_titulos).Find("FORECAST",LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            for_last=ws.Range(ws.Cells(fila_titulos,for1),ws.Cells(fila_titulos,blankcol)).Find("FORECAST",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            if for1 == for_last:
                new_blankcol=ws.Cells(fila_titulos,for1).End(win32.constants.xlToRight).Column+1
                for_last=ws.Range(ws.Cells(fila_titulos,for1),ws.Cells(fila_titulos,new_blankcol)).Find("FORECAST",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            #Entre primer y último BP, buscar mes
            try:
                for_mes=ws.Range(ws.Cells(fila_titulos+1,for1),ws.Cells(fila_titulos+1,for_last)).Find(fecha,LookIn=win32.constants.xlValues).Column
            except:
                for_mes=ws.Range(ws.Cells(fila_titulos+1,for1),ws.Cells(fila_titulos+1,for_last)).Find(fecha.replace("-","/"),LookIn=win32.constants.xlValues).Column
            #AGRUPAR
            if for_mes!=for1:
                rango=buscar_rango(for1,for_mes-1)
                ws.Range(rango).Group()
            if for_mes!=for_last:
                rango=buscar_rango(for_mes+1,for_last)
                ws.Range(rango).Group()
        except:
            pass
        
        
        try:
            #DELTA FC
            #buscar primer y último DELTA FC
            for1=ws.Rows(fila_titulos).Find("DELTA FC",LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            for_last=ws.Range(ws.Cells(fila_titulos,for1),ws.Cells(fila_titulos,blankcol)).Find("DELTA FC",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            if for1 == for_last:
                new_blankcol=ws.Cells(fila_titulos,for1).End(win32.constants.xlToRight).Column+1
                for_last=ws.Range(ws.Cells(fila_titulos,for1),ws.Cells(fila_titulos,new_blankcol)).Find("DELTA FC",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            #Entre primer y último BP, buscar mes
            try:
                for_mes=ws.Range(ws.Cells(fila_titulos+1,for1),ws.Cells(fila_titulos+1,for_last)).Find(fecha,LookIn=win32.constants.xlValues).Column
            except:
                for_mes=ws.Range(ws.Cells(fila_titulos+1,for1),ws.Cells(fila_titulos+1,for_last)).Find(fecha.replace("-","/"),LookIn=win32.constants.xlValues).Column
            #AGRUPAR
            if for_mes!=for1:
                rango=buscar_rango(for1,for_mes-1)
                ws.Range(rango).Group()
            if for_mes!=for_last:
                rango=buscar_rango(for_mes+1,for_last)
                ws.Range(rango).Group()
        except:
            pass
        
        
        try:
            #DELTA BP
            #buscar primer y último DELTA BP
            for1=ws.Rows(fila_titulos).Find("DELTA BP",LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            for_last=ws.Range(ws.Cells(fila_titulos,for1),ws.Cells(fila_titulos,blankcol)).Find("DELTA BP",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            if for1 == for_last:
                new_blankcol=ws.Cells(fila_titulos,for1).End(win32.constants.xlToRight).Column+1
                for_last=ws.Range(ws.Cells(fila_titulos,for1),ws.Cells(fila_titulos,new_blankcol)).Find("DELTA BP",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            #Entre primer y último BP, buscar mes
            try:
                for_mes=ws.Range(ws.Cells(fila_titulos+1,for1),ws.Cells(fila_titulos+1,for_last)).Find(fecha,LookIn=win32.constants.xlValues).Column
            except:
                for_mes=ws.Range(ws.Cells(fila_titulos+1,for1),ws.Cells(fila_titulos+1,for_last)).Find(fecha.replace("-","/"),LookIn=win32.constants.xlValues).Column
            #AGRUPAR
            if for_mes!=for1:
                rango=buscar_rango(for1,for_mes-1)
                ws.Range(rango).Group()
            if for_mes!=for_last:
                rango=buscar_rango(for_mes+1,for_last)
                ws.Range(rango).Group()
        except:
            pass
        
        
        try:
            #DELTA BP %
            #buscar primer y último DELTA BP %
            for1=ws.Rows(fila_titulos).Find("DELTA BP %",LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            for_last=ws.Range(ws.Cells(fila_titulos,for1),ws.Cells(fila_titulos,blankcol)).Find("DELTA BP %",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            if for1 == for_last:
                new_blankcol=ws.Cells(fila_titulos,for1).End(win32.constants.xlToRight).Column+1
                for_last=ws.Range(ws.Cells(fila_titulos,for1),ws.Cells(fila_titulos,new_blankcol)).Find("DELTA BP %",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            #Entre primer y último BP, buscar mes
            try:
                for_mes=ws.Range(ws.Cells(fila_titulos+1,for1),ws.Cells(fila_titulos+1,for_last)).Find(fecha,LookIn=win32.constants.xlValues).Column
            except:
                for_mes=ws.Range(ws.Cells(fila_titulos+1,for1),ws.Cells(fila_titulos+1,for_last)).Find(fecha.replace("-","/"),LookIn=win32.constants.xlValues).Column
            #AGRUPAR
            if for_mes!=for1:
                rango=buscar_rango(for1,for_mes-1)
                ws.Range(rango).Group()
            if for_mes!=for_last:
                rango=buscar_rango(for_mes+1,for_last)
                ws.Range(rango).Group()
        except:
            pass
        
        
        try:
            #DELTA FC %
            #buscar primer y último DELTA FC
            for1=ws.Rows(fila_titulos).Find("DELTA FC %",LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            for_last=ws.Range(ws.Cells(fila_titulos,for1),ws.Cells(fila_titulos,blankcol)).Find("DELTA FC %",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            if for1 == for_last:
                new_blankcol=ws.Cells(fila_titulos,for1).End(win32.constants.xlToRight).Column+1
                for_last=ws.Range(ws.Cells(fila_titulos,for1),ws.Cells(fila_titulos,new_blankcol)).Find("DELTA FC %",SearchDirection=win32.constants.xlPrevious,LookAt=win32.constants.xlWhole,SearchOrder=win32.constants.xlByRows).Column
            #Entre primer y último BP, buscar mes
            try:
                for_mes=ws.Range(ws.Cells(fila_titulos+1,for1),ws.Cells(fila_titulos+1,for_last)).Find(fecha,LookIn=win32.constants.xlValues).Column
            except:
                for_mes=ws.Range(ws.Cells(fila_titulos+1,for1),ws.Cells(fila_titulos+1,for_last)).Find(fecha.replace("-","/"),LookIn=win32.constants.xlValues).Column
            #AGRUPAR
            if for_mes!=for1:
                rango=buscar_rango(for1,for_mes-1)
                ws.Range(rango).Group()
            if for_mes!=for_last:
                rango=buscar_rango(for_mes+1,for_last)
                ws.Range(rango).Group()
        except:
            pass

        ws.Outline.ShowLevels(ColumnLevels = 1)


        # Hide Metric columns in new model

        # Leave one empty column before month variables columns 
        if actuals1 - variable_names_column > 2:
            range_to_hide = buscar_rango(variable_names_column + 1, actuals1 - 2)
            ws.Range(range_to_hide).EntireColumn.Hidden = True     
        else: 
            pass

        ## Empty columns in month variables
        previous_blank_col = actuals1
        while previous_blank_col < ( YTD_first_column - 2) - 1:
            new_blank_col_object = ws.Range(ws.Cells(fila_titulos, previous_blank_col), ws.Cells(fila_titulos, (YTD_first_column - 2) - 1)).Find("",SearchDirection=win32.constants.xlNext,SearchOrder=win32.constants.xlByColumns)
            try:
                new_blank_col = new_blank_col_object.Column
                if new_blank_col != previous_blank_col:
                    range_to_hide = buscar_rango(new_blank_col, new_blank_col)
                    ws.Range(range_to_hide).EntireColumn.Hidden = True
                    previous_blank_col = new_blank_col     
                else: 
                    break
            except:
                break
        
        ## Leave just one Empty columns in month and YTD variables
        if YTD_first_column - month_last_column > 2:
            range_to_hide = buscar_rango(month_last_column + 2, YTD_first_column - 1)
            ws.Range(range_to_hide).EntireColumn.Hidden = True     
        else: 
            pass


        ## Empty columns in YTD variables
        previous_blank_col = YTD_first_column
        while previous_blank_col < YTD_last_column - 1:
            new_blank_col_object = ws.Range(ws.Cells(fila_titulos, previous_blank_col), ws.Cells(fila_titulos, YTD_last_column - 1)).Find("",SearchDirection=win32.constants.xlNext,SearchOrder=win32.constants.xlByColumns)
            try:
                new_blank_col = new_blank_col_object.Column
                if new_blank_col != previous_blank_col:
                    range_to_hide = buscar_rango(new_blank_col, new_blank_col)
                    ws.Range(range_to_hide).EntireColumn.Hidden = True
                    previous_blank_col = new_blank_col     
                else: 
                    break
            except:
                break

        # if actuals1 >6:
        #     range_to_hide = buscar_rango(5, actuals1 - 2)
        #     ws.Range(range_to_hide).EntireColumn.Hidden = True
        
    #Cambiar mes en "Aux"
    try:
        ws2=wb.Sheets("Aux")
        mes_anterior=ws2.Range("month").Value
        if mes_anterior>12:
            ws2.Range("month").Value=mes_anterior+1
        else:
            ws2.Range("month").Value=mes
    except:
        pass
        
    wb.Save()
    wb.Close()
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()    
