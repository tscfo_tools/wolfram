# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 12:43:53 2021

@author: romina

Agrosingularity
"""
import pandas as pd
import datetime as dt
from xlwings import App,Book

def cambiar_mes(fecha):
    try:
        fecha=dt.datetime.strptime(fecha,'%d-%m-%Y').strftime("%m")
    except:
        
            fecha=dt.datetime.strptime(fecha,'%d/%m/%Y').strftime("%m")
        
    return fecha

def cambiar_fecha2(fecha):
    
    try:
        fecha=fecha.strftime('%d-%m-%Y')
        fecha=fecha.replace("-","/")
    except:
            fecha=fecha.strftime('%d/%m/%Y')
    return fecha

def dataframe_difference(df1, df2, lista=None, which=None):
    '''
    Parameters
    ----------
    df1 : dataframe
    df2 : dataframe
    which : TYPE, optional
        The default is None.

    Returns
    -------
    diff_df : dataframe.

    '''
    """Find rows which are different between two DataFrames."""
    comparison_df = df1.merge(
        df2,
        indicator=True,
        how='right',
        on=lista,
        suffixes=('_DROP', ''),
    )
    if which is None:
        diff_df = comparison_df[comparison_df['_merge'] != 'both']
    else:
        diff_df = comparison_df[comparison_df['_merge'] == which]
        #diff_df.to_csv('data/diff.csv')
    return diff_df

def cambiar_fecha1(columna_fecha):
    try:
        columna_fecha=pd.to_datetime(columna_fecha,format='%d-%m-%Y')
        columna_fecha=columna_fecha.replace("-","/")
    except:
        try:
            columna_fecha=pd.to_datetime(columna_fecha,format='%d/%m/%Y')
        except:
            columna_fecha=pd.to_datetime(columna_fecha,format='%d-%m-%y')
    return columna_fecha   



def insertar_ventas_compras(ventas,compras,report,hoja_report_ventas,hoja_report_compras,mes,anno,v,c):
    
    app = App(visible=False)
    wb=Book(report)
        
    if v==True:
        df_mayor=pd.read_excel(ventas)
        df_report=pd.read_excel(report, sheet_name=hoja_report_ventas)
        columnas_report=list(df_report.columns)
        
        total_report=len(df_report)+1
        
        col_iguales=list(set(df_mayor.columns).intersection(set(df_report.columns)))
        
        df_report["indexes"]=list(df_report.axes[0])
        
        df_report=df_report.drop(df_report[df_report["Fecha registro"]=="END"].index)    
    
        
        df_mayor["Mes"]=df_mayor["Fecha registro"].apply(lambda x: cambiar_mes(cambiar_fecha2(x)))
        df_report["Mes"]=df_report["Fecha registro"].apply(lambda x: cambiar_mes(cambiar_fecha2(cambiar_fecha1(x))))
        
        col_iguales.remove("Fecha registro")
        diff=dataframe_difference(df_report,df_mayor,lista=col_iguales)
        diff=diff.drop(['_merge'],axis=1)    
        col_iguales.append("Fecha registro")
        diff=diff[col_iguales]
        diff["Fecha registro"]=diff["Fecha registro"].apply(lambda x: cambiar_fecha2(cambiar_fecha1(x)))
        diff["Fecha registro"]=diff["Fecha registro"].apply(lambda x: "'"+x)
        
        if len(diff)>0:
            
            ws=wb.sheets[hoja_report_ventas]
            rango=str(total_report) +":" + str(total_report+len(diff))
            ws.range(rango).insert()
            
            for titulo in col_iguales:
                columna=columnas_report.index(titulo)+1
                rango=chr(64+columna)+str(total_report)
                ws.range(rango).options(index=False).value=diff[titulo]
                
            ws.range(str(total_report)+":"+str(total_report)).api.Delete()
            
            #Copiar fórmula de Month
            rango="A"+str(total_report)+":A"+str(total_report+len(diff)-1)
            ws.range("A"+str(total_report-1)).copy()
            ws.range(rango).paste("formulas")
        
    if c==True:
        df_mayor=pd.read_excel(compras)
        df_report=pd.read_excel(report, sheet_name=hoja_report_compras)
        columnas_report=list(df_report.columns)
        total_report=len(df_report)+1
        
        col_iguales=list(set(df_mayor.columns).intersection(set(df_report.columns)))
        
        df_report["indexes"]=list(df_report.axes[0])
        
        df_report=df_report.drop(df_report[df_report["Fecha registro"]=="END"].index)    
    
        
        df_mayor["Mes"]=df_mayor["Fecha registro"].apply(lambda x: cambiar_mes(cambiar_fecha2(x)))
        df_report["Mes"]=df_report["Fecha registro"].apply(lambda x: cambiar_mes(cambiar_fecha2(cambiar_fecha1(x))))
        
        col_iguales.remove("Fecha registro")
        diff=dataframe_difference(df_report,df_mayor,lista=col_iguales)
        diff=diff.drop(['_merge'],axis=1)    
        col_iguales.append("Fecha registro")
        diff=diff[col_iguales]
        diff["Fecha registro"]=diff["Fecha registro"].apply(lambda x: cambiar_fecha2(cambiar_fecha1(x)))
        diff["Fecha registro"]=diff["Fecha registro"].apply(lambda x: "'"+x)
        
        if len(diff)>0:
            
            ws=wb.sheets[hoja_report_compras]
            rango=str(total_report) +":" + str(total_report+len(diff))
            ws.range(rango).insert()
            
            for titulo in col_iguales:
                columna=columnas_report.index(titulo)+1
                rango=chr(64+columna)+str(total_report)
                ws.range(rango).options(index=False).value=diff[titulo]
                
            ws.range(str(total_report)+":"+str(total_report)).api.Delete()
            
            #Copiar fórmula de Month
            rango="A"+str(total_report)+":A"+str(total_report+len(diff)-1)
            ws.range("A"+str(total_report-1)).copy()
            ws.range(rango).paste("formulas")

    wb.save()
    wb.close()
     
    app.kill()
